struct OptimizationArguments 
    inputFileName::String
    outputFileName::String
    coords::String
    noHydrogenBonds::Bool
    underlyingCoords::String
    updateEachStepInBackTransformation::Bool
    constraints::SetOfConstraints
    trustRadius::Float64
    trustRadiusOptions::Dict{Symbol, Float64}
    engine::String
    engineOptions::Dict{Symbol, Any}
    hessian::String
    initialHessian::String
    guessType::String
    updateHessian::String
    updateHessianOptions::Int64
    method::String
    useQuadraticModel::Bool
    convergenceCheck::String
    convergenceCriteria::Array{Float64}
    trustRadiusUpdate::String
    optimizer::String
    isDryRun::Bool
    maxIter::Int64
    logLevelDebug::Bool
    logLevelWarn::Bool
    logFile::String
end