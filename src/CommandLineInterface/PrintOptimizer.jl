struct PrintOptimizer
    coords::AbstractCoordinates
    gradientsAndHessian::AbstractGradientsAndHessian
    stepControl::AbstractStepControl
    convergenceCheck::AbstractConvergenceCheck
    initialHessian::AbstractInitialHessian
    trustRadiusUpdate::AbstractTrustRadiusUpdate
end

struct PrintOptimizationAlgorithm
    optimizer::PrintOptimizer
    optimizerType::String
    maxIter::Int
end

function printOptimizer(optimizer::PrintOptimizer)
    @info("Types and data supposed to be used for an optimization:",
        typeCoords = typeof(optimizer.coords),
        typeGradientsAndHessian = typeof(optimizer.gradientsAndHessian),
        typeStepControl = typeof(optimizer.stepControl),
        typeConvergenceCheck = typeof(optimizer.convergenceCheck),
        typeInitialHessian = typeof(optimizer.initialHessian),
        typeTrustRadiusUpdate = typeof(optimizer.trustRadiusUpdate),
        coords = optimizer.coords,
        gradientsAndHessian = optimizer.gradientsAndHessian,
        stepControl = optimizer.stepControl,
        convergenceCheck = optimizer.convergenceCheck,
        initialHessian = optimizer.initialHessian,
        trustRadiusUpdate = optimizer.trustRadiusUpdate
    )
end

function optimize!(algorithm::PrintOptimizationAlgorithm)
    @info "The Algorithm got the following arguments passed:" optimizerType=algorithm.optimizerType maxIter=algorithm.maxIter
    printOptimizer(algorithm.optimizer)
    getCartesians(algorithm.optimizer.coords)
end