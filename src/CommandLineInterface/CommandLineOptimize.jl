function getKeyFromDictOtherwiseError(key, dict, getErrorMessage)
    try 
        dict[key]
    catch
        @error getErrorMessage(key)
        throw(ArgumentError(getErrorMessage(key)))
    end
end

const updates = Dict(
       "BFGS" => (_) -> BfgsUpdate(), 
       "NBFGS" => (n) -> BfgsUseLastNForUpdate(n), 
       "SR1_PBS" => (_) -> Sr1PsbUpdate()
)

getHessianUpdate(args::OptimizationArguments) = getKeyFromDictOtherwiseError(args.updateHessian, updates, (k) -> "Could not find $(k) as Hessian Update.")(args.updateHessianOptions)

const initialHessians = Dict(
       "Calculate" => (_, _) -> CalculatedHessian(), 
       "Guess" => (symbols, guessType) -> GuessHessian(symbols, guessType), 
       "Identity" => (_, _) -> IdentityHessian()
)

getInitialHessianMethod(args::OptimizationArguments, symbols::Array{String}) = getKeyFromDictOtherwiseError(args.initialHessian, initialHessians, (k) -> "Could not find $(k) as Initial Hessian.")(symbols, args.guessType)

const engines = Dict(
       "Psi4" => (options) -> Psi4Engine(; options...),
       "Xtb" => (options) -> XtbEngine(; options...),
)

getEngine(args::OptimizationArguments) = getKeyFromDictOtherwiseError(args.engine, engines, (k) -> "Could not find $(k) as Engine.")(args.engineOptions)

const methods = Dict(
       "VerySimpleNewton" => (::Float64, ::Int64, ::Bool) -> SimpleNewton(), 
       "SimpleNewton" => (::Float64, saddlePoint::Int64, ::Bool) -> SimpleModHessianNewton(saddlePoint), 
       "ScalingNewton" => (trustRadius::Float64, saddlePoint::Int64, ::Bool) -> ScalingNewton(trustRadius, saddlePoint), 
       "TRM" => (trustRadius::Float64, saddlePoint::Int64, ::Bool) -> TrustRegionMethod(trustRadius, saddlePoint), 
       "RFO" => (::Float64, saddlePoint::Int, useQuadraticModel::Bool) -> SimpleRFO(saddlePointOrder=saddlePoint, useNewtonModelEnergyChange=useQuadraticModel), 
       "ScalingRFO" => (trustRadius::Float64, saddlePoint::Int64, useQuadraticModel::Bool) -> ScalingRFO(trustRadius=trustRadius, saddlePointOrder=saddlePoint, useNewtonModelEnergyChange=useQuadraticModel),
       "RSRFO" => (trustRadius::Float64, saddlePoint::Int64, useQuadraticModel::Bool) -> RSRFO(trustRadius=trustRadius, saddlePointOrder=saddlePoint, useNewtonModelEnergyChange=useQuadraticModel)
)

getMethod(args::OptimizationArguments) = getKeyFromDictOtherwiseError(args.method, methods, (k) -> "Could not find $(k) as Optimization Method.")(args.trustRadius, 0, args.useQuadraticModel)

const trustRadiusUpdateMethods = Dict(
       "None" => (_) -> NoTrustRadiusUpdate(),
       "Bofill" => (_) -> BofillTrustRadiusUpdate(),
       "Simple" => (options) -> StandardTrustRadiusUpdate(;options...),
       "SimpleNoReject" => (options) -> NonRejectingTrustRadiusUpdate(;options...)
)

getTrustRadiusUpdate(args::OptimizationArguments) = getKeyFromDictOtherwiseError(args.trustRadiusUpdate, trustRadiusUpdateMethods, (k) -> "Could not find $(k) as Trust Radius Update Method.")(args.trustRadiusOptions)

const convergenceChecks = Dict(
       "GradientsAndChange" => (args) -> GradientsAndChangeCheck(args...),
       "Gradients" => (args) -> OnlyGradientsCheck(args...),
       "ValueAndGradients" => (args) -> ValueAndGradientsCheck(args...),
       "ValueGradientsAndChange" => (args) -> ValueGradientsAndChangeCheck(args...),
       "GradientsAndValueOrChangeCheck" => (args) -> GradientsAndValueOrChangeCheck(args...)
)

getConvergenceCheck(args::OptimizationArguments) = getKeyFromDictOtherwiseError(args.convergenceCheck, convergenceChecks, (k) -> "Could not find $(k) as Convergence Check.")(args.convergenceCriteria)

function getCoordinateSystemAndSymbols(arguments::OptimizationArguments)
    constraints = arguments.constraints
    symbols, cartesians = readFromAngCartesiansToBohr(arguments.inputFileName)
    coordinates = if arguments.coords == "cart"
        CartesianCoordinates(cartesians; constraints=constraints)
    elseif arguments.coords == "zmat"
        if hasConstraints(constraints)
            throw(ArgumentError("Currently Z-Matrix Minimization does not work with constraints."))
        end
        newOrder, bondGraph  = getReorderedSequenceAndGraph(symbols, cartesians)
        symbols, cartesians = reorderSymbols(newOrder, symbols), reorderCoordinates(newOrder, cartesians)
        internalCoordinates = findAllInternalCoordinates(bondGraph, symbols, cartesians, withHydrogens=false)
        ZmatrixInternalCoordinates(internalCoordinates, cartesians)
    elseif arguments.coords == "ric"
        useZmat = arguments.underlyingCoords == "zmat"
        bondGraph = if useZmat
            newOrder, bondGraph  = getReorderedSequenceAndGraph(symbols, cartesians)
            symbols, cartesians = reorderSymbols(newOrder, symbols), reorderCoordinates(newOrder, cartesians)
            constraints = reorderConstraints(newOrder, constraints)
            bondGraph
        else
            buildBondGraph(symbols, cartesians)
        end
        internalCoordinates = findAllInternalCoordinates(bondGraph, symbols, cartesians, withHydrogens=!arguments.noHydrogenBonds)

        RedundantInternalCoordinates(internalCoordinates, cartesians; constraints=constraints, tryToUseZmatrixForTransformation=useZmat, updateOnCartesianBacktransformation=arguments.updateEachStepInBackTransformation)
    elseif arguments.coords == "del"
        useZmat = arguments.underlyingCoords == "zmat"
        bondGraph = if useZmat
            newOrder, bondGraph  = getReorderedSequenceAndGraph(symbols, cartesians)
            symbols, cartesians = reorderSymbols(newOrder, symbols), reorderCoordinates(newOrder, cartesians)
            constraints = reorderConstraints(newOrder, arguments.constraints)
            bondGraph
        else
            buildBondGraph(symbols, cartesians)
        end
        internalCoordinates = findAllInternalCoordinates(bondGraph, symbols, cartesians, withHydrogens=!arguments.noHydrogenBonds)
        
        DelocalizedInternalCoordinates(internalCoordinates, cartesians; constraints=constraints, tryToUseZmatrixForTransformation=useZmat, updateOnCartesianBacktransformation=arguments.updateEachStepInBackTransformation)
    else
        throw(ArgumentError("I do not know the coordinate system you want to use. Please use either 'red' for redundant interanl Coordinates, 'dic' for delocalized internal Coordinates, 'cart' for cartesian Coordinates, or 'zmat' for a Z-Matrix representation."))
    end
    coordinates, symbols
end
     
const methodsWithoutTrustRadius = ["VerySimpleNewton", "SimpleNewton", "RFO"]

function optimize(arguments::OptimizationArguments)
    logLevel = if arguments.logLevelDebug
        Debug
    elseif arguments.logLevelWarn
        Warn
    else
        Info
    end
    
    logger, closeFile = if arguments.logFile == "Console"
        ConsoleLogger(logLevel), () -> nothing
    else
        file = open(arguments.logFile, "w")
        SimpleLogger(file, logLevel), () -> close(file)
    end
    
    with_logger(logger) do 

        coords, symbols = getCoordinateSystemAndSymbols(arguments)

        gradientsAndHessian = begin 
            engine = getEngine(arguments)
            if arguments.hessian == "Update"
                hessianUpdate = getHessianUpdate(arguments)
                AnalyticGradientsUpdatedHessian(symbols, hessianUpdate, engine)
            elseif arguments.hessian == "Calculate"
                AnalyticGradientsAndHessian(symbols, engine)
            else
                throw(ArgumentError("You passed $(arguments.hessian) as argument for --hesssian, which is not known. Try to used 'Update' or ''."))
            end
        end

        initialHessian = getInitialHessianMethod(arguments, symbols)

        stepControl = getMethod(arguments)

        trustRadius = begin
            if arguments.method in methodsWithoutTrustRadius
                NoTrustRadiusUpdate()
            else
                getTrustRadiusUpdate(arguments)
            end
        end

        convergenceCheck = getConvergenceCheck(arguments)

        algorithm = if arguments.isDryRun 
            optimizer = PrintOptimizer(coords, gradientsAndHessian, stepControl, convergenceCheck, initialHessian, trustRadius)
            PrintOptimizationAlgorithm(optimizer, arguments.optimizer, arguments.maxIter)
        else 
            optimizer = StandardOptimizer(
                coords, 
                gradientsAndHessian, 
                stepControl, 
                convergenceCheck, 
                trustRadiusUpdate=trustRadius, 
                initialHessian=initialHessian
            )
            if arguments.optimizer == "Active"
                optimizer = StandardOptimizerUseActive(optimizer)
            end
            OptimizeWithGradientsCheckFromLastCycle(optimizer; maxIter=arguments.maxIter)
        end

        final = optimize!(algorithm)

        open(arguments.outputFileName, "w") do oFile
            write(oFile, writeAngCartesiansFromBohr(symbols, final))
        end
    end
    closeFile()
end