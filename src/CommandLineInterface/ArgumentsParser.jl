using ArgParse

function stringsToConstraints(args)
    n = length(args)
    if n == 0 return NoConstraints() end
    i = 1

    bonds = BondConstraint[]
    angles = AngleConstraint[]
    dihedrals = DihedralConstraint[]
    translationsX = TranslationXConstraint[]
    translationsY = TranslationYConstraint[]
    translationsZ = TranslationZConstraint[]
    rotationsA = RotationAConstraint[]
    rotationsB = RotationBConstraint[]
    rotationsC = RotationCConstraint[]
    function readIndices(args, iter)
        iter += 1
        indices = Int64[]
        nextNumber = tryparse(Int64, args[iter])
        while nextNumber !== nothing
            push!(indices, nextNumber)
            iter+=1
            if iter > n break end
            nextNumber = tryparse(Int64, args[iter])
        end
        iter, indices
    end
    while i <= n
        if args[i] == "b"
            b = BondConstraint(parse(Int, args[i+1]), parse(Int, args[i+2]))
            push!(bonds, b)
            i += 3
        elseif args[i] == "a"
            a = AngleConstraint(parse(Int, args[i+1]), parse(Int, args[i+2]), parse(Int, args[i+3]))
            push!(angles, a)
            i += 4
        elseif args[i] == "d"
            d = DihedralConstraint(parse(Int, args[i+1]), parse(Int, args[i+2]), parse(Int, args[i+3]), parse(Int, args[i+4]))
            push!(dihedrals, d)
            i += 5
        elseif args[i] == "tx"
            i, indices = readIndices(args, i)
            push!(translationsX, TranslationXConstraint(indices))
        elseif args[i] == "ty"
            i, indices = readIndices(args, i)
            push!(translationsY, TranslationYConstraint(indices))
        elseif args[i] == "tz"
            i, indices = readIndices(args, i)
            push!(translationsZ, TranslationZConstraint(indices))
        elseif args[i] == "t"
            i, indices = readIndices(args, i)
            push!(translationsX, TranslationXConstraint(indices))
            push!(translationsY, TranslationYConstraint(indices))
            push!(translationsZ, TranslationZConstraint(indices))
        elseif args[i] == "ra"
            i, indices = readIndices(args, i)
            push!(rotationsA, RotationAConstraint(indices))
        elseif args[i] == "rb"
            i, indices = readIndices(args, i)
            push!(rotationsB, RotationBConstraint(indices))
        elseif args[i] == "rc"
            i, indices = readIndices(args, i)
            push!(rotationsC, RotationCConstraint(indices))
        elseif args[i] == "r"
            i, indices = readIndices(args, i)
            push!(rotationsA, RotationAConstraint(indices))
            push!(rotationsB, RotationBConstraint(indices))
            push!(rotationsC, RotationCConstraint(indices))
        else
            throw(ArgumentError("Could not parse your constraints! Please enter [b|a|d] followed by [2|3|4] integer for your constraints!"))
        end
    end
    primitiveConstraints = PrimitiveConstraints(bonds, angles, dihedrals)
    if isempty(translationsX) && isempty(translationsY) && isempty(translationsZ) && isempty(rotationsA) && isempty(rotationsB) && isempty(rotationsC)
        primitiveConstraints
    else
        TrAndPrimitiveConstraints(translationsX, translationsY, translationsZ, rotationsA, rotationsB, rotationsC, primitiveConstraints)
    end
end

const intArgumentsEngine = ["memoryInGb", "charge", "multiplicity", "cores", "gfnMethod"]

function convertEngineArgs(key, value)
    if key in intArgumentsEngine
        parse(Int64, value)
    else
        value
    end
end

function parseEngineAndOptions(args)
    n = length(args)
    if n % 2 == 0
        throw(ArgumentError("Please pass an engine followed by key value pairs. I got: $(args)"))
    end
    engine = args[1]
    options = Dict()
    for i in 2:2:n
        options[Symbol(args[i])] = convertEngineArgs(args[i], args[i+1])
    end
    engine, options
end

function parseUpdateMethod(args)
    if args[1] == "NBFGS"
        args[1], length(args) > 1 ? parse(Int64, args[2]) : 4
    else
        args[1], 0
    end
end

function parseConvergenceCheckValues(args)
    method = args[1]

    method, [parse(Float64, x) for x in args[2:end]]
end

function parseMethod(args)
    method = args[1]
    useQuadraticModel = length(args) > 1 && args[2] == "useQuadraticModel"
    method, useQuadraticModel
end

function parseInitialHessian(hessianType, args)
    if hessianType == "Calculate"
        "Calculate", ""
    elseif args[1] == "Guess"
        if length(args) > 1
            args[1], args[2]
        else
            args[1], "Schlegel"
        end
    else 
        args[1], ""
    end
end

function parseCoords(args)
    if args[1] == "ric"
        if length(args) > 1
            if args[2] == "cart" && length(args) > 2 && args[3] == "noMicroUpdate"
                args[1], args[2], false
            else
                args[1], args[2], true
            end
        else
            args[1], "cart", true
        end
    elseif args[1] == "del"
        if length(args) > 1
            if args[2] == "cart" && length(args) > 2 && args[3] == "noMicroUpdate"
                args[1], args[2], false
            else
                args[1], args[2], true
            end
        else
            args[1], "zmat", true
        end
    else
        args[1], "", true
    end
end

const simpleRejectingMapper = Dict(
    "maxTrustRadius" => (arg) -> (:maxTrustRadius, parse(Float64, arg)), 
    "minTrustRadius" => (arg) -> (:minTrustRadius, parse(Float64, arg)), 
    "increaseFactor" => (arg) -> (:increaseFactor, parse(Float64, arg)), 
    "decreaseFactor" => (arg) -> (:decreaseFactor, parse(Float64, arg)), 
    "rejectThreshold" => (arg) -> (:rejectingWhenStepPredectionBelowThreshold, parse(Float64, arg)), 
)

const simpleNonRejectingMapper = Dict(
    "maxTrustRadius" => (arg) -> (:maxTrustRadius, parse(Float64, arg)), 
    "minTrustRadius" => (arg) -> (:minTrustRadius, parse(Float64, arg)), 
    "increaseFactor" => (arg) -> (:increaseFactor, parse(Float64, arg)), 
    "decreaseFactor" => (arg) -> (:decreaseFactor, parse(Float64, arg)),
)

function handleError(::KeyError, method, key, _)
    throw(ArgumentError("I cannot handle the argument \"$(key)\" for update method \"$(method)\""))
end

function handleError(::ArgumentError, method, key, value)
    throw(ArgumentError("I expected a float after argument \"$(key)\" for update method \"$(method)\". I got \"$(value)\"."))
end

function handleError(e, _, _, _)
    rethrow(e)
end

function parseTrustRadiusUpdate(args)
    n = length(args)
    if n % 2 == 0
        throw(ArgumentError("Please pass a trust radius update method followed by key value pairs. I got: $(args)"))
    end
    updateMethod = args[1]
    if updateMethod != "Simple" && updateMethod != "SimpleNoReject"
        return updateMethod, Dict()
    end
    options = Dict()
    mapper = updateMethod == "Simple" ? simpleRejectingMapper : simpleNonRejectingMapper
    for i in 2:2:n
        key, value = try
            mapper[args[i]](args[i+1])
        catch e
            handleError(e, updateMethod, args[i], args[i+1])
        end
        options[key] = value
    end
    updateMethod, options
end

function parseArguments()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "--constraints"
            help = "If constraints are desired write \"b\", \"a\", \"d\", \"ra\", \"rb\", \"rc\", \"tx\", \"ty\", \"tz\", \"r\", or \"t\" followed by two, three, or four indices, respectively."
            nargs = '+'
        "--inputFile", "-i"
            help = "The name of the input file can be specified."
            arg_type = String
            default = "input.xyz"
        "--outputFile", "-o"
            help = "The name of the output file can be specified."
            arg_type = String
            default = "output.xyz"
        "--coords", "-c"
            help = "The type of coordinates to use. \"cart\" for cartesian coordinates, \"zmat\" for a Z-Matrix, \"ric\" for Redundant Internal Coordinates, or \"del\" for Delocalized Internal Coordinates. \"ric\" and \"del\" use a underlying coordiate system to evaluate change. This can be chosen by add \"cart\" or \"zmat\". If not provided \"cart\" is used for \"ric\" and \"zmat\" is used for \"del\". If \"cart\" is used as underlying system then a thrid value of \"noMicroUpdate\" can be set, then the B-Matrix and G-Matrix will not be updated in the microiterative step."
            nargs = '+'
            default = ["del", "zmat"]
        "--noHydrogenBonds"
            help = "Boolean flag which is to disable the search for hydrogen bonds."
            nargs = 0
        "--trustRadius"
            help = "The value of the initial Trust Radius."
            arg_type = Float64
            default = 0.5
        "--engine", "-e"
            help = "The engine you want to use. Options: Psi4, Xtb. Optionally followed by parameters for the specific engine. For Psi4 there are: \"path\", \"memoryInGb\", \"basisSet\", \"method\", \"charge\", \"multiplicity\", and \"cores\". For Xtb the parameters are: \"path\" and \"gfnMethod\"."
            nargs = '+'
            default = ["Psi4"]
        "--hessian"
            help = "Decide if the Hessian is supposed to be Updated with \"Update\" or calculated with \"Calculate\"."
            arg_type = String
            default = "Update"
        "--initialHessian"
            help = "Decide which iitial Hessian should be used. Either \"Guess\", \"Calculate\", or \"Identity\". If the --hessian flag was set to \"Calculate\" then the initial Hessian will always be calculated. The option \"Guess\" can be followed by either \"Schlegel\" or \"Lindh\" defining the scheme which is supposed to be used. If no scheme is added \"Schlegel\" is used."
            nargs = '+'
            default = ["Guess", "Schlegel"]
        "--updateHessian"
            help = "If \"Update\" was chosen you can define which update Mechanism is supposed to be used. Choose between \"BFGS\", \"SR1_PBS\", or \"NBFGS\" where for \"NBFGS\" an integer can be appended which defines how many previous steps are supposed to be used to update the current Hessian with. If no integer is provided the value \"4\" is used."
            nargs = '+'
            default = ["BFGS"]
        "--method", "-m"
            help = "Define the method to use for the optimization. Options are: \"SimpleNewton\", \"ScalingNewton\", \"TRM\", \"RFO\", \"ScalingRFO\", \"RSRFO\". For all RFO methods it can be optionally set that the quadratic model is supposed to be used for the model energy by adding \"useQuadraticModel\" after the RFO method."
            nargs = '+'
            default = ["RSRFO"]
        "--convergenceCheck"
            help = "Decide which values should be checked in order to define convergence. First the Name is defined followed by max(g), rms(g), max(Δx), rms(Δx), Δe. If any value is not used it is excluded. So if checked only for gradients and energy it would be max(g), rms(g), Δe. Possiblities: \"GradientsAndChange\", \"Gradients\", \"ValueAndGradients\", \"ValueGradientsAndChange\", \"GradientsAndValueOrChangeCheck\"."
            nargs = '+'
            default = ["GradientsAndChange"]
        "--trustRadiusUpdate"
            help = "Decide which trust radius updating scheme should be used. Options are: \"None\", \"Bofill\", \"Simple\", and \"SimpleNoReject\". If \"Simple\" or \"SimpleNoReject\" is chosen additional arguments can be chosen: \"maxTrustRadius\", \"minTrustRadius\", \"increaseFactor\", \"decreaseFactor\", and \"rejectThreshold\". Whereas the last only applies for the \"Simple\" method."
            nargs = '+'
            default = ["SimpleNoReject"]
        "--optimizer"
            help = "Decide which optimizer is supposed to be used. One which only uses active gradients and Hessian, also for the Hessian update, or one which is using none active gradients for the Hessian update. Options are: \"Active\" and \"NonActive\"."
            arg_type = String
            default = "Active"
        "--dry"
            help = "Boolean flag which is to be set if a dryrun only printing out which values are used for an optimization with the given parameters are printed out. Only used for testing and debugging."
            nargs = 0
        "--maxIter"
            help = "Decide after how many steps the optimizer will terminate if no convergence will be reached."
            arg_type = Int64
            default = 100
        "--verbose"
            help = "Boolean flag which sets the log level to Debug."
            nargs = 0
        "--silent"
            help = "Boolean flag which sets the log level to Warn. (--verbose has a higher precedence and thus, if it is set, the log level will be Debug)"
            nargs = 0
        "--logFile"
            help = "Name of the logfile. Use \"Console\" if it should be logged to the console."
            arg_type = String
            default = "log.dat"
    end
    parsed_args = parse_args(ARGS, s)

    constraints = stringsToConstraints(parsed_args["constraints"])
    engine, engineOptions = parseEngineAndOptions(parsed_args["engine"])
    hessian = parsed_args["hessian"]
    initialHessian, guessType = parseInitialHessian(hessian, parsed_args["initialHessian"])
    hessianUpdate, updateOptions = parseUpdateMethod(parsed_args["updateHessian"])
    checkMethod, checkThresholds = parseConvergenceCheckValues(parsed_args["convergenceCheck"])
    method, useQuadraticModel = parseMethod(parsed_args["method"])
    coords, underlyingCoords, updateEachStepInBackTransformation = parseCoords(parsed_args["coords"])
    trustRadiusUpdate, trustRadiusOptions = parseTrustRadiusUpdate(parsed_args["trustRadiusUpdate"])
    
    OptimizationArguments(parsed_args["inputFile"], 
                          parsed_args["outputFile"], 
                          coords, 
                          parsed_args["noHydrogenBonds"], 
                          underlyingCoords, 
                          updateEachStepInBackTransformation,
                          constraints, 
                          parsed_args["trustRadius"],
                          trustRadiusOptions, 
                          engine, 
                          engineOptions, 
                          hessian, 
                          initialHessian, 
                          guessType, 
                          hessianUpdate, 
                          updateOptions,
                          method, 
                          useQuadraticModel, 
                          checkMethod, 
                          checkThresholds, 
                          trustRadiusUpdate,
                          parsed_args["optimizer"], 
                          parsed_args["dry"], 
                          parsed_args["maxIter"], 
                          parsed_args["verbose"], 
                          parsed_args["silent"], 
                          parsed_args["logFile"]
    )
end