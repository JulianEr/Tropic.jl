mutable struct StandardOptimizer <: AbstractOptimizer
    xs::AbstractCoordinates
    formerXs::AbstractCoordinates

    energy::Float64
    formerEnergy::Float64
    
    change::Array{Float64}
    gradients::Array{Float64}
    formerGradients::Array{Float64}
    
    hessian::Array{Float64, 2}
    formerHessian::Array{Float64, 2}

    initialHessian::AbstractInitialHessian
    gradientsAndHessian::AbstractGradientsAndHessian
    stepControl::AbstractStepControl
    convergenceCheck::AbstractConvergenceCheck
    trustRadiusUpdate::AbstractTrustRadiusUpdate
end

struct DataOfFormersStandardOptimizer <: ResetData
    energy::Float64
    xs::AbstractCoordinates
    gradients::Array{Float64}
end

getEnergy(optimizer::StandardOptimizer) = optimizer.energy
getFormerEnergy(optimizer::StandardOptimizer) = optimizer.formerEnergy
getCartesians(optimizer::StandardOptimizer) = getCartesians(optimizer.xs)
getFormerCartesians(optimizer::StandardOptimizer) = getCartesians(optimizer.formerXs)
getChange(optimizer::StandardOptimizer) = getChange(optimizer, getPrimitiveValues(optimizer.formerXs))
getChange(optimizer::StandardOptimizer, primitiveValues::Array{Float64}) = getChangeFromPrimitives(optimizer.xs, primitiveValues)
getPrimitiveValues(optimizer::StandardOptimizer) = getPrimitiveValues(optimizer.xs)
transformCartesianGradients(optimizer::StandardOptimizer, cartesianG::Array{Float64}) = transformCartesianGradients(optimizer.xs, cartesianG)
transformCartesianHessian(optimizer::StandardOptimizer, cartesianH::AbstractArray{Float64, 2}, cartesianG::Array{Float64}) = transformCartesianHessian(optimizer.xs, cartesianH, cartesianG, getCartesians(optimizer))
getCoordinateSystem(optimizer::StandardOptimizer) = optimizer.xs
getGradients(optimizer::StandardOptimizer) = optimizer.gradients
getActiveGradients(optimizer::StandardOptimizer) = gradientsToActiveSpace(optimizer.xs, optimizer.gradients)
getHessian(optimizer::StandardOptimizer) = optimizer.hessian
getActiveHessian(optimizer::StandardOptimizer) = weightsToHessian(optimizer.xs, hessianToActiveSpace(optimizer.xs, optimizer.hessian))


function takeSnapShot(optimizer::StandardOptimizer) 
    DataOfFormersStandardOptimizer(optimizer.formerEnergy, deepcopy(optimizer.formerXs), optimizer.formerGradients)
end

function resetToSnapShot!(optimizer::StandardOptimizer, data::DataOfFormersStandardOptimizer)
    optimizer.energy, optimizer.formerEnergy = optimizer.formerEnergy, data.energy
    optimizer.xs, optimizer.formerXs = deepcopy(optimizer.formerXs), deepcopy(data.xs)
    optimizer.gradients, optimizer.formerGradients = deepcopy(optimizer.formerGradients), deepcopy(data.gradients)
end

function resetCoordinateSystem!(optimizer::StandardOptimizer) 
    resetCoordinateSystem!(optimizer.xs)
    initialize!(optimizer)
end

function isConvergenceReached(optimizer::StandardOptimizer) 
    isConvergenceReached(optimizer.convergenceCheck, optimizer)
end

function getNewEnergyAndGradients!(optimizer::StandardOptimizer)
    optimizer.formerEnergy, optimizer.formerGradients = optimizer.energy, optimizer.gradients
    optimizer.energy, optimizer.gradients = getEnergyAndGradients(optimizer.gradientsAndHessian, optimizer)
end

function initialize!(self::StandardOptimizer)
    getNewEnergyAndGradients!(self)
    self.hessian = getInitialHessian(self.initialHessian, self)
    @info "The initial Hessian:" H=self.hessian
    notify!(self.gradientsAndHessian, self)
end

function takeStep(optimizer::StandardOptimizer)
    seconds = @elapsed Δxs = takeStep!(optimizer.stepControl, optimizer)
    @info @sprintf("Took %.2f seconds to caclulate the step", seconds)
    Δxs
end

function applyChange!(optimizer::StandardOptimizer, Δxs::Array{Float64})
    optimizer.change = Δxs
    optimizer.formerXs = deepcopy(optimizer.xs)
    
    seconds = @elapsed applyChange!(optimizer.xs, Δxs)
    @info @sprintf("Took %.2f seconds to apply the change", seconds)
end

function updateTrustRadius!(optimizer::StandardOptimizer)
    updateTrustRadius(optimizer.trustRadiusUpdate, optimizer.stepControl, getEnergy(optimizer) - getFormerEnergy(optimizer))
end

function getNewHessian!(optimizer::StandardOptimizer)
    optimizer.formerHessian = optimizer.hessian
    optimizer.hessian = getHessian(optimizer.gradientsAndHessian, optimizer)
end

getAnalyticHessian(self::StandardOptimizer) = getAnalyticHessian(self.gradientsAndHessian, self)

function executeAfterStep!(self::StandardOptimizer) 
    notify!(self.gradientsAndHessian, self)
end

function StandardOptimizer(coords::AbstractCoordinates, gradientsAndHessian::AbstractGradientsAndHessian, stepControl::AbstractStepControl, convergenceCheck::AbstractConvergenceCheck; initialHessian::AbstractInitialHessian=IdentityHessian(), trustRadiusUpdate::AbstractTrustRadiusUpdate=NoTrustRadiusUpdate()) 
    self = StandardOptimizer(
        coords,
        EmptyCoordinates(),
        Inf,
        Inf,
        Array{Float64}(undef, 0),
        Array{Float64}(undef, 0),
        Array{Float64}(undef, 0),
        Array{Float64}(undef, 0, 0),
        Array{Float64}(undef, 0, 0),
        initialHessian,
        gradientsAndHessian,
        stepControl,
        convergenceCheck,
        trustRadiusUpdate
    )
    self
end