using LinearAlgebra
using LightGraphs
using DataStructures

function buildBondGraph(symbols::Array{String}, coordinates::Array{Float64})
    numAtoms = length(coordinates)÷3
    bondGraph = SimpleGraph(numAtoms)
    for i ∈ 1:numAtoms
        a = coordinates[((i-1)*3)+1:((i-1)*3)+3]
        for j ∈ i+1:numAtoms
            b = coordinates[((j-1)*3)+1:((j-1)*3)+3]
            dist = norm(a - b)
            if dist < COVALENT_THRESHOLD*(getCovalentRadius(symbols[i])+getCovalentRadius(symbols[j]))
                add_edge!(bondGraph, i, j)
            end
        end
    end
    bondGraph
end

function getBonds(bondGraph::SimpleGraph{Int64})
    bonds = Array{Tuple{Int, Int}}(undef, 0)
    for edge ∈ edges(bondGraph)
        push!(bonds, (src(edge), dst(edge)))
    end
    bonds
end

function getAngles(bondGraph::SimpleGraph{Int64})
    angles = Array{Tuple{Int, Int, Int}}(undef, 0)
    for v ∈ vertices(bondGraph)
        n = neighbors(bondGraph, v)
        for i ∈ eachindex(n)
            leftIndex = n[i]
            for rightIndex ∈ n[i+1:end]
                push!(angles, (leftIndex, v , rightIndex))
            end
        end
    end
    angles
end

function getDihedrals(bondGraph::SimpleGraph{Int64})
    dihedrals = Array{Tuple{Int, Int, Int, Int}}(undef, 0)
    for e ∈ edges(bondGraph)
        midLeft = src(e)
        midRight = dst(e)
        for outerLeft ∈ neighbors(bondGraph, midLeft)
            if outerLeft == midRight; continue; end
            for outerRight ∈ neighbors(bondGraph, midRight)
                if outerRight == midLeft || outerRight == outerLeft; continue; end
                push!(dihedrals, (outerLeft, midLeft, midRight, outerRight))
            end
        end
    end
    dihedrals
end

function getMolecules(bondGraph::SimpleGraph{Int64})
    connected_components(bondGraph)
end

function getLongestShortestPathFromVertex(bondGraph::SimpleGraph{Int}, vertex::Int)
    bfsParents = bfs_parents(bondGraph, vertex)
    longestPath = Array{Int}(undef, 0)

    for i in eachindex(bfsParents)
        if i == vertex || bfsParents[i] == 0; continue; end
        currentPath = [i]
        d = bfsParents[i]
        while d != vertex
            push!(currentPath, d)
            d = bfsParents[d]
        end
        push!(currentPath, d)
        
        if length(currentPath) > length(longestPath)
            longestPath = currentPath
        end
    end
    longestPath
end

function getLongestShortestPathAlongPath(bondGraph::SimpleGraph{Int}, path)
    longestPath, start = Array{Int}(undef, 0), 0
    for p ∈ path
        currentPath = getLongestShortestPathFromVertex(bondGraph, p)
        if length(currentPath) > length(longestPath)
            longestPath, start = currentPath, p
        end
    end

    longestPath = longestPath[1] == start ? longestPath : reverse(longestPath)
    edges = [(longestPath[i], longestPath[i+1]) for i ∈ 1:length(longestPath)-1]

    (longestPath, edges)
end

hasEdges(bondGraph::SimpleGraph{Int}) = ne(bondGraph) != 0

function reorderSymbols(newOrder::Array{Int}, symbols::Array{String})
    [symbols[i] for i ∈ newOrder]
end

function reorderCoordinates(newOrder::Array{Int}, coordinates::Array{Float64})
    newCoordinates = Array{Float64}(undef, length(coordinates))
    for (i, o) ∈ enumerate(newOrder)
        newCoordinates[(i-1)*3+1:(i-1)*3+3] = coordinates[(o-1)*3+1:(o-1)*3+3]
    end
    newCoordinates
end

function reorderConstraints(newOrder::Array{Int}, constraints::SetOfConstraints)
    n = length(newOrder)
    orderMapper = Array{Int64}(undef, n)
    for i in 1:n
        orderMapper[newOrder[i]] = i
    end
    reorderConstraints(constraints, orderMapper)
end

function reorderBondGraph(newOrder::Array{Int}, bondGraph::SimpleGraph{Int})
    newBondGraph = SimpleGraph(nv(bondGraph))
    biNewOrder = Array{Int}(undef, length(newOrder))

    for i in 1:length(biNewOrder)
        biNewOrder[newOrder[i]] = i
    end

    for edge in edges(bondGraph)
        s, d = biNewOrder[src(edge)], biNewOrder[dst(edge)]
        add_edge!(newBondGraph, s, d)
    end
    newBondGraph
end

function getNewOrder(bondGraph::SimpleGraph{Int})
    newOrder = OrderedSet{Int}()
    vertexCheckList = fill(false, nv(bondGraph))

    while hasEdges(bondGraph)
        path, edges = getLongestShortestPathAlongPath(bondGraph, length(newOrder) != 0 ? newOrder : vertices(bondGraph))
        union!(newOrder, path)
        for (s,d) ∈ edges
            rem_edge!(bondGraph, s, d)
            vertexCheckList[s] = vertexCheckList[d] = true
        end
    end

    for i ∈ eachindex(vertexCheckList)
        if !vertexCheckList[i]
            push!(newOrder, i)
        end
    end
    newOrder
end

function getReorderedSequence(bondGraph::SimpleGraph{Int})
    #bondGraphCopy = copy(bondGraph)
    newOrder = Array{Int}(undef, 0)

    for molecule ∈ connected_components(bondGraph)
        subGraph, vertexList = induced_subgraph(bondGraph, molecule)
        subOrder = getNewOrder(subGraph)
        append!(newOrder, [vertexList[no] for no ∈ subOrder])
    end

    newOrder
end

function getReorderedSequenceAndGraph(symbols::Array{String}, coordinates::Array{Float64})
    bondGraph = buildBondGraph(symbols, coordinates)
    
    newOrder = getReorderedSequence(bondGraph)

    newOrder, reorderBondGraph(newOrder, bondGraph)
end