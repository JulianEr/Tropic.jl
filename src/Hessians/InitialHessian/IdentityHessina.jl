struct IdentityHessian <: AbstractInitialHessian end

function getInitialHessian(::IdentityHessian, optimizer::AbstractOptimizer)
    identityHessian(getCoordinateSystem(optimizer))
end