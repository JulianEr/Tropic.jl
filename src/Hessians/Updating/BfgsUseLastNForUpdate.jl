mutable struct BfgsUseLastNForUpdate <: AbstractHessianUpdate 
    valuesMemory::Array{Array{Float64}}
    gradientsMemory::Array{Array{Float64}}
    updateLimitMax::Float64
    updateLimitScale::Float64
    updateDenominatorLimit::Float64
    updateMaxmimumChangeLimit::Float64
    N::Int64
end

BfgsUseLastNForUpdate(N::Int64=4; updateLimitMax::Float64=1.0, updateLimitScale::Float64=0.5, updateDenominatorLimit::Float64=1e-7, updateMaxmimumChangeLimit::Float64=0.5) = BfgsUseLastNForUpdate(Array{Array{Float64}}(undef, 0), Array{Array{Float64}}(undef, 0), updateLimitMax, updateLimitScale, updateDenominatorLimit, updateMaxmimumChangeLimit, N)

function bfgs_change(Δxs::Array{Float64}, Δg::Array{Float64}, H::AbstractArray{Float64, 2}, ΔgΔxs::Float64)
    term1 = (Δg*Δg') / ΔgΔxs
    term2 = (H*Δxs*Δxs'*H) / (Δxs'*H*Δxs)
    term1 - term2
end

function notify!(self::BfgsUseLastNForUpdate, optimizer::AbstractOptimizer)
    pushfirst!(self.valuesMemory, getPrimitiveValues(optimizer))
    pushfirst!(self.gradientsMemory, getGradients(optimizer))
end

function updateHessian(self::BfgsUseLastNForUpdate, optimizer::AbstractOptimizer)
    H_new = deepcopy(getHessian(optimizer))
    count = 0
    for (value, gradient) in zip(self.valuesMemory, self.gradientsMemory)
        Δxs = getChange(optimizer, value)
        Δg = getGradients(optimizer) - gradient
        ΔgΔxs, ΔxsΔxs = Δg'*Δxs, Δxs'*Δxs

        if abs(ΔgΔxs) < self.updateDenominatorLimit || abs(ΔxsΔxs) < self.updateDenominatorLimit || maximum(abs.(Δxs)) > self.updateMaxmimumChangeLimit
            @warn "Either one of the denominators is too small (ΔgΔxs or ΔxsΔxs) or the maximum step size is too large\n$(abs(ΔgΔxs)) < $(self.updateDenominatorLimit) || $(abs(ΔxsΔxs)) < $(self.updateDenominatorLimit) || $(maximum(abs.(Δxs))) > $(self.updateMaxmimumChangeLimit)"
            continue
        end

        H_change = bfgs_change(Δxs, Δg, H_new, ΔgΔxs)
        for i in axes(H_new, 1)
            for j in axes(H_new, 2)
                limit = max(abs(H_new[i,j]*self.updateLimitScale), self.updateLimitMax)
                if abs(H_change[i,j]) > limit
                    H_change[i,j] = sign(H_change[i,j])*limit
                end
            end
        end
        H_new += H_change
        count += 1
        if count >= self.N
            break
        end
    end
    0.5*(H_new+H_new')
end