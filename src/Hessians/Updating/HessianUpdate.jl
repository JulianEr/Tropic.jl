mutable struct HessianUpdate <: AbstractHessianUpdate
    update::Function
    lastValues::Array{Float64}
    lastGradients::Array{Float64}
    HessianUpdate(update::Function) = new(update, Array{Float64}(undef, 0), Array{Float64}(undef, 0))
end

function notify!(self::HessianUpdate, optimizer::AbstractOptimizer) 
    self.lastValues = getPrimitiveValues(optimizer)
    self.lastGradients = getGradients(optimizer)
end

function updateHessian(self::HessianUpdate, optimizer::AbstractOptimizer)
    @debug "Primitives now: " p=getPrimitiveValues(optimizer)
    @debug "Last Primitives: " p=self.lastValues
    Δxs = getChange(optimizer, self.lastValues)
    @debug "Change I assume: " c=Δxs
    @debug "Change from coordinate System: " c=getChange(optimizer)
    Δg = getGradients(optimizer) - self.lastGradients

    H = getHessian(optimizer)
    self.update(Δxs, Δg, H)
end