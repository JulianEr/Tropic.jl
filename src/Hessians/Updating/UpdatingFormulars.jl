function combineUpdates(Δxs::Array{Float64}, Δg::Array{Float64}, H::AbstractArray{Float64, 2}, func1::Function, func2::Function)
    θ = ((Δg - H*Δxs)'*Δxs)^2 / (norm(Δg - H*Δxs)^2 * norm(Δxs)^2)
    θ*func1(Δxs, Δg, H) + (1 - θ)*func2(Δxs, Δg, H)
end

function bfgs(Δxs::Array{Float64}, Δg::Array{Float64}, H::AbstractArray{Float64, 2})
    term1 = (Δg*Δg') / (Δg'*Δxs)
    term2 = (H*Δxs*Δxs'*H) / (Δxs'*H*Δxs)
    H += term1 - term2
    Hermitian(0.5*(H+H'))
end

function sr1(Δxs::Array{Float64}, Δg::Array{Float64}, H::AbstractArray{Float64, 2})
    H += (((Δg - H*Δxs)*(Δg - H*Δxs)') ./ ((Δg - H*Δxs)'*Δxs))
    Hermitian(0.5*(H+H'))
end

function psb(Δxs::Array{Float64}, Δg::Array{Float64}, H::AbstractArray{Float64, 2})
    H += (((Δg - H*Δxs)*Δxs' + Δxs*(Δg - H*Δxs)') ./ (Δxs'*Δxs)) - ((((Δg - H*Δxs)'*Δxs)*Δxs*Δxs') ./ (Δxs'*Δxs)^2)
    Hermitian(0.5*(H+H'))
end

sr1_psb_update(Δxs::Array{Float64}, Δg::Array{Float64}, H::AbstractArray{Float64, 2}) = combineUpdates(Δxs, Δg, H, sr1, psb)

sr1_bfgs_update(Δxs::Array{Float64}, Δg::Array{Float64}, H::AbstractArray{Float64, 2}) = combineUpdates(Δxs, Δg, H, sr1, bfgs)