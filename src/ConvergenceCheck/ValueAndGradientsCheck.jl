struct ValueAndGradientsCheck <: AbstractConvergenceCheck
    gradientMAXThreshold::Float64
    gradientRMSThreshold::Float64
    functionValueThreshold::Float64
end

ValueAndGradientsCheck(maxGrad::Float64 = 1.0e-3, rmsGrad::Float64 = 5.0e-4) = ValueAndGradientsCheck(maxGrad, rmsGrad, 1.0e-6)

function isConvergenceReached(self::ValueAndGradientsCheck, optimizer::AbstractOptimizer)
    ΔF = getEnergy(optimizer) - getFormerEnergy(optimizer)
    gMax, gRMS = let g = getActiveGradients(optimizer)
        (maximum(abs.(g)), sqrt(1.0/length(g)*dot(g,g)))
    end

    @info(@sprintf(
"Checking For Convergence:
Convergence Criteria: MAX(g) = %10.5e RMS(g) = %10.5e; f = %10.5e
Values this step:     MAX(g) = %10.5e RMS(g) = %10.5e; f = %10.5e", self.gradientMAXThreshold, self.gradientRMSThreshold, self.functionValueThreshold, gMax, gRMS, ΔF
    ))
    
    if gRMS > self.gradientRMSThreshold || abs(gMax) > self.gradientMAXThreshold
        return false
    elseif abs(ΔF) > self.functionValueThreshold
        return false
    end
    
    return true
end