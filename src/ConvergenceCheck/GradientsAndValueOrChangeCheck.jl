struct GradientsAndValueOrChangeCheck <: AbstractConvergenceCheck
    gradientMAXThreshold::Float64
    gradientRMSThreshold::Float64
    changeMAXThreshold::Float64
    changeRMSThreshold::Float64
    functionValueThreshold::Float64
end

GradientsAndValueOrChangeCheck(
    gradientMAXThreshold::Float64 = 1.0e-3, gradientRMSThreshold::Float64 = 5.0e-4,
    changeMAXThreshold::Float64 = 1.0e-3, changeRMSThreshold::Float64 = 5.0e-4 
    ) = GradientsAndValueOrChangeCheck(gradientMAXThreshold, gradientRMSThreshold, changeMAXThreshold, changeRMSThreshold, 1.0e-6)

function isConvergenceReached(self::GradientsAndValueOrChangeCheck, optimizer::AbstractOptimizer)
    ΔF =  getEnergy(optimizer) - getFormerEnergy(optimizer) 
    ΔxMax,  ΔxRMS = let Δx = getChange(optimizer)
        (maximum(abs.(Δx)), sqrt(1.0/length(Δx)*dot(Δx,Δx)))
    end

    gMax, gRMS = let g = getActiveGradients(optimizer)
        (maximum(abs.(g)), sqrt(1.0/length(g)*dot(g,g)))
    end

    @info(@sprintf(
"Checking For Convergence!
Convergence Criteria: MAX(Δx) = %10.5e; RMS(Δx) = %10.5e; MAX(g) = %10.5e RMS(g) = %10.5e; f = %10.5e
Values this step:     MAX(Δx) = %10.5e; RMS(Δx) = %10.5e; MAX(g) = %10.5e RMS(g) = %10.5e; f = %10.5e", 
    self.changeMAXThreshold,  self.changeRMSThreshold, self.gradientMAXThreshold, self.gradientRMSThreshold, self.functionValueThreshold, ΔxMax,  ΔxRMS, gMax, gRMS, ΔF))

    if gRMS > self.gradientRMSThreshold || abs(gMax) > self.gradientMAXThreshold
        return false
    elseif abs(ΔF) > self.functionValueThreshold && (ΔxRMS > self.changeRMSThreshold || abs(ΔxMax) > self.changeMAXThreshold)
        return false
    end
    
    return true
end