using LinearAlgebra
using Printf

include("OnlyGradientsCheck.jl")
include("ValueAndGradientsCheck.jl")
include("GradientsAndChangeCheck.jl")
include("ValueGradientsAndChangeCheck.jl")
include("GradientsAndValueOrChangeCheck.jl")
