struct GradientsAndChangeCheck <: AbstractConvergenceCheck
    gradientMAXThreshold::Float64
    gradientRMSThreshold::Float64
    changeMAXThreshold::Float64
    changeRMSThreshold::Float64
end

GradientsAndChangeCheck(
    gradientMAXThreshold::Float64 = 4.5e-4, gradientRMSThreshold::Float64 = 3.0e-4,
    changeMAXThreshold::Float64 = 1.8e-3
    ) = GradientsAndChangeCheck(gradientMAXThreshold, gradientRMSThreshold, changeMAXThreshold, 1.2e-3)

function isConvergenceReached(self::GradientsAndChangeCheck, optimizer::AbstractOptimizer)
    ΔxMax,  ΔxRMS = let Δx = getChange(optimizer)
        (maximum(abs.(Δx)), sqrt(1.0/length(Δx)*dot(Δx,Δx)))
    end

    gMax, gRMS = let g = abs.(getActiveGradients(optimizer))
        (maximum(abs.(g)), sqrt(1.0/length(g)*dot(g,g)))
    end

    @info(@sprintf(
"Checking For Convergence!
Convergence Criteria: MAX(Δx) = %10.5e; RMS(Δx) = %10.5e; MAX(g) = %10.5e RMS(g) = %10.5e
Values this step:     MAX(Δx) = %10.5e; RMS(Δx) = %10.5e; MAX(g) = %10.5e RMS(g) = %10.5e", 
    self.changeMAXThreshold,  self.changeRMSThreshold, self.gradientMAXThreshold, self.gradientRMSThreshold, ΔxMax,  ΔxRMS, gMax, gRMS))

    if gRMS > self.gradientRMSThreshold || abs(gMax) > self.gradientMAXThreshold
        return false
    elseif ΔxRMS > self.changeRMSThreshold || abs(ΔxMax) > self.changeMAXThreshold
        return false
    end
    return true
end
