mutable struct TrustRegionMethod <: AbstractStepControl 
    trustRadius::Float64
    ε₁::Float64
    ε₂::Float64
    saddlePointOrder::Int64
    lastNorm::Float64
    modelEnergyChange::Float64
end

TrustRegionMethod(trustRadius::Float64 = 0.3, ε₁::Float64 = 0.1, ε₂::Float64 = 1.e-6) = TrustRegionMethod(trustRadius, ε₁, ε₂, 0, Inf, Inf)
TrustRegionMethod(trustRadius::Float64, saddlePointOrder::Int64, ε₁::Float64 = 0.1, ε₂::Float64 = 1.e-6) = TrustRegionMethod(trustRadius, ε₁, ε₂, saddlePointOrder,  Inf, Inf)

getPredictedEnergyChnage(self::TrustRegionMethod) = self.modelEnergyChange

function chooseλ(saddlePointOrder::Int64, hessianEigenvalues::Array{Float64})::Float64
    if saddlePointOrder == 0
        if  hessianEigenvalues[1] > -1e-7
            return 0.0
        else
            firstEv = hessianEigenvalues[1]
            secondEv = hessianEigenvalues[2]
            return firstEv - abs(firstEv - secondEv)/2.0
        end 
    elseif saddlePointOrder == length(hessianEigenvalues)
        if hessianEigenvalues[end] < 1e-7
            return 0.0
        else
            lastSaddlePoint = hessianEigenvalues[end]
            beforLastToSaddlePoint = hessianEigenvalues[end-1]
            return lastSaddlePoint + abs(lastSaddlePoint - beforLastToSaddlePoint)/2.0
        end
    elseif hessianEigenvalues[saddlePointOrder] < 1e-7
        if hessianEigenvalues[saddlePointOrder+1] > -1e-7
            return 0.0
        elseif hessianEigenvalues[saddlePointOrder+1] < 1e-7
            saddlePoint = hessianEigenvalues[saddlePointOrder]
            nextToSaddlePoint = hessianEigenvalues[saddlePointOrder+1]
            return saddlePoint - abs(saddlePoint - nextToSaddlePoint)/2.0
        end
    else
        saddlePoint = hessianEigenvalues[saddlePointOrder]
        nextToSaddlePoint = hessianEigenvalues[saddlePointOrder+1]
        return saddlePoint + abs(nextToSaddlePoint - saddlePoint)/2.0
    end
end

function possibleDevideBy0(x::Array{Float64}, y::Array{Float64}, f::Function, ɛ::Float64=1e-7)
    z = Array{Float64}(undef, length(y))
    for i ∈ eachindex(y)
        z[i] = if abs(y[i]) > ɛ
            f(x[i],y[i])
        else
            0.0
        end
    end
    z
end

function getΔxs(f::Array{Float64}, h::Array{Float64}, λ::Float64)
    possibleDevideBy0(f, h, (x,y) -> x/(y-λ))
end

#TODO rename
function getNominatorVectorOf∂norm_∂λ(f::Array{Float64}, h::Array{Float64}, λ::Float64)
    possibleDevideBy0(f, h, (x,y) -> (x^2.0)/((y - λ)^3.0))
end

function takeStep!(self::TrustRegionMethod, optimizer::AbstractOptimizer)
    gradients, hessian = getActiveGradients(optimizer), getActiveHessian(optimizer)
    λᵢ = 0.0
    i::Int64 = 0

    h, U = eigen(hessian)
    f = transpose(U) * gradients
    
    λᵢ = chooseλ(self.saddlePointOrder, h)
    λᵢ₋₁ = λᵢ - 10.0 * self.ε₂
    Δxs = -U * getΔxs(f, h, λᵢ)

    @debug "Values for TRM:" λᵢ=λᵢ Δxs=Δxs f=f h=h U=U

    self.lastNorm = norm(Δxs)
    @info("\tstart with: λᵢ = $(λᵢ); λᵢ₋₁=$(λᵢ₋₁); |Δxs| = $(self.lastNorm); abs(|Δxs| - τ)=$(abs(self.lastNorm-self.trustRadius)); |λᵢ₋₁ - λ|=$(λᵢ₋₁ - λᵢ)")

    if self.lastNorm > self.trustRadius# && abs(λᵢ) < 1.0e-10
        while abs(self.lastNorm/self.trustRadius - 1.0) > self.ε₁ && abs(λᵢ₋₁ - λᵢ) > self.ε₂
            ∂norm_∂λ = sum((f .^ 2.0) ./ ((h .- λᵢ) .^ 3.0))/self.lastNorm
    
            λᵢ₋₁ = λᵢ
            λᵢ -= self.lastNorm/self.trustRadius*(self.lastNorm - self.trustRadius)/∂norm_∂λ
            
            Δxs = -U * getΔxs(f, h, λᵢ)
            self.lastNorm = norm(Δxs)
            @info("\tmicro step $(i): λᵢ = $(λᵢ); λᵢ₋₁=$(λᵢ₋₁); |Δxs| = $(self.lastNorm); abs(|Δxs| - τ)=$(abs(self.lastNorm-self.trustRadius)); |λᵢ₋₁ - λ|=$(λᵢ₋₁ - λᵢ)")
            
            i += 1
            if i >= 200
                error("$(λᵢ) $(norm(Δxs)) $(Δxs)")
            end
        end
    end
     
    self.modelEnergyChange = transpose(gradients)*Δxs + 0.5*transpose(Δxs)*hessian*Δxs
    
    Δxs
end