mutable struct SimpleModHessianNewton <: AbstractStepControl 
    saddlePointOrder::Int64
    modelEnergyChange::Float64
end

SimpleModHessianNewton(saddlePointOrder::Int64 = 0) = SimpleModHessianNewton(saddlePointOrder, Inf)

getPredictedEnergyChnage(self::SimpleModHessianNewton) = self.modelEnergyChange

function takeStep!(self::SimpleModHessianNewton, optimizer::AbstractOptimizer)
    gradients, hessian = getActiveGradients(optimizer), getActiveHessian(optimizer)
    h, U = eigen(hessian)
    f = transpose(U)*gradients

    for i ∈ 1:self.saddlePointOrder
        f[i] = abs(h[i]) > 1.0e-6 ? f[i] : 0
        h[i] = -abs(h[i])
    end
    for i ∈ self.saddlePointOrder+1:length(h)
        f[i] = abs(f[i]) > 1.0e-6 ? f[i] : 0
        h[i] = abs(h[i])
    end

    Δx = -U*(f ./ h)
    self.modelEnergyChange = transpose(gradients)*Δx + 0.5*transpose(Δx)*hessian*Δx

    Δx
end
