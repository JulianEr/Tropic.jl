function getLowestNonZeroEigenvalue(νs)
    for (i, v) in enumerate(νs[1,:])
        if abs(v) > 0.01
            return i
        end
    end
end

function rfo(gradients::Array{Float64}, hessian::AbstractArray{Float64, 2}, useNewtonModelEnergyChange::Bool = false)
    h, U = eigen(hessian)
    f = transpose(U)*gradients

    f[abs.(h).<1.0e-6] .= 0

    rfo = getRfoMatrix(f, h)

    λs, νs = eigen(rfo)

    @debug "Eigenvalues and first elements of eigenvectors:" λs=λs νs_firstRow=νs[1,:]

    n = getLowestNonZeroEigenvalue(νs)

    @info "Choosing Eigenvalue and vec(up to 1:n):" i=n λs=λs[1:n]

    if λs[n] >= 0
        @warn "The eigenvalue to the eigenvector starting with a value closest to 1 is not negative. Something went wrong" index = n λ=λs[1:n] ν_starts=νs[1,1:n] ν=νs[:,n]
    end

    Δx = U * (νs[2:end, n] ./ νs[1, n])

    if useNewtonModelEnergyChange
        Δx, (transpose(gradients)*Δxs + 0.5*transpose(Δxs)*hessian*Δxs) / (1.0 + transpose(Δxs)*Δxs)
    else 
        Δx, 0.5*(λs[n]/(νs[1,n]^2.0))
    end
end

# TODO this is not how it is intended to happen
# function partitionedRfo(orderOfSaddlePoint::Int64, gradients::Array{Float64}, hessian::AbstractArray{Float64, 2})
#     h, U = eigen(hessian)
#     f = transpose(U)*gradients

#     f[abs.(h).<1.0e-6] .= 0

#     rfoMax, rfoMin = getMaxAndMinRFO(orderOfSaddlePoint, f, h)
    
#     λs_max, νs_max = eigen(rfoMax)
#     λs_min, νs_min = eigen(rfoMin)

#     (U * [νs_max[2:end, end] ./ νs_max[1, end];
#          νs_min[2:end, 1]   ./ νs_min[1, 1]], 0.5*((λs_max[end]/(νs_max[1,end]^2.0)) + (λs_min[1]/(νs_min[1,1])^2.0)))
# end

function getRfoMatrix(f::Array{Float64}, h::Array{Float64})
    Hermitian([0.0 transpose(f);
     f   Diagonal(h)])
end

# function getMaxAndMinRFO(orderOfSaddlePoint::Int64, f::Array{Float64}, h::Array{Float64})
#     (Hermitian([0.0                     transpose(f[1:orderOfSaddlePoint]);
#       f[1:orderOfSaddlePoint] Diagonal(h[1:orderOfSaddlePoint])]),
#      Hermitian([0.0                         transpose(f[orderOfSaddlePoint+1:end]);
#       f[orderOfSaddlePoint+1:end] Diagonal(h[orderOfSaddlePoint+1:end])]))
# end