struct Psi4Engine <: AbstractEngine
    path::String
    memoryInGb::Int64
    basisSet::String
    method::String
    charge::Int64
    multiplicity::Int64
    cores::Int64
end

const write_python_matrix = "def writeToFile(matrix, filename):
\twith open(filename, \"w\") as oFile:
\t\tfor i in range(matrix.shape[0]):
\t\t\tif matrix.ndim == 1:
\t\t\t\toFile.write(f\"{matrix[i]:25.15f}\")
\t\t\telse:
\t\t\t\tfor j in range(matrix.shape[1]):
\t\t\t\t\toFile.write(f\"{matrix[i, j]:25.15f}\")
\t\t\toFile.write(\"\\n\")\n\n"

Psi4Engine(; path::String="psi4", memoryInGb::Int64=0, basisSet::String="sto-3g", method::String="scf", charge::Int64=0, multiplicity::Int64=1, cores::Int64=2) = Psi4Engine(path, memoryInGb, basisSet, method, charge, multiplicity, cores)

## TODO find out why it sometimes dies
## If dynamic error occurs try to add
## first "damping_percentage 20"
## then  "basis_guess true"
## then  "soscf_max_iter 20"
## then increase damping to 40
## exmaple how the worst case scenario looks:
## set {
##     basis sto-3g
##     soscf_max_iter 20
##     basis_guess true
##     damping_percentage 40
## }
## see https://forum.psicode.org/t/iterations-do-not-converge/984/4
function makeCallAndReturnOutputPsi4(cmd::Cmd)
    o = Pipe()
    e = Pipe()
      
    run(pipeline(ignorestatus(cmd), stdout=o, stderr=e))
    close(o.in)
    close(e.in)

    err = String(read(e))
    if !isempty(err)
        error("I could not execute psi4! Something went wrong. Are you sure you provided the exact path and psi4 was fed with the right commands? Check the \"input\" file in $(pwd()). $(err)")
    end
      
    String(read(o))
end

function makeCalculation(::Psi4Engine, call::Function)
    dirName = mktempdir("."; prefix="psi4_", cleanup=false)
    root = pwd()
    cd(dirName)

    result = call()

    cd(root)
    rm(dirName, recursive=true)

    result
end

function getEnergyFromFilePsi4()
    fileContent = open("energy") do iFile
        read(iFile, String)
    end

    parse(Float64, fileContent)
end

function getGradientsFromFileAndRotationMatrixPsi4(coordinates::Array{Float64})

    coords2D = toBaryCenter(cartesianFlattenTo2d(copy(coordinates)))
    molecule = readdlm("geometry")
    
    _, q = getBestFittingQuaternion2D(coords2D, molecule)

    gradients_psi4 = readdlm("gradients")
    @info("q_0 in gradient calculation to get gradient to current structure: $(q[1])
Center Of Mass for the gradients: $(getBaryCenter(gradients_psi4))")

    if abs(q[1] - 1) < 1e-8
        return Array{Float64}(reshape(gradients_psi4', length(gradients_psi4))), Diagonal(fill(1.0, 3))
    end
    U = getRotationMartixFromQuaternion(q)
    gradients =  gradients_psi4*U'
    
    Array{Float64}(reshape(gradients', length(gradients))), U
end

function getHessianFromFilePsi4(U::AbstractArray{Float64, 2})
    hessian = readdlm("hessian")
    for i ∈ 1:size(hessian, 1)÷3
        for j ∈ 1:size(hessian, 2)÷3
            hessian[3*i-2:3*i, 3*j-2:3*j] = U*hessian[3*i-2:3*i, 3*j-2:3*j]*U'
        end
    end
    Array{Float64, 2}(hessian)
end

function parseEnergyAndGradients(::Psi4Engine, coordinates::Array{Float64})
    getEnergyFromFilePsi4(), getGradientsFromFileAndRotationMatrixPsi4(coordinates)[1]
end

function parseGradientsAndHessian(::Psi4Engine, coordinates::Array{Float64})
    g, U = getGradientsFromFileAndRotationMatrixPsi4(coordinates)
    g, getHessianFromFilePsi4(U)
end

function parseEnergyGradientsAndHessian(self::Psi4Engine, coordinates::Array{Float64})
    g, H = parseGradientsAndHessian(self, coordinates)
    getEnergyFromFilePsi4(), g, H
end

function getEnergyAndGradients(self::Psi4Engine, symbols::Array{String}, coordinates::Array{Float64})
    function call()
        writeInputFileGradients(self, symbols, coordinates)
        _ = makeCallAndReturnOutputPsi4(`$(self.path) -n $(self.cores) input output`)
        parseEnergyAndGradients(self, coordinates)
    end

    seconds = @elapsed result = makeCalculation(self, call)
    @info @sprintf("Took %.2f seconds to calculate energy and gradients", seconds)
    result
end

function getGradientsAndHessian(self::Psi4Engine, symbols::Array{String}, coordinates::Array{Float64})
    function call()
        writeInputFileHessian(self, symbols, coordinates)
        _ = makeCallAndReturnOutputPsi4(`$(self.path) input output`)

        parseGradientsAndHessian(self, coordinates)
    end

    seconds = @elapsed result = makeCalculation(self, call)
    @info @sprintf("Took %.2f seconds to calculate gradients and Hessian", seconds)
    result
end

function writeInputFileEnergy(self::Psi4Engine, symbols::Array{String}, coordinates::Array{Float64})
    input = writeCartesianInputAndParameters(self, symbols, coordinates)
    input *= "_, w = energy('$(self.method)', return_wfn=True)\nclean()\n\n"
    input *= write_python_matrix
    input *= "writeToFile(np.array([w.energy()]), \"energy\")\n"

    open("input", "w") do oFile
        write(oFile, input)
    end
end

function writeInputFileGradients(self::Psi4Engine, symbols::Array{String}, coordinates::Array{Float64})
    input = writeCartesianInputAndParameters(self, symbols, coordinates)
    input *= "_, w = gradient('$(self.method)', return_wfn=True)\nclean()\n\n"
    input *= write_python_matrix
    input *= "writeToFile(np.array([w.energy()]), \"energy\")\nwriteToFile(np.array(mol.geometry()), \"geometry\")\nwriteToFile(np.array(w.gradient()), \"gradients\")\n"

    open("input", "w") do oFile
        write(oFile, input)
    end
end

function writeInputFileHessian(self::Psi4Engine, symbols::Array{String}, coordinates::Array{Float64})
    input = writeCartesianInputAndParameters(self, symbols, coordinates)
    input *= "_, w = hessian('$(self.method)', return_wfn=True)\nclean()\n\n"
    input *= write_python_matrix
    input *= "writeToFile(np.array([w.energy()]), \"energy\")\nwriteToFile(np.array(mol.geometry()), \"geometry\")\nwriteToFile(np.array(w.gradient()), \"gradients\")\nwriteToFile(np.array(w.hessian()), \"hessian\")"

    open("input", "w") do oFile
        write(oFile, input)
    end
end

function writeCartesianInputAndParameters(self::Psi4Engine, symbols::Array{String}, coordinates::Array{Float64})
    str = @sprintf("%smolecule mol{\n\tunits au\n\t%d %d\n", 
        self.memoryInGb == 0 ? "" : @sprintf("memory %d gb\n", self.memoryInGb), 
        self.charge, self.multiplicity)
    for i ∈ 1:length(coordinates)÷3
        str *= @sprintf("\t%s %25.15f %25.15f %25.15f\n", symbols[i], coordinates[(i-1)*3 + 1], coordinates[(i-1)*3 + 2], coordinates[(i-1)*3 + 3])
    end
    str *= @sprintf("}\n\nset basis %s\n\n", self.basisSet)
    str
end