function readInZmatrix(stuff::String)
    translationPattern = r"Translation\s*([+-]?\d+\.\d+)\s*([+-]?\d+\.\d+)\s*([+-]?\d+\.\d+)"
    rotationPattern = r"Rotation\s*([+-]?\d+\.\d+)\s*([+-]?\d+\.\d+)\s*([+-]?\d+\.\d+)"
    firstLineCapture = r"([A-Z][a-z]?[a-z]?)"
    secondLineCapture = r"([A-Z][a-z]?[a-z]?)\s*(\d+)\s*([+-]?\d+\.\d+)"
    thirdLineCapture = r"([A-Z][a-z]?[a-z]?)\s*(\d+)\s*([+-]?\d+\.\d+)\s*(\d+)\s*([+-]?\d+\.\d+)"
    otherLineCapture = r"([A-Z][a-z]?[a-z]?)\s*(\d+)\s*([+-]?\d+\.\d+)\s*(\d+)\s*([+-]?\d+\.\d+)\s*(\d+)\s*([+-]?\d+\.\d+)"
    referenceLine = r"\s*([+-]?\d+\.\d+)\s*([+-]?\d+\.\d+)\s*([+-]?\d+\.\d+)"
    zMatrixLine = 0
    symbols = Array{String}(undef, 0)
    currentTranslation = Array{Float64}(undef, 0)
    currentRotation = Array{Float64}(undef, 0)
    atomCount = 0
    indices = Array{Int}(undef, 0)
    lines = Array{ZmatrixLine}(undef, 0)
    values = Array{Float64}(undef, 0)
    allValues = Array{Float64}(undef, 0)
    zMatrices = Array{TRZmatrixFragment}(undef, 0)
    reference = Array{Float64}(undef, 0, 3)
    for line in eachline(IOBuffer(stuff))
        m = match(translationPattern, line)
        if !isnothing(m) && !isempty(lines)
            tline = TranslationLine(TranslationX(indices), TranslationY(indices), TranslationZ(indices))
            rline = RotationLine(RotationAForSlicedCenteredCartesian(indices, copy(reference)), RotationBForSlicedCenteredCartesian(indices, copy(reference)), RotationCForSlicedCenteredCartesian(indices, copy(reference)))
            radiusOfGyration = rline.rotationA.rotation.radiusOfGyration
            currentRotation = currentRotation .* radiusOfGyration
            push!(zMatrices, TRZmatrixFragment(tline, rline, Zmatrix(lines), radiusOfGyration, copy(reference)))
            append!(allValues, vcat(currentTranslation, currentRotation, values))

            indices = Array{Int}(undef, 0)
            currentTranslation = Array{Float64}(undef, 0)
            currentRotation = Array{Float64}(undef, 0)
            lines = Array{ZmatrixLine}(undef, 0)
            values = Array{Float64}(undef, 0)
            reference = Array{Float64}(undef, 0, 3)
            zMatrixLine = 0
        end
        if !isnothing(m)
            currentTranslation = [parse(Float64, m.captures[1])/bohr2Ang, parse(Float64, m.captures[2])/bohr2Ang, parse(Float64, m.captures[3])/bohr2Ang]
            continue
        end
        m = match(rotationPattern, line)
        if !isnothing(m)
            currentRotation = [parse(Float64, m.captures[1])/180*π, parse(Float64, m.captures[2])/180*π, parse(Float64, m.captures[3])/180*π]
            zMatrixLine += 1
            continue
        end
        m = match(firstLineCapture, line)
        if !isnothing(m) && zMatrixLine == 1
            push!(symbols, m.captures[1])
            push!(lines, FirstLine())
            zMatrixLine += 1
            atomCount += 1
            push!(indices, atomCount)
            continue
        end
        m = match(secondLineCapture, line)
        if !isnothing(m) && zMatrixLine == 2
            atomCount += 1
            push!(symbols, m.captures[1])
            b = parse(Int, m.captures[2])
            bond = Bond(b, atomCount)
            push!(values, parse(Float64, m.captures[3])/bohr2Ang)
            push!(lines, SecondLine(bond))

            zMatrixLine += 1
            push!(indices, atomCount)
            continue
        end
        m = match(thirdLineCapture, line)
        if !isnothing(m) && zMatrixLine == 3
            atomCount += 1
            push!(symbols, m.captures[1])
            b = parse(Int, m.captures[2])
            bond = Bond(b, atomCount)
            a = parse(Int, m.captures[4])
            angle = Angle(a, b, atomCount)
            push!(values, parse(Float64, m.captures[3])/bohr2Ang)
            push!(values, parse(Float64, m.captures[5])*π/180.0)
            push!(lines, ThirdLine(bond, angle))

            zMatrixLine += 1
            push!(indices, atomCount)
            continue
        end
        m = match(otherLineCapture, line)
        if !isnothing(m) && zMatrixLine > 3
            atomCount += 1
            push!(symbols, m.captures[1])
            b = parse(Int, m.captures[2])
            bond = Bond(b, atomCount)
            a = parse(Int, m.captures[4])
            angle = Angle(a, b, atomCount)
            d = parse(Int, m.captures[6])
            dihedral = Dihedral(d, a, b, atomCount)
            push!(values, parse(Float64, m.captures[3])/bohr2Ang)
            push!(values, parse(Float64, m.captures[5])*π/180.0)
            push!(values, parse(Float64, m.captures[7])*π/180.0)
            push!(lines, DefaultLine(zMatrixLine, bond, angle, dihedral))

            zMatrixLine += 1
            push!(indices, atomCount)
            continue
        end
        m = match(referenceLine, line)
        if !isnothing(m) && zMatrixLine > 3
            reference = [reference; [parse(Float64, m.captures[1]) parse(Float64, m.captures[2]) parse(Float64, m.captures[3])]]
            continue
        end
    end
    tline = TranslationLine(TranslationX(indices), TranslationY(indices), TranslationZ(indices))
    rline = RotationLine(RotationAForSlicedCenteredCartesian(indices, reference), RotationBForSlicedCenteredCartesian(indices, reference), RotationCForSlicedCenteredCartesian(indices, reference))
    radiusOfGyration = rline.rotationA.rotation.radiusOfGyration
    currentRotation = currentRotation .* radiusOfGyration
    push!(zMatrices, TRZmatrixFragment(tline, rline, Zmatrix(lines), radiusOfGyration, copy(reference)))
    append!(allValues, vcat(currentTranslation, currentRotation, values))

    trMatrix = TRZmatrix(zMatrices)
    trMatrix, toCartesians(trMatrix, allValues), symbols
end
