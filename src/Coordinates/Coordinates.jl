include("UnderlyingSystems/UnderlyingSystems.jl")

include("InternalCoordinates/InternalCoordinates.jl")
include("Constraints/Constraints.jl")
include("SetsOfInternalCoordinates/SetsOfInternalCoordinates.jl")
include("BuildInternalCoordinates/BuildInternalCoordinates.jl")
include("Zmatrices/Zmatrices.jl")

function getDifferencesCartesians(self::AbstractCoordinates, newCartesians::Array{Float64}, oldCartesians::Array{Float64})
    oldValues = getValuesFromCartesians(self.internalCoordinates, oldCartesians)
    newValues = getValuesFromCartesians(self.internalCoordinates, newCartesians)
    getDifferences(self.internalCoordinates, newValues, oldValues)
end

include("EmptyCoordinates.jl")
include("CartesianCoordinates.jl")
include("ZmatrixInternalCoordinates.jl")
include("RedundantInternalCoordinates.jl")
include("DelocalizedInternalCoordinates.jl")

include("Formats/Formats.jl")