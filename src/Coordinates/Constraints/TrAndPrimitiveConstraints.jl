struct TrAndPrimitiveConstraints <: SetOfConstraints
    translationsX::Array{TranslationXConstraint}
    translationsY::Array{TranslationYConstraint}
    translationsZ::Array{TranslationZConstraint}
    rotationsA::Array{RotationAConstraint}
    rotationsB::Array{RotationBConstraint}
    rotationsC::Array{RotationCConstraint}
    primitives::PrimitiveConstraints
end

function addTranslation!(self::TrAndPrimitiveConstraints, translationX::TranslationXConstraint)
    push!(self.translationsX, translationX)
    self
end

function addTranslation!(self::TrAndPrimitiveConstraints, translationY::TranslationYConstraint)
    push!(self.translationsY, translationY)
    self
end

function addTranslation!(self::TrAndPrimitiveConstraints, translationZ::TranslationZConstraint)
    push!(self.translationsZ, translationZ)
    self
end

function addRotation!(self::TrAndPrimitiveConstraints, rotationA::RotationAConstraint)
    push!(self.rotationsA, rotationA)
    self
end

function addRotation!(self::TrAndPrimitiveConstraints, rotationB::RotationBConstraint)
    push!(self.rotationsB, rotationB)
    self
end

function addRotation!(self::TrAndPrimitiveConstraints, rotationC::RotationCConstraint)
    push!(self.rotationsC, rotationC)
    self
end

function reorderConstraints(self::TrAndPrimitiveConstraints, newOrder::Array{Int})
    TrAndPrimitiveConstraints(
        [reorderTranslationXConstraint(t, newOrder) for t ∈ self.translationsX],
        [reorderTranslationYConstraint(t, newOrder) for t ∈ self.translationsY],
        [reorderTranslationZConstraint(t, newOrder) for t ∈ self.translationsZ],
        [reorderRotationAConstraint(r, newOrder) for r ∈ self.rotationsA],
        [reorderRotationBConstraint(r, newOrder) for r ∈ self.rotationsB],
        [reorderRotationCConstraint(r, newOrder) for r ∈ self.rotationsC],
        reorderConstraints(self.primitives, newOrder)
    )
end