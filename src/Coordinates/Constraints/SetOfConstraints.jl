# COV_EXCL_START
hasConstraints(self::SetOfConstraints) = true
addTranslation!(self::SetOfConstraints, ::TranslationXConstraint) = error("\'addTranslation!\' with TranslationXConstraint seems not to be implemented for type $(typeof(self))")
addTranslation!(self::SetOfConstraints, ::TranslationYConstraint) = error("\'addTranslation!\' with TranslationYConstraint seems not to be implemented for type $(typeof(self))")
addTranslation!(self::SetOfConstraints, ::TranslationZConstraint) = error("\'addTranslation!\' with TranslationYConstraint seems not to be implemented for type $(typeof(self))")
addRotation!(self::SetOfConstraints, ::RotationAConstraint) = error("\'addRotation!\' with RotationAConstraint seems not to be implemented for type $(typeof(self))")
addRotation!(self::SetOfConstraints, ::RotationBConstraint) = error("\'addRotation!\' with RotationBConstraint seems not to be implemented for type $(typeof(self))")
addRotation!(self::SetOfConstraints, ::RotationCConstraint) = error("\'addRotation!\' with RotationCConstraint seems not to be implemented for type $(typeof(self))")
reorderConstraints(self::SetOfConstraints, ::Array{Int}) = error("\'reorderConstraints\' seems not to be implemented for type $(typeof(self))")
# COV_EXCL_STOP