struct RotationAConstraint <: Constraint
    indices::Array{Int64}
end

struct RotationBConstraint <: Constraint
    indices::Array{Int64}
end

struct RotationCConstraint <: Constraint
    indices::Array{Int64}
end

function ==(a::RotationA, b::RotationAConstraint)
    sort(a.rotation.indices) == sort(b.indices)
end

function ==(a::RotationAConstraint, b::RotationA)
    b == a
end

function ==(a::RotationB, b::RotationBConstraint)
    sort(a.rotation.indices) == sort(b.indices)
end

function ==(a::RotationBConstraint, b::RotationB)
    b == a
end

function ==(a::RotationC, b::RotationCConstraint)
    sort(a.rotation.indices) == sort(b.indices)
end

function ==(a::RotationCConstraint, b::RotationC)
    b == a
end

function reorderRotationAConstraint(r::RotationAConstraint, newOrder::Array{Int})
    RotationAConstraint([newOrder[i] for i in r.indices])
end

function reorderRotationBConstraint(r::RotationBConstraint, newOrder::Array{Int})
    RotationBConstraint([newOrder[i] for i in r.indices])
end

function reorderRotationCConstraint(r::RotationCConstraint, newOrder::Array{Int})
    RotationCConstraint([newOrder[i] for i in r.indices])
end