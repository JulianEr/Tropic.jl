struct AngleConstraint <: Constraint
    a::Int64
    b::Int64
    c::Int64
end

function ==(a::Angle, b::AngleConstraint)
    a.a == b.a && a.b == b.b && a.c == b.c || a.a == b.c && a.b == b.b && a.c == b.a
end

function ==(a::AngleConstraint, b::Angle)
    b == a
end

function reorderAngleConstraint(a::AngleConstraint, newOrder::Array{Int})
    AngleConstraint(newOrder[a.a], newOrder[a.b], newOrder[a.c])
end
