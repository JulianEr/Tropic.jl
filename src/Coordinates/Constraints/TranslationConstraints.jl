struct TranslationXConstraint <: Constraint
    indices::Array{Int64}
end

struct TranslationYConstraint <: Constraint
    indices::Array{Int64}
end

struct TranslationZConstraint <: Constraint
    indices::Array{Int64}
end

function ==(a::TranslationX, b::TranslationXConstraint)
    sort(a.translation.indices) == sort(b.indices)
end

function ==(a::TranslationXConstraint, b::TranslationX)
    b == a
end

function ==(a::TranslationY, b::TranslationYConstraint)
    sort(a.translation.indices) == sort(b.indices)
end

function ==(a::TranslationYConstraint, b::TranslationY)
    b == a
end

function ==(a::TranslationZ, b::TranslationZConstraint)
    sort(a.translation.indices) == sort(b.indices)
end

function ==(a::TranslationZConstraint, b::TranslationZ)
    b == a
end

function reorderTranslationXConstraint(t::TranslationXConstraint, newOrder::Array{Int})
    TranslationXConstraint([newOrder[i] for i in t.indices])
end

function reorderTranslationYConstraint(t::TranslationYConstraint, newOrder::Array{Int})
    TranslationYConstraint([newOrder[i] for i in t.indices])
end

function reorderTranslationZConstraint(t::TranslationZConstraint, newOrder::Array{Int})
    TranslationZConstraint([newOrder[i] for i in t.indices])
end