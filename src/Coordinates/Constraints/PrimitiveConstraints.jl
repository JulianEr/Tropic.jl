struct PrimitiveConstraints <: SetOfConstraints
    bonds::Array{BondConstraint}
    angles::Array{AngleConstraint}
    dihedrals::Array{DihedralConstraint}
end

function addTranslation!(self::PrimitiveConstraints, translationX::TranslationXConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], self)
    addTranslation!(newConstraintSet, translationX)
end

function addTranslation!(self::PrimitiveConstraints, translationY::TranslationYConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], self)
    addTranslation!(newConstraintSet, translationY)
end

function addTranslation!(self::PrimitiveConstraints, translationZ::TranslationZConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], self)
    addTranslation!(newConstraintSet, translationZ)
end

function addRotation!(self::PrimitiveConstraints, rotationA::RotationAConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], self)
    addRotation!(newConstraintSet, rotationA)
end

function addRotation!(self::PrimitiveConstraints, rotationB::RotationBConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], self)
    addRotation!(newConstraintSet, rotationB)
end

function addRotation!(self::PrimitiveConstraints, rotationC::RotationCConstraint)
    newConstraintSet = TrAndPrimitiveConstraints([], [], [], [], [], [], self)
    addRotation!(newConstraintSet, rotationC)
end

function reorderConstraints(primitives::PrimitiveConstraints, newOrder::Array{Int})
    PrimitiveConstraints([reorderBondConstraint(b, newOrder) for b ∈ primitives.bonds],
                         [reorderAngleConstraint(a, newOrder) for a ∈ primitives.angles],
                         [reorderDihedralConstraint(d, newOrder) for d ∈ primitives.dihedrals])
end