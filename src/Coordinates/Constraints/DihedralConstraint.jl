struct DihedralConstraint <: Constraint
    a::Int64
    b::Int64
    c::Int64
    d::Int64
end

function ==(a::Dihedral, b::DihedralConstraint)
    a.a == b.a && a.b == b.b && a.c == b.c && a.d == b.d || a.a == b.d && a.b == b.c && a.c == b.b && a.d == b.a
end

function ==(a::DihedralConstraint, b::Dihedral)
    b == a
end

function reorderDihedralConstraint(d::DihedralConstraint, newOrder::Array{Int})
    DihedralConstraint(newOrder[d.a], newOrder[d.b], newOrder[d.c], newOrder[d.d])
end