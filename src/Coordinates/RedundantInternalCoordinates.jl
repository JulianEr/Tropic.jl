mutable struct RedundantInternalCoordinates <: AbstractCoordinates
    internalCoordinates::AbstractSetOfInternalCoordinates
    coords::UnderlyingSystem
    values::Array{Float64}
    B::Array{Float64, 2}
    G⁻¹::Array{Float64, 2}
    P::Array{Float64, 2}
    constraints::Array{Int}
    updateOnCartesianBacktransformation::Bool
end

function guessHessianLindh(self::RedundantInternalCoordinates, symbols::Array{String}, ::AbstractOptimizer)
    Hermitian(guessHessianLindh(self.internalCoordinates, symbols, self.coords.cartesians))
end

function guessHessianSchlegel(self::RedundantInternalCoordinates, symbols::Array{String}, ::AbstractOptimizer)
    Hermitian(guessHessianSchlegel(self.internalCoordinates, symbols, self.coords.cartesians))
end

identityHessian(self::RedundantInternalCoordinates) = Hermitian(1.0I(getFullInternalsSize(self.internalCoordinates)))

function getPG⁻¹B(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64}, constraints::Array{Int})
    B = getBmatrix(internalCoordinates, cartesians)
    G = B*B'
    G⁻¹ = pinv(G, 1e-7)

    P = G*G⁻¹
    if !isempty(constraints)
        C = let c=fill(0.0, size(B, 1))
            c[constraints] .= 1.0
            Diagonal(c)
        end
        P -= P*C*pinv(C*P*C, 1e-7)*C*P
    end
    @debug "Redundant important matrices" P=P Pnorm=norm(P) G⁻¹=G⁻¹ B=B 
    P, G⁻¹, B
end

function RedundantInternalCoordinates(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64}; constraints::SetOfConstraints=NoConstraints(), tryToUseZmatrixForTransformation::Bool=false, updateOnCartesianBacktransformation::Bool = true, constraintOverallTR::Bool=true)
    if constraintOverallTR && hasTranslationAndRotation(internalCoordinates)
        indicesArray = [i for i in 1:length(cartesians)÷3]

        constraints = addTranslation!(constraints, TranslationXConstraint(indicesArray))
        constraints = addTranslation!(constraints, TranslationYConstraint(indicesArray))
        constraints = addTranslation!(constraints, TranslationZConstraint(indicesArray))
        constraints = addRotation!(constraints, RotationAConstraint(indicesArray))
        constraints = addRotation!(constraints, RotationBConstraint(indicesArray))
        constraints = addRotation!(constraints, RotationCConstraint(indicesArray))
    end
    
    internalCoordinates, listOfConstraints = addAndFindConstraints!(internalCoordinates, constraints, cartesians)
    RedundantInternalCoordinates(internalCoordinates, cartesians, listOfConstraints, tryToUseZmatrixForTransformation, updateOnCartesianBacktransformation)
end

function RedundantInternalCoordinates(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64}, constraints::Array{Int}, tryToUseZmatrixForTransformation::Bool, updateOnCartesianBacktransformation::Bool)
    removeUndefinedDihedrals(internalCoordinates, cartesians)
    coords = if tryToUseZmatrixForTransformation
        coordsForRedundantInternalCoordinates(internalCoordinates, cartesians)
    else
        UnderlyingCartesians(cartesians)
    end

    values = getValuesFromCartesians(internalCoordinates, cartesians)

    P, G⁻¹, B = getPG⁻¹B(internalCoordinates, cartesians, constraints)

    RedundantInternalCoordinates(internalCoordinates, coords, values, B, G⁻¹, P, constraints, updateOnCartesianBacktransformation)
end

function setCoordinates!(coords::RedundantInternalCoordinates, newCoords::Array{Float64})
    setCoordinates!(coords.coords, coords, newCoords)
end

function setCoordinates!(coords::UnderlyingZmatrix, redundants::RedundantInternalCoordinates, newCoords::Array{Float64})
    zmatrixValues = newCoords[redundants.redundantIndices]
    coords.cartesians = toCartesians(coords.zMatrix, zmatrixValues)
    redundants.values = getValuesFromCartesians(redundants.internalCoordinates, coords.cartesians)
    @debug("Total Norm Of Internal Change after applying new Coordinates: $(norm(newCoords-redundants.values))")
    @debug("Norm of Coordinates in the Z-Matrix: $(norm(newCoords[coords.redundantIndices]-redundants.values[coords.redundantIndices]))")
    redundants.P, redundants.G⁻¹, redundants.B = getPG⁻¹B(redundants.internalCoordinates, coords.cartesians, redundants.constraints)
end

function setCoordinates!(::UnderlyingCartesians, coords::RedundantInternalCoordinates, newCoords::Array{Float64})
    error("setCoordinates! for the Cartesians is not implemented for RedundantInternalCoordinates")
end

function getChangeFromPrimitives(self::RedundantInternalCoordinates, primitiveValues::Array{Float64})
    getDifferences(self.internalCoordinates, self.values, primitiveValues)
end

# TODO: might use a function for internSetCoordinates! which applies change but does not calculate B, G, and P?
#       Is handy for the microiterative process? But First try if it is necessary

function applyChange!(redundants::RedundantInternalCoordinates, Δq::Array{Float64})
    applyChange!(redundants.coords, redundants, Δq)
end

resetCoordinateSystem!(::RedundantInternalCoordinates) = error("Redundant Internal Coordinates were tried to reset. I cannot reset Redundant Internal Coordinates. Aborting.")

function applyChange!(coords::UnderlyingZmatrix, redundants::RedundantInternalCoordinates, Δq::Array{Float64})
    xᵢ₊₁ = coords.cartesians
    qᵢ = q₀ = redundants.values
    max_micro, count = 50, 0
    new_q_norm = norm(Δq)
    old_q_norm = new_q_norm + 0.1
    @debug "Initial Del Change: " Δq=Δq
    @debug "Values before applying change" toString(redundants, q₀)
    while count == 0 || !(new_q_norm < 1e-6 || abs(new_q_norm - old_q_norm) < 1e-8) 
        if count == max_micro
            @warn("Could not converge applying change to Z-Matrix. Norm is still $(norm(Δs)).")
            throw(CoordinateTransformationException())
        else 
            count += 1
        end
        
        qᵢ₊₁ = removeOverwind(redundants.internalCoordinates, qᵢ + Δq)
        xᵢ₊₁ = toCartesians(coords.zMatrix, qᵢ₊₁[coords.redundantIndices])
        qᵢ₋₁ = qᵢ
        qᵢ = getValuesFromCartesians(redundants.internalCoordinates, xᵢ₊₁)
        Δq_actual = getDifferences(redundants.internalCoordinates, qᵢ, qᵢ₋₁)
        @debug "Supposed Change: " Δqᵢ=Δq step=count
        Δq -= Δq_actual
        old_q_norm = new_q_norm
        new_q_norm = norm(Δq)
        @debug "Actual Change: " Δq_actual=Δq_actual step=count
        @debug "Left Del Change: " Δq=Δq step=count
        @debug "Current Values: " qᵢ=qᵢ step=count
    end
    @info(@sprintf("To apply the change for the Z-Matrix I needed %2d iterations. |Δq| = %10.5e", count, new_q_norm))
    redundants.values = qᵢ
    @debug "Values after applying change" toString(redundants, qᵢ)
    @debug "Applied this change" toString(redundants, removeOverwind(redundants.internalCoordinates, qᵢ - q₀))
    coords.cartesians = xᵢ₊₁
    redundants.P, redundants.G⁻¹, redundants.B = getPG⁻¹B(redundants.internalCoordinates, coords.cartesians, redundants.constraints)
    @debug "Values after new coordinates." P=redundants.P G⁻¹=redundants.G⁻¹ B=redundants.B
end

function applyChange!(coords::UnderlyingCartesians, redundants::RedundantInternalCoordinates, Δq::Array{Float64})
    initialCartesian = coords.cartesians
    Δq_left = Δq
    BtG⁻¹ = redundants.B' * redundants.G⁻¹

    @info "Applying change: " Δq
    @debug "Values before applying change" toString(redundants, redundants.values)

    bestCartesian = Array{Float64}(undef, 0)
    Δq_actual = Array{Float64}(undef, 0)
    bestInternalNorm = Inf64
    internalNorm = Inf64
    x_rmsLast = Inf64
    max_micro = 50

    for i in 1:max_micro
        cartesian_changes = BtG⁻¹ * Δq_left
        coords.cartesians += cartesian_changes
        x_rms, x_max = getRms(cartesian_changes), getAbsMax(cartesian_changes)

        Δq_actual = getDifferencesCartesians(redundants, coords.cartesians, initialCartesian)
        Δq_left = Δq - Δq_actual
        internalNorm = norm(Δq_left)
        @debug @sprintf("RMS X: %15.10f; Max Abs(X): %15.10f; Internal Norm: %15.10f", x_rms, x_max, internalNorm)
        if internalNorm < bestInternalNorm
            bestInternalNorm, bestCartesian = internalNorm, coords.cartesians
        end
        
        if x_rms < 1e-7 && x_max < 1e-7
            break
        elseif abs(x_rms - x_rmsLast) < 1e-12
            @warn "Still did not converge but the change in cartesian RMS is not changing"
            break
        elseif i == max_micro
            @warn "We reached the max iter aborting"
            break
        end
        x_rmsLast = x_rms
        if redundants.updateOnCartesianBacktransformation
            B, G⁻¹ = getBG⁻¹(redundants.internalCoordinates, coords.cartesians)
            BtG⁻¹ = B' * G⁻¹
        end
    end
    if internalNorm < bestInternalNorm
        @warn "The internal norm after the apply change is worse than one of the prior internal norms" internalNorm=internalNorm bestInternalNorm=bestInternalNorm
        coords.cartesians = bestCartesian
    end
    redundants.values = getValuesFromCartesians(redundants.internalCoordinates, coords.cartesians)
    @debug "This was the actual change" toString(redundants, Δq_actual)
    @debug "Values after applying change" toString(redundants, redundants.values)
    @info "Applied change: " Δq_actual
    redundants.P, redundants.G⁻¹, redundants.B = getPG⁻¹B(redundants.internalCoordinates, coords.cartesians, redundants.constraints)
    @debug "Values after new coordinates." P=redundants.P G⁻¹=redundants.G⁻¹ B=redundants.B
end

getProjectedValues(self::RedundantInternalCoordinates) = self.values

getPrimitiveValues(self::RedundantInternalCoordinates) = self.values

getCartesians(self::RedundantInternalCoordinates) = self.coords.cartesians

function transformCartesianGradients(coords::RedundantInternalCoordinates, g::Array{Float64})
    #coords.P*coords.G⁻¹*coords.B*g
    g_d = coords.G⁻¹*coords.B*g
    @info "Values and gradinets" allCoordinates=getPrimitiveValues(coords) cartesianGradients=g internalGradients=g_d
    g_d
end

function transformCartesianHessian(coords::RedundantInternalCoordinates, H::AbstractArray{Float64, 2}, g::Array{Float64}, cartesians::Array{Float64})
    transformCartesianHessian(coords.internalCoordinates, H, g, coords.B, coords.G⁻¹, cartesians)
end


function gradientsToActiveSpace(coords::RedundantInternalCoordinates, g::Array{Float64}) 
    #g
    g_a = coords.P*g
    @info "Activate Gradients" g=g normG=norm(g) g_a=g_a normG_a=norm(g_a)
    g_a
end

function hessianToActiveSpace(coords::RedundantInternalCoordinates, H::AbstractArray{Float64, 2}) 
    H_a = coords.P*H*coords.P
    @info "Active Hessian" H=H normH=norm(H) H_a=H_a normH_a=norm(H_a)
    Hermitian(H_a)
end

function weightsToHessian(coords::RedundantInternalCoordinates, H::AbstractArray{Float64, 2})
    Hermitian(H + 1000.0 * (I-coords.P))
end

toString(self::RedundantInternalCoordinates) = toString(self.internalCoordinates, self.values)
toString(self::RedundantInternalCoordinates, values::Array{Float64}) = toString(self.internalCoordinates, values)