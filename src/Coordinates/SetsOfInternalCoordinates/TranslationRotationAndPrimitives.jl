mutable struct TranslationRotationAndPrimitives <: AbstractSetOfInternalCoordinates
    molecules::Array{Array{Int, 1}, 1}
    translationsX::Array{TranslationX}
    translationsY::Array{TranslationY}
    translationsZ::Array{TranslationZ}
    rotationsA::Array{RotationA}
    rotationsB::Array{RotationB}
    rotationsC::Array{RotationC}
    primitives::AbstractSetOfInternalCoordinates
end

function coordsForRedundantInternalCoordinates(internalCoordinates::TranslationRotationAndPrimitives, cartesians::Array{Float64}, internalsOffset::Int64)
    @info "Using translation and rotation as internal coordinates."
    try
        if length(internalCoordinates.molecules) < 2
            @info "No or only one Molecule found in the TR coordinates. Creating Z-Matrix for a single Molecule, I expect the TR coming from constraints."
            coordsForRedundantInternalCoordinates(internalCoordinates.primitives, cartesians, internalsOffset)
        else
            lines, redundantIndicesTuples = zMatrixForRedundantInternalCoordinates(internalCoordinates)
            UnderlyingZmatrix(lines, [i+internalsOffset for indices ∈ redundantIndicesTuples for i ∈ indices if i != 0], cartesians)
        end
    catch execption
        @info "$(execption.msg).\nThat did not work out, using  cartesian coordinates for change transformation."
        UnderlyingCartesians(cartesians)
    end
end

getThisInternalsSize(self::TranslationRotationAndPrimitives) = length(self.translationsX) + length(self.translationsY) + length(self.translationsZ) + length(self.rotationsA) + length(self.rotationsB) + length(self.rotationsC)
getFullInternalsSize(self::TranslationRotationAndPrimitives) = getThisInternalsSize(self) + getFullInternalsSize(self.primitives)

getBonds(internalCoordinates::TranslationRotationAndPrimitives) = getBonds(internalCoordinates.primitives)

function removeUndefinedDihedrals(internalCoordinates::TranslationRotationAndPrimitives, cartesians::Array{Float64})
    removeUndefinedDihedrals(internalCoordinates.primitives, cartesians)
end

function addToBmatrix!(self::TranslationRotationAndPrimitives, cartesians::Array{Float64}, B::Array{Float64, 2}, internalsOffset::Int64)
    ntx, nty, ntz = length(self.translationsX), length(self.translationsY), length(self.translationsZ)
    nra, nrb, nrc = length(self.rotationsA), length(self.rotationsB), length(self.rotationsC)
    
    fromXTranslations, toXTranslations = internalsOffset+1, internalsOffset+ntx
    fromYTranslations, toYTranslations = toXTranslations+1, toXTranslations+nty
    fromZTranslations, toZTranslations = toYTranslations+1, toYTranslations+ntz
    
    fromARotations, toARotations = toZTranslations+1, toZTranslations+nra
    fromBRotations, toBRotations = toARotations+1, toARotations+nrb
    fromCRotations, toCRotations = toBRotations+1, toBRotations+nrc

    for (index, i) ∈ enumerate(fromXTranslations:toXTranslations)
        B[i,:] = derivativeVector(self.translationsX[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromYTranslations:toYTranslations)
        B[i,:] = derivativeVector(self.translationsY[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromZTranslations:toZTranslations)
        B[i,:] = derivativeVector(self.translationsZ[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromARotations:toARotations)
        B[i,:] = derivativeVector(self.rotationsA[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromBRotations:toBRotations)
        B[i,:] = derivativeVector(self.rotationsB[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromCRotations:toCRotations)
        B[i,:] = derivativeVector(self.rotationsC[index], cartesians)
    end
    addToBmatrix!(self.primitives, cartesians, B, toCRotations)
end

function addToBmatrixDerivative!(self::TranslationRotationAndPrimitives, cartesians::Array{Float64}, BB::Array{Float64, 3}, internalsOffset::Int64)
    ntx, nty, ntz = length(self.translationsX), length(self.translationsY), length(self.translationsZ)
    nra, nrb, nrc = length(self.rotationsA), length(self.rotationsB), length(self.rotationsC)
    
    fromXTranslations, toXTranslations = internalsOffset+1, internalsOffset+ntx
    fromYTranslations, toYTranslations = toXTranslations+1, toXTranslations+nty
    fromZTranslations, toZTranslations = toYTranslations+1, toYTranslations+ntz
    
    fromARotations, toARotations = toZTranslations+1, toZTranslations+nra
    fromBRotations, toBRotations = toARotations+1, toARotations+nrb
    fromCRotations, toCRotations = toBRotations+1, toBRotations+nrc

    for (index, i) ∈ enumerate(fromXTranslations:toXTranslations)
        BB[i,:,:] = secondDerivativeMatrix(self.translationsX[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromYTranslations:toYTranslations)
        BB[i,:,:] = secondDerivativeMatrix(self.translationsY[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromZTranslations:toZTranslations)
        BB[i,:,:] = secondDerivativeMatrix(self.translationsZ[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromARotations:toARotations)
        BB[i,:,:] = secondDerivativeMatrix(self.rotationsA[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromBRotations:toBRotations)
        BB[i,:,:] = secondDerivativeMatrix(self.rotationsB[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromCRotations:toCRotations)
        BB[i,:,:] = secondDerivativeMatrix(self.rotationsC[index], cartesians)
    end
    addToBmatrixDerivative!(self.primitives, cartesians, BB, toCRotations)
end

function addToHessianGuessLindh!(self::TranslationRotationAndPrimitives, symbols::Array{String}, cartesians::Array{Float64}, h::Array{Float64}, internalsOffset::Int64)
    ntx, nty, ntz = length(self.translationsX), length(self.translationsY), length(self.translationsZ)
    nra, nrb, nrc = length(self.rotationsA), length(self.rotationsB), length(self.rotationsC)
    
    fromXTranslations, toXTranslations = internalsOffset+1, internalsOffset+ntx
    fromYTranslations, toYTranslations = toXTranslations+1, toXTranslations+nty
    fromZTranslations, toZTranslations = toYTranslations+1, toYTranslations+ntz
    
    fromARotations, toARotations = toZTranslations+1, toZTranslations+nra
    fromBRotations, toBRotations = toARotations+1, toARotations+nrb
    fromCRotations, toCRotations = toBRotations+1, toBRotations+nrc

    for (index, i) ∈ enumerate(fromXTranslations:toXTranslations)
        h[i] = getHessianGuessLindh(self.translationsX[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromYTranslations:toYTranslations)
        h[i] = getHessianGuessLindh(self.translationsY[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromZTranslations:toZTranslations)
        h[i] = getHessianGuessLindh(self.translationsZ[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromARotations:toARotations)
        h[i] = getHessianGuessLindh(self.rotationsA[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromBRotations:toBRotations)
        h[i] = getHessianGuessLindh(self.rotationsB[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromCRotations:toCRotations)
        h[i] = getHessianGuessLindh(self.rotationsC[index], symbols, cartesians)
    end
    addToHessianGuessLindh!(self.primitives, symbols, cartesians, h, toCRotations)
end

function addToHessianGuessSchlegel!(self::TranslationRotationAndPrimitives, symbols::Array{String}, cartesians::Array{Float64}, h::Array{Float64}, internalsOffset::Int64)
    ntx, nty, ntz = length(self.translationsX), length(self.translationsY), length(self.translationsZ)
    nra, nrb, nrc = length(self.rotationsA), length(self.rotationsB), length(self.rotationsC)
    
    fromXTranslations, toXTranslations = internalsOffset+1, internalsOffset+ntx
    fromYTranslations, toYTranslations = toXTranslations+1, toXTranslations+nty
    fromZTranslations, toZTranslations = toYTranslations+1, toYTranslations+ntz
    
    fromARotations, toARotations = toZTranslations+1, toZTranslations+nra
    fromBRotations, toBRotations = toARotations+1, toARotations+nrb
    fromCRotations, toCRotations = toBRotations+1, toBRotations+nrc

    for (index, i) ∈ enumerate(fromXTranslations:toXTranslations)
        h[i] = getHessianGuessSchlegel(self.translationsX[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromYTranslations:toYTranslations)
        h[i] = getHessianGuessSchlegel(self.translationsY[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromZTranslations:toZTranslations)
        h[i] = getHessianGuessSchlegel(self.translationsZ[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromARotations:toARotations)
        h[i] = getHessianGuessSchlegel(self.rotationsA[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromBRotations:toBRotations)
        h[i] = getHessianGuessSchlegel(self.rotationsB[index], symbols, cartesians)
    end
    for (index, i) ∈ enumerate(fromCRotations:toCRotations)
        h[i] = getHessianGuessSchlegel(self.rotationsC[index], symbols, cartesians)
    end
    addToHessianGuessSchlegel!(self.primitives, symbols, cartesians, h, toCRotations)
end

function removeOverwindStartingAtOffset!(self::TranslationRotationAndPrimitives, values::Array{Float64}, internalsOffset::Int64)
    ntx, nty, ntz = length(self.translationsX), length(self.translationsY), length(self.translationsZ)
    nra, nrb, nrc = length(self.rotationsA), length(self.rotationsB), length(self.rotationsC)
    rotationsStart = internalsOffset + ntx + nty + ntz + 1
    rotationsEnd = rotationsStart + nra + nrb + nrc - 1
    for i ∈ rotationsStart:rotationsEnd
        values[i] = keepPiInRange(values[i])
    end
    removeOverwindStartingAtOffset!(self.primitives, values, rotationsEnd)
end

function addValuesToCoordinates!(self::TranslationRotationAndPrimitives, cartesians::Array{Float64}, values::Array{Float64}, internalsOffset::Int64)
    ntx, nty, ntz = length(self.translationsX), length(self.translationsY), length(self.translationsZ)
    nra, nrb, nrc = length(self.rotationsA), length(self.rotationsB), length(self.rotationsC)
    
    fromXTranslations, toXTranslations = internalsOffset+1, internalsOffset+ntx
    fromYTranslations, toYTranslations = toXTranslations+1, toXTranslations+nty
    fromZTranslations, toZTranslations = toYTranslations+1, toYTranslations+ntz
    
    fromARotations, toARotations = toZTranslations+1, toZTranslations+nra
    fromBRotations, toBRotations = toARotations+1, toARotations+nrb
    fromCRotations, toCRotations = toBRotations+1, toBRotations+nrc

    for (index, i) ∈ enumerate(fromXTranslations:toXTranslations)
        values[i] = value(self.translationsX[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromYTranslations:toYTranslations)
        values[i] = value(self.translationsY[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromZTranslations:toZTranslations)
        values[i] = value(self.translationsZ[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromARotations:toARotations)
        values[i] = value(self.rotationsA[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromBRotations:toBRotations)
        values[i] = value(self.rotationsB[index], cartesians)
    end
    for (index, i) ∈ enumerate(fromCRotations:toCRotations)
        values[i] = value(self.rotationsC[index], cartesians)
    end
    addValuesToCoordinates!(self.primitives, cartesians, values, toCRotations)
end

function addAndFindConstraints!(self::TranslationRotationAndPrimitives, constraints::PrimitiveConstraints, cartesians::Array{Float64}, listOfConstraints::Array{Int64}, internalsOffset::Int64)
    translationEnd = internalsOffset + length(self.translationsX) + length(self.translationsY) + length(self.translationsZ)
    rotationEnd = translationEnd + length(self.rotationsA) + length(self.rotationsB) + length(self.rotationsC)
    addAndFindConstraints!(self.primitives, constraints, cartesians, listOfConstraints, rotationEnd)
end

function addAndFindConstraints!(self::TranslationRotationAndPrimitives, constraints::TrAndPrimitiveConstraints, cartesians::Array{Float64}, listOfConstraints::Array{Int64}, internalsOffset::Int64)
    for tx ∈ constraints.translationsX
        index = findfirst(x -> x == tx, self.translationsX)
        if index !== nothing
            push!(listOfConstraints, internalsOffset + index)
        else
            push!(self.translationsX, TranslationX(tx.indices))
            push!(listOfConstraints, internalsOffset + length(self.translationsX))
        end
    end
    txEnd = internalsOffset + length(self.translationsX)

    for ty ∈ constraints.translationsY
        index = findfirst(x -> x == ty, self.translationsY)
        if index !== nothing
            push!(listOfConstraints, txEnd + index)
        else
            push!(self.translationsY, TranslationY(ty.indices))
            push!(listOfConstraints, txEnd + length(self.translationsY))
        end
    end
    tyEnd = txEnd + length(self.translationsY)

    for tz ∈ constraints.translationsZ
        index = findfirst(x -> x == tz, self.translationsZ)
        if index !== nothing
            push!(listOfConstraints, tyEnd + index)
        else
            push!(self.translationsZ, TranslationZ(tz.indices))
            push!(listOfConstraints, tyEnd + length(self.translationsZ))
        end
    end
    tzEnd = tyEnd + length(self.translationsZ)

    for ra ∈ constraints.rotationsA
        index = findfirst(x -> x == ra, self.rotationsA)
        if index !== nothing
            push!(listOfConstraints, tzEnd + index)
        else
            push!(self.rotationsA, RotationA(ra.indices, cartesians))
            push!(listOfConstraints, tzEnd + length(self.rotationsA))
        end
    end
    raEnd = tzEnd + length(self.rotationsA)

    for rb ∈ constraints.rotationsB
        index = findfirst(x -> x == rb, self.rotationsB)
        if index !== nothing
            push!(listOfConstraints, raEnd + index)
        else
            push!(self.rotationsB, RotationB(rb.indices, cartesians))
            push!(listOfConstraints, raEnd + length(self.rotationsB))
        end
    end
    rbEnd = raEnd + length(self.rotationsB)

    for rc ∈ constraints.rotationsC
        index = findfirst(x -> x == rc, self.rotationsC)
        if index !== nothing
            push!(listOfConstraints, rbEnd + index)
        else
            push!(self.rotationsC, RotationC(rc.indices, cartesians))
            push!(listOfConstraints, rbEnd + length(self.rotationsC))
        end
    end
    rcEnd = rbEnd + length(self.rotationsC)
    addAndFindConstraints!(self.primitives, constraints, cartesians, listOfConstraints, rcEnd)
end

hasTranslationAndRotation(self::TranslationRotationAndPrimitives, ::Bool) = hasTranslationAndRotation(self.primitives, true)

function toStringWithOffset(self::TranslationRotationAndPrimitives, values::Array{Float64}, internalsOffset::Int64)
    result = ""

    ntx, nty, ntz = length(self.translationsX), length(self.translationsY), length(self.translationsZ)
    nra, nrb, nrc = length(self.rotationsA), length(self.rotationsB), length(self.rotationsC)
    
    fromXTranslations, toXTranslations = internalsOffset+1, internalsOffset+ntx
    fromYTranslations, toYTranslations = toXTranslations+1, toXTranslations+nty
    fromZTranslations, toZTranslations = toYTranslations+1, toYTranslations+ntz
    
    fromARotations, toARotations = toZTranslations+1, toZTranslations+nra
    fromBRotations, toBRotations = toARotations+1, toARotations+nrb
    fromCRotations, toCRotations = toBRotations+1, toBRotations+nrc

    for (index, i) ∈ enumerate(fromXTranslations:toXTranslations)
        result *= toString(self.translationsX[index])*@sprintf("%15.10f\n", values[i])
    end
    for (index, i) ∈ enumerate(fromYTranslations:toYTranslations)
        result *= toString(self.translationsY[index])*@sprintf("%15.10f\n", values[i])
    end
    for (index, i) ∈ enumerate(fromZTranslations:toZTranslations)
        result *= toString(self.translationsZ[index])*@sprintf("%15.10f\n", values[i])
    end
    for (index, i) ∈ enumerate(fromARotations:toARotations)
        result *= toString(self.rotationsA[index])*@sprintf("%15.10f\n", values[i])
    end
    for (index, i) ∈ enumerate(fromBRotations:toBRotations)
        result *= toString(self.rotationsB[index])*@sprintf("%15.10f\n", values[i])
    end
    for (index, i) ∈ enumerate(fromCRotations:toCRotations)
        result *= toString(self.rotationsC[index])*@sprintf("%15.10f\n", values[i])
    end
    result*toStringWithOffset(self.primitives, values, toCRotations)
end