function keepPiInRange(x::Float64)
    if x < -π
        x = mod(x, -2*π)
        if x < -π
            x += 2*π
        end
    elseif x > π
        x = mod(x, 2*π)
        if x > π
            x -= 2*π
        end
    end
    x
end

function coordsForRedundantInternalCoordinates(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64})
    coordsForRedundantInternalCoordinates(internalCoordinates, cartesians, 0)
end

function getBmatrix(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64})
    B = Array{Float64}(undef, (getFullInternalsSize(internalCoordinates), length(cartesians)))
    addToBmatrix!(internalCoordinates, cartesians, B, 0)
    B 
end

function getDerivativeOfBmatrix(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64})
    BB = Array{Float64}(undef, getFullInternalsSize(internalCoordinates), length(cartesians), length(cartesians))
    addToBmatrixDerivative!(internalCoordinates, cartesians, BB, 0)
    BB
end

function guessHessianLindh(self::AbstractSetOfInternalCoordinates, symbols::Array{String}, cartesians::Array{Float64})
    h = Array{Float64}(undef, getFullInternalsSize(self))
    addToHessianGuessLindh!(self, symbols, cartesians, h, 0)
    Diagonal(h)
end

function guessHessianSchlegel(self::AbstractSetOfInternalCoordinates, symbols::Array{String}, cartesians::Array{Float64})
    h = Array{Float64}(undef, getFullInternalsSize(self))
    addToHessianGuessSchlegel!(self, symbols, cartesians, h, 0)
    Diagonal(h)
end

function removeOverwind(self::AbstractSetOfInternalCoordinates, values::Array{Float64})
    newValues = deepcopy(values)
    removeOverwindStartingAtOffset!(self, newValues, 0)
    newValues
end

function getValuesFromCartesians(self::AbstractSetOfInternalCoordinates, cartesians::Array{Float64})
    values = Array{Float64}(undef, getFullInternalsSize(self))
    addValuesToCoordinates!(self, cartesians, values, 0)
    values
end

function getDifferences(self::AbstractSetOfInternalCoordinates, values_now::Array{Float64}, values_old::Array{Float64})
    diff = values_now - values_old
    removeOverwindStartingAtOffset!(self, diff, 0)
    diff
end

function addAndFindConstraints!(self::AbstractSetOfInternalCoordinates, constraints::SetOfConstraints, cartesians::Array{Float64})
    listOfConstraints::Array{Int64} = []
    addAndFindConstraints!(self, constraints, cartesians, listOfConstraints, 0)
    self, unique(listOfConstraints)
end

function addAndFindConstraints!(self::AbstractSetOfInternalCoordinates, ::NoConstraints, ::Array{Float64})
    self, Int64[]
end

function hasTranslationAndRotation(self::AbstractSetOfInternalCoordinates)
    hasTranslationAndRotation(self, false)
end

function toString(self::AbstractSetOfInternalCoordinates, values::Array{Float64})
    toStringWithOffset(self, values, 0)
end

function getK(g_q::Array{Float64}, BB::Array{Float64, 3})
    K = Array{Float64}(undef, size(BB, 2), size(BB, 3))
    for i ∈ axes(BB, 2)
        for j ∈ axes(BB, 3)
            K[i, j] = transpose(BB[:, i, j])*g_q
        end
    end
    K
end

function transformCartesianHessian(internalCoordinates::AbstractSetOfInternalCoordinates, H::Array{Float64, 2}, g::Array{Float64}, B::Array{Float64, 2}, G⁻¹::Array{Float64, 2}, cartesians::Array{Float64})
    g_q = G⁻¹*B*g
    BB = getDerivativeOfBmatrix(internalCoordinates, cartesians)
    K = getK(g_q, BB)
    g_q, Hermitian(G⁻¹*B*(H - K)*B'*G⁻¹)
end

include("Primitives.jl")
include("TranslationRotationAndPrimitives.jl")
include("InternalCoordinatesWithHydrogenBonds.jl")
include("ZmatrixSetOfInternals.jl")