mutable struct ZmatrixSetOfInternals <: AbstractSetOfInternalCoordinates
    zMatrix::AbstractZmatrix
end

getThisInternalsSize(self::ZmatrixSetOfInternals) = getInternalsSize(self.zMatrix)
getFullInternalsSize(self::ZmatrixSetOfInternals) = getThisInternalsSize(self)

removeUndefinedDihedrals(self::ZmatrixSetOfInternals, ::Array{Float64}) = error("\'removeUndefinedDihedrals\' seems not to be implemented for type $(typeof(self))")
coordsForRedundantInternalCoordinates(self::ZmatrixSetOfInternals, ::Array{Float64}) = error("\'coordsForRedundantInternalCoordinates\' seems not to be implemented for type $(typeof(self))")

function addToBmatrix!(self::ZmatrixSetOfInternals, cartesians::Array{Float64}, B::Array{Float64, 2}, internalsOffset::Int64)
    internalB = getBmatrix(self.zMatrix, cartesians)
    thisSize = size(internalB, 1)
    B[internalsOffset+1:internalsOffset+thisSize, :] = internalB
end

function addToBmatrixDerivative!(self::ZmatrixSetOfInternals, cartesians::Array{Float64}, BB::Array{Float64, 3}, internalsOffset::Int64)
    internalBB = getDerivativeOfBmatrix(self.zMatrix, cartesians)
    thisSize = size(internalBB, 1)
    BB[internalsOffset+1:internalsOffset+thisSize, :, :] = internalBB
end

function addToHessianGuessLindh!(self::ZmatrixSetOfInternals, symbols::Array{String}, cartesians::Array{Float64}, h::Array{Float64}, internalsOffset::Int64)
    internalH = getHessianGuessLindh(self.zMatrix, symbols, cartesians)
    thisSize = length(internalH)
    h[internalsOffset+1:internalsOffset+thisSize] = internalH
end

function addToHessianGuessSchlegel!(self::ZmatrixSetOfInternals, symbols::Array{String}, cartesians::Array{Float64}, h::Array{Float64}, internalsOffset::Int64)
    internalH = getHessianGuessSchlegel(self.zMatrix, symbols, cartesians)
    thisSize = length(internalH)
    h[internalsOffset+1:internalsOffset+thisSize] = internalH
end

function removeOverwindStartingAtOffset!(self::ZmatrixSetOfInternals, values::Array{Float64}, internalsOffset::Int64)
    removeOverwindStartingAtOffset!(self.zMatrix, values, internalsOffset)
end

function addValuesToCoordinates!(self::ZmatrixSetOfInternals, cartesians::Array{Float64}, values::Array{Float64}, internalsOffset::Int64)
    valueArray = getInternalValueArray(self.zMatrix, cartesians)
    thisSize = length(valueArray)
    values[internalsOffset+1:internalsOffset+thisSize] = valueArray
end

addAndFindConstraints!(self::ZmatrixSetOfInternals, constraints::SetOfConstraints, ::Array{Float64}, ::Array{Int64}, ::Int64) = error("\'addAndFindConstraints!\' seems not to be implemented for type $(typeof(self)) and $(typeof(constraints))")

hasTranslationAndRotation(self::ZmatrixSetOfInternals, tr::Bool) = tr ? true : hasTranslationAndRotation(self.zMatrix)

toStringWithOffset(self::ZmatrixSetOfInternals, ::Array{Float64}, ::Int64) = error("\'toStringWithOffset\' seems not to be implemented for type $(typeof(self))")