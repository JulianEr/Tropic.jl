mutable struct InternalCoordinatesWithHydrogenBonds <: AbstractSetOfInternalCoordinates
    hydrogenBonds::Array{HydrogenBond}
    internalCoordinates::AbstractSetOfInternalCoordinates
end

function coordsForRedundantInternalCoordinates(internalCoordinates::InternalCoordinatesWithHydrogenBonds, cartesians::Array{Float64}, internalsOffset::Int64)
    coordsForRedundantInternalCoordinates(internalCoordinates.internalCoordinates, cartesians, internalsOffset+getThisInternalsSize(internalCoordinates))
end

getThisInternalsSize(self::InternalCoordinatesWithHydrogenBonds) = length(self.hydrogenBonds)
getFullInternalsSize(self::InternalCoordinatesWithHydrogenBonds) = getThisInternalsSize(self) + getFullInternalsSize(self.internalCoordinates)

getBonds(internalCoordinates::InternalCoordinatesWithHydrogenBonds) = getBonds(internalCoordinates.internalCoordinates)

function removeUndefinedDihedrals(internalCoordinates::InternalCoordinatesWithHydrogenBonds, cartesians::Array{Float64})
    removeUndefinedDihedrals(internalCoordinates.internalCoordinates, cartesians)
end

function addToBmatrix!(self::InternalCoordinatesWithHydrogenBonds, cartesians::Array{Float64}, B::Array{Float64, 2}, internalsOffset::Int64)
    nhb = getThisInternalsSize(self)
    fromBonds, toBonds = internalsOffset+1, internalsOffset+nhb
    for (index, i) ∈ enumerate(fromBonds:toBonds)
        B[i,:] = derivativeVector(self.hydrogenBonds[index], cartesians)
    end
    addToBmatrix!(self.internalCoordinates, cartesians, B, toBonds)
end

function addToBmatrixDerivative!(self::InternalCoordinatesWithHydrogenBonds, cartesians::Array{Float64}, BB::Array{Float64, 3}, internalsOffset::Int64)
    nhb = getThisInternalsSize(self)
    fromBonds, toBonds = internalsOffset+1, internalsOffset+nhb
    for (index, i) ∈ enumerate(fromBonds:toBonds)
        BB[i,:,:] = secondDerivativeMatrix(self.hydrogenBonds[index], cartesians)
    end
    addToBmatrixDerivative!(self.internalCoordinates, cartesians, BB, toBonds)
end

function addToHessianGuessLindh!(self::InternalCoordinatesWithHydrogenBonds, symbols::Array{String}, cartesians::Array{Float64}, h::Array{Float64}, internalsOffset::Int64)
    nhb = getThisInternalsSize(self)
    fromBonds, toBonds = internalsOffset+1, internalsOffset+nhb
    for (index, i) ∈ enumerate(fromBonds:toBonds)
        h[i] = getHessianGuessLindh(self.hydrogenBonds[index], symbols, cartesians)
    end
    addToHessianGuessLindh!(self.internalCoordinates, symbols, cartesians, h, toBonds)
end

function addToHessianGuessSchlegel!(self::InternalCoordinatesWithHydrogenBonds, symbols::Array{String}, cartesians::Array{Float64}, h::Array{Float64}, internalsOffset::Int64)
    nhb = getThisInternalsSize(self)
    fromBonds, toBonds = internalsOffset+1, internalsOffset+nhb
    for (index, i) ∈ enumerate(fromBonds:toBonds)
        h[i] = getHessianGuessSchlegel(self.hydrogenBonds[index], symbols, cartesians)
    end
    addToHessianGuessSchlegel!(self.internalCoordinates, symbols, cartesians, h, toBonds)
end

function removeOverwindStartingAtOffset!(self::InternalCoordinatesWithHydrogenBonds, values::Array{Float64}, internalsOffset::Int64)
    removeOverwindStartingAtOffset!(self.internalCoordinates, values, internalsOffset + getThisInternalsSize(self))
end

function addValuesToCoordinates!(self::InternalCoordinatesWithHydrogenBonds, cartesians::Array{Float64}, values::Array{Float64}, internalsOffset::Int64)
    nhb = length(self.hydrogenBonds)
    startHydrogenBonds = internalsOffset + 1
    endHydrogenBonds = startHydrogenBonds + nhb - 1
    for (index, i) ∈ enumerate(startHydrogenBonds:endHydrogenBonds)
        values[i] = value(self.hydrogenBonds[index], cartesians)
    end
    addValuesToCoordinates!(self.internalCoordinates, cartesians, values, endHydrogenBonds)
end

function addAndFindConstraintsForHydrogenBonds!(self::InternalCoordinatesWithHydrogenBonds, constraints::PrimitiveConstraints, listOfConstraints::Array{Int64}, internalsOffset::Int64)
    leftoverBonds::Array{BondConstraint} = []
    for b ∈ constraints.bonds
        index = findfirst(x -> x == b, self.hydrogenBonds)
        if index !== nothing
            push!(listOfConstraints, internalsOffset + index)
        else
            push!(leftoverBonds, b)
        end
    end
    leftoverBonds
end

function addAndFindConstraints!(self::InternalCoordinatesWithHydrogenBonds, constraints::PrimitiveConstraints, cartesians::Array{Float64}, listOfConstraints::Array{Int64}, internalsOffset::Int64)
    leftoverBonds = addAndFindConstraintsForHydrogenBonds!(self, constraints, listOfConstraints, internalsOffset)
    newConstraints = PrimitiveConstraints(leftoverBonds, constraints.angles, constraints.dihedrals)
    addAndFindConstraints!(self.internalCoordinates, newConstraints, cartesians, listOfConstraints, internalsOffset+length(self.hydrogenBonds))
end

function addAndFindConstraints!(self::InternalCoordinatesWithHydrogenBonds, constraints::TrAndPrimitiveConstraints, cartesians::Array{Float64}, listOfConstraints::Array{Int64}, internalsOffset::Int64)
    leftoverBonds = addAndFindConstraintsForHydrogenBonds!(self, constraints.primitives, listOfConstraints, internalsOffset)
    newConstraints = TrAndPrimitiveConstraints(
        constraints.translationsX, constraints.translationsY, constraints.translationsZ,
        constraints.rotationsA, constraints.rotationsB, constraints.rotationsC,
        PrimitiveConstraints(leftoverBonds, constraints.primitives.angles, constraints.primitives.dihedrals))
    addAndFindConstraints!(self.internalCoordinates, newConstraints, cartesians, listOfConstraints, internalsOffset+length(self.hydrogenBonds))
end

hasTranslationAndRotation(self::InternalCoordinatesWithHydrogenBonds, tr::Bool) = hasTranslationAndRotation(self.internalCoordinates, tr)

function toStringWithOffset(self::InternalCoordinatesWithHydrogenBonds, values::Array{Float64}, internalsOffset::Int64)
    result = ""
    nhb = length(self.hydrogenBonds)
    startHydrogenBonds = internalsOffset + 1
    endHydrogenBonds = startHydrogenBonds + nhb - 1
    for (index, i) ∈ enumerate(startHydrogenBonds:endHydrogenBonds)
        result *= toString(self.hydrogenBonds[index])*@sprintf("%15.10f\n", values[i])
    end
    result*toStringWithOffset(self.internalCoordinates, values, endHydrogenBonds)
end