mutable struct DelocalizedInternalCoordinates <: AbstractCoordinates
    internalCoordinates::AbstractSetOfInternalCoordinates
    coords::UnderlyingSystem
    primitiveValues::Array{Float64}
    B::Array{Float64, 2}
    U::Array{Float64, 2}
    constraints::Array{Int}
    updateOnCartesianBacktransformation::Bool
    function DelocalizedInternalCoordinates(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64}, constraints::Array{Int}, tryToUseZmatrixForTransformation::Bool, updateOnCartesianBacktransformation::Bool)
        removeUndefinedDihedrals(internalCoordinates, cartesians)
        coords = if tryToUseZmatrixForTransformation
            coordsForRedundantInternalCoordinates(internalCoordinates, cartesians)
        else
            UnderlyingCartesians(cartesians)
        end
        
        values = getValuesFromCartesians(internalCoordinates, cartesians)
    
        U, B = getDelocalizedSystem(internalCoordinates, cartesians, constraints)
    
        new(internalCoordinates, coords, values, B, U, constraints, updateOnCartesianBacktransformation)
    end
end

function DelocalizedInternalCoordinates(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64}; constraints::SetOfConstraints=NoConstraints(), tryToUseZmatrixForTransformation::Bool=true, updateOnCartesianBacktransformation::Bool=true, constraintOverallTR::Bool=true)
    if constraintOverallTR && hasTranslationAndRotation(internalCoordinates)
        indicesArray = [i for i in 1:length(cartesians)÷3]

        constraints = addTranslation!(constraints, TranslationXConstraint(indicesArray))
        constraints = addTranslation!(constraints, TranslationYConstraint(indicesArray))
        constraints = addTranslation!(constraints, TranslationZConstraint(indicesArray))
        constraints = addRotation!(constraints, RotationAConstraint(indicesArray))
        constraints = addRotation!(constraints, RotationBConstraint(indicesArray))
        constraints = addRotation!(constraints, RotationCConstraint(indicesArray))
    end
    
    internalCoordinates, listOfConstraints = addAndFindConstraints!(internalCoordinates, constraints, cartesians)
    DelocalizedInternalCoordinates(internalCoordinates, cartesians, listOfConstraints, tryToUseZmatrixForTransformation, updateOnCartesianBacktransformation)
end

function guessHessianLindh(delocalizeds::DelocalizedInternalCoordinates, symbols::Array{String}, ::AbstractOptimizer)
    Hermitian(delocalizeds.U'*guessHessianLindh(delocalizeds.internalCoordinates, symbols, delocalizeds.coords.cartesians)*delocalizeds.U)
end

function guessHessianSchlegel(delocalizeds::DelocalizedInternalCoordinates, symbols::Array{String}, ::AbstractOptimizer)
    Hermitian(delocalizeds.U'*guessHessianSchlegel(delocalizeds.internalCoordinates, symbols, delocalizeds.coords.cartesians)*delocalizeds.U)
end

function identityHessian(delocalizeds::DelocalizedInternalCoordinates) 
    Hermitian(delocalizeds.U'*Array{Float64}(I, size(delocalizeds.U, 1), size(delocalizeds.U, 1))*delocalizeds.U)
end

function getChangeFromPrimitives(self::DelocalizedInternalCoordinates, primitiveValues::Array{Float64})
    self.U'*getDifferences(self.internalCoordinates, self.primitiveValues, primitiveValues)
end

function applyChange!(delocalizeds::DelocalizedInternalCoordinates, Δs::Array{Float64})
    applyChange!(delocalizeds.coords, delocalizeds, Δs)
end

function resetCoordinateSystem!(delocalizeds::DelocalizedInternalCoordinates)
    @info("Resetting Delocalized Internal Coordinate System")
    resetDelocalizedSystem!(delocalizeds, delocalizeds.coords.cartesians)
end

# TODO: reset also the Hessian
function resetDelocalizedSystem!(delocalizeds::DelocalizedInternalCoordinates, cartesians::Array{Float64})
    delocalizeds.U, delocalizeds.B = 
                getDelocalizedSystem(delocalizeds.internalCoordinates, cartesians, 
                    delocalizeds.constraints)
end

function applyChange!(coords::UnderlyingZmatrix, delocalizeds::DelocalizedInternalCoordinates, Δs::Array{Float64})
    xᵢ₊₁ = coords.cartesians
    qᵢ = q₀ = delocalizeds.primitiveValues
    max_micro, count = 50, 0
    new_s_norm = norm(Δs)
    old_s_norm = new_s_norm + 0.1
    @debug "Initial Del Change: " Δs=Δs
    @debug "Values before applying change" toString(delocalizeds, q₀)
    while count == 0 || !(new_s_norm < 1e-6 || abs(new_s_norm - old_s_norm) < 1e-8) 
        if count == max_micro
            @warn("Could not converge applying change to Z-Matrix. Norm is still $(norm(Δs)).")
            throw(CoordinateTransformationException())
        else 
            count += 1
        end
        
        Δqᵢ = delocalizeds.U*Δs
        qᵢ₊₁ = removeOverwind(delocalizeds.internalCoordinates, qᵢ + Δqᵢ)
        xᵢ₊₁ = toCartesians(coords.zMatrix, qᵢ₊₁[coords.redundantIndices])
        qᵢ₋₁ = qᵢ
        qᵢ = getValuesFromCartesians(delocalizeds.internalCoordinates, xᵢ₊₁)
        Δq_actual = getDifferences(delocalizeds.internalCoordinates, qᵢ, qᵢ₋₁)
        Δs -= delocalizeds.U'*Δq_actual
        old_s_norm = new_s_norm
        new_s_norm = norm(Δs)
        @debug "Supposed Change: " Δqᵢ=Δqᵢ step=count
        @debug "Actual Change: " Δq_actual=Δq_actual step=count
        @debug "Left Del Change: " Δs=Δs step=count
        @debug "Current Values: " qᵢ=qᵢ step=count
    end
    @info(@sprintf("To apply the change for the Z-Matrix I needed %2d iterations. |Δs| = %10.5e", count, new_s_norm))
    delocalizeds.primitiveValues = qᵢ
    @debug "Values after applying change" toString(delocalizeds, qᵢ)
    @debug "Applied this change" toString(delocalizeds, removeOverwind(delocalizeds.internalCoordinates, qᵢ - q₀))
    coords.cartesians = xᵢ₊₁
    updateBmatrix!(delocalizeds)
end

function applyChange!(coords::UnderlyingCartesians, delocalizeds::DelocalizedInternalCoordinates, Δs::Array{Float64})
    initialCartesian = coords.cartesians
    Δs_left = Δs
    BtG⁻¹ = delocalizeds.B' * getG⁻¹(delocalizeds.B)

    @info "Applying change: " Δs
    @debug "Values before applying change" toString(delocalizeds, delocalizeds.primitiveValues)

    bestCartesian = Array{Float64}(undef, 0)
    Δs_actual = Array{Float64}(undef, 0)
    bestInternalNorm = Inf64
    internalNorm = Inf64
    x_rmsLast = Inf64
    max_micro = 50

    for i in 1:max_micro
        cartesian_changes = BtG⁻¹ * Δs_left
        coords.cartesians += cartesian_changes
        x_rms, x_max = getRms(cartesian_changes), getAbsMax(cartesian_changes)

        Δs_actual = delocalizeds.U'*getDifferencesCartesians(delocalizeds, coords.cartesians, initialCartesian)
        Δs_left = Δs - Δs_actual
        internalNorm = norm(Δs_left)
        @debug @sprintf("RMS X: %15.10f; Max Abs(X): %15.10f; Internal Norm: %15.10f", x_rms, x_max, internalNorm)
        if internalNorm < bestInternalNorm
            bestInternalNorm, bestCartesian = internalNorm, coords.cartesians
        end
        
        if x_rms < 1e-7 && x_max < 1e-7
            break
        elseif abs(x_rms - x_rmsLast) < 1e-12
            @warn "Still did not converge but the change in cartesian RMS is not changing"
            break
        elseif i == max_micro
            @warn "We reached the max iter aborting"
            throw(CoordinateTransformationException())
        end
        x_rmsLast = x_rms
        if delocalizeds.updateOnCartesianBacktransformation
            B = delocalizeds.U'*getBmatrix(delocalizeds.internalCoordinates, delocalizeds.coords.cartesians)
            BtG⁻¹ = B' * getG⁻¹(delocalizeds.B)
        end
    end
    if internalNorm < bestInternalNorm
        @warn "The internal norm after the apply change is worse than one of the prior internal norms" internalNorm=internalNorm bestInternalNorm=bestInternalNorm
        coords.cartesians = bestCartesian
    end
    delocalizeds.primitiveValues = getValuesFromCartesians(delocalizeds.internalCoordinates, coords.cartesians)
    @debug "This was the actual change" toString(delocalizeds, delocalizeds.U*Δs_actual)
    @debug "Values after applying change" toString(delocalizeds, delocalizeds.primitiveValues)
    @info "Applied change: " Δs_actual
    updateBmatrix!(delocalizeds)
end

getProjectedValues(delocalizeds::DelocalizedInternalCoordinates) = delocalizeds.U'*delocalizeds.primitiveValues

getPrimitiveValues(delocalizeds::DelocalizedInternalCoordinates) = delocalizeds.primitiveValues

getCartesians(delocalizeds::DelocalizedInternalCoordinates) = delocalizeds.coords.cartesians

# function transformCartesianGradients(delocalizeds::DelocalizedInternalCoordinates, g::Array{Float64})
#     delocalizeds.G⁻¹*delocalizeds.B*g
# end

function transformCartesianGradients(delocalizeds::DelocalizedInternalCoordinates, g::Array{Float64})
    G = delocalizeds.B*delocalizeds.B'
    D⁻¹ = Diagonal(1 ./ diag(G))
    xᵢ, rᵢ = let b = delocalizeds.B*g
        b, b - G*b
    end
    hᵢ = D⁻¹*rᵢ
    dᵢ = hᵢ
    max_micro, count = 50, 0
    while norm(rᵢ) > 1e-6
        if count == max_micro
            @warn("Could not converge Cartesian gradients while transformation into delocalized space. Norm is still $(norm(rᵢ)). Calculating G⁻¹ as fallback.")
            xᵢ = getG⁻¹(delocalizeds.B)*delocalizeds.B'*g
            break
        else 
            count += 1
        end
        zᵢ = G*dᵢ
        ɑᵢ = (rᵢ'*hᵢ)/(dᵢ'*zᵢ)

        xᵢ₊₁ = xᵢ + ɑᵢ*dᵢ
        rᵢ₊₁ = rᵢ - ɑᵢ*zᵢ
        hᵢ₊₁ = D⁻¹*rᵢ₊₁

        βᵢ = (rᵢ₊₁'*hᵢ₊₁)/(rᵢ'hᵢ)
        dᵢ = hᵢ₊₁ + βᵢ*dᵢ
        xᵢ, rᵢ, hᵢ = xᵢ₊₁, rᵢ₊₁, hᵢ₊₁
    end
    @info(@sprintf("To transform the gradients I needed %2d iterations. |r| = %10.5e", count, norm(rᵢ)))
    @debug "Gradient Transformation: " cartesianGradients=g internalGradients=xᵢ
    xᵢ
end

function transformCartesianHessian(delocalizeds::DelocalizedInternalCoordinates, H::AbstractArray{Float64, 2}, g::Array{Float64}, cartesians::Array{Float64})
    BB = getDerivativeOfBmatrix(delocalizeds.internalCoordinates, cartesians)
    G⁻¹ = pinv(delocalizeds.B*delocalizeds.B', 1e-7)
    g_q = G⁻¹*delocalizeds.B*g
    K = Array{Float64}(undef, size(BB, 2), size(BB, 3))
    for i ∈ 1:size(BB, 2)
        for j ∈ 1:size(BB, 3)
            K[i, j] = transpose(delocalizeds.U'*BB[:, i, j])*g_q
        end
    end

    g_q, Hermitian(G⁻¹*delocalizeds.B*(H - K)*delocalizeds.B'*G⁻¹)
end

function gradientsToActiveSpace(::DelocalizedInternalCoordinates, g::Array{Float64})
    @debug "Active Gradients: " g_a = g
    g
end

function hessianToActiveSpace(::DelocalizedInternalCoordinates, H::AbstractArray{Float64, 2})
    @debug "Active Hessian: " H_a = H
    H
end

weightsToHessian(::DelocalizedInternalCoordinates, H::AbstractArray{Float64, 2}) = H

toString(self::DelocalizedInternalCoordinates) = toString(self.internalCoordinates, self.primitiveValues)
toString(self::DelocalizedInternalCoordinates, values::Array{Float64}) = toString(self.internalCoordinates, values)

getInternalsSize(dic::DelocalizedInternalCoordinates) = size(dic.U, 2)

function getDelocalizedSystem(internalCoordinates::AbstractSetOfInternalCoordinates, cartesians::Array{Float64}, constraints::Array{Int})
    B = getBmatrix(internalCoordinates, cartesians)
    G = B*B'
    l, UR = eigen(Hermitian(G))
    l_indices = abs.(l).>1.0e-7
    U = UR[:, l_indices]
    if !isempty(constraints)
        appliedConstraints = 0
        for c in 1:length(constraints)
            currentConstraint = constraints[c]
            if norm(U[currentConstraint, appliedConstraints+1:end]) < 0.001
                U[currentConstraint, appliedConstraints+1:end] .= 0.0
                @info "Cannot Apply constraint for internal coordinate $(currentConstraint) because it was already inactive"
                continue
            end

            C = fill(0.0, size(U, 1))
            C[currentConstraint] = 1.0
            C_p = sum([C'*U[:,i]*U[:,i] for i in appliedConstraints+1:size(U,2)])
            c_n = norm(C_p)
            C_p /= c_n
            V = [C_p U[:, appliedConstraints+1:end]]
            removeCandidate = size(V, 2)
            for i in 2:size(V, 2)
                s = sum([V[:, i]'*V[:, j]*V[:, j] for j ∈ 1:i-1])
                v = V[:, i] - s
                v_n = norm(v)
                if v_n > 1e-7
                    V[:, i] = v/v_n
                else 
                    V[:, i] = v
                    removeCandidate = i
                end
            end
            U = [U[:, 1:appliedConstraints] V[:, 1:removeCandidate-1] V[:, removeCandidate+1:end]]

            appliedConstraints += 1
        end
        U = U[:,appliedConstraints+1:end]
    end
    U, U'*B
end

function updateBmatrix!(delocalizeds::DelocalizedInternalCoordinates)
    delocalizeds.B = delocalizeds.U'*getBmatrix(delocalizeds.internalCoordinates, delocalizeds.coords.cartesians)
end