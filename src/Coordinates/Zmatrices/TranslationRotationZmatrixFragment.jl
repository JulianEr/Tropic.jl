# TODO make the fragment to a Z-Matgrix line rather than the translation and rotation line.
mutable struct TRZmatrixFragment
    translation::TranslationLine
    rotation::RotationLine
    zmatrix::Zmatrix
    radiusOfGyration::Float64
    reference::Array{Float64, 2}
end

getInternalsSize(zMatrix::TRZmatrixFragment) = getInternalsSize(zMatrix.zmatrix)+6

function getInternalValueArray(zMatrix::TRZmatrixFragment, cartesians::Array{Float64})
    internalValues = Array{Float64}(undef, getInternalsSize(zMatrix))

    internalValues[1] = value(zMatrix.translation.translationX, cartesians)
    internalValues[2] = value(zMatrix.translation.translationY, cartesians)
    internalValues[3] = value(zMatrix.translation.translationZ, cartesians)
    internalValues[4] = value(zMatrix.rotation.rotationA, cartesians)
    internalValues[5] = value(zMatrix.rotation.rotationB, cartesians)
    internalValues[6] = value(zMatrix.rotation.rotationC, cartesians)
    internalValues[7:end]  = getInternalValueArray(zMatrix.zmatrix.lines, cartesians)

    internalValues
end

function addToBmatrixGetSize!(self::TRZmatrixFragment, B::Array{Float64, 2}, cartesians::Array{Float64}, internalsOffset::Int64)
    B[internalsOffset+1:internalsOffset+3, :] = getBmatrixLines(self.translation, cartesians)
    B[internalsOffset+4:internalsOffset+6, :] = getBmatrixLines(self.rotation, cartesians)
    zMatrixSize = getInternalsSize(self)
    B[internalsOffset+7:internalsOffset+zMatrixSize, :] = getBmatrix(self.zmatrix, cartesians)
    zMatrixSize
end

function addToDerivativeOfBmatrixGetSize!(self::TRZmatrixFragment, BB::Array{Float64, 3}, cartesians::Array{Float64}, internalsOffset::Int64) 
    BB[internalsOffset+1:internalsOffset+3, :, :] = getDerivativeOfBmatrixMatrices(self.translation, cartesians)
    BB[internalsOffset+4:internalsOffset+6, :, :] = getDerivativeOfBmatrixMatrices(self.rotation, cartesians)
    zMatrixSize = getInternalsSize(self)
    BB[internalsOffset+7:internalsOffset+zMatrixSize, :, :] = getDerivativeOfBmatrix(self.zmatrix, cartesians)
    zMatrixSize
end


function addToHessianGuessGetSize!(self::TRZmatrixFragment, h::Array{Float64}, symbols::Array{String}, cartesians::Array{Float64}, internalsOffset::Int64)
    h[internalsOffset+1:internalsOffset+3] = getHessianGuessValues(self.translation, symbols, cartesians)
    h[internalsOffset+4:internalsOffset+6] = getHessianGuessValues(self.rotation, symbols, cartesians)
    zMatrixSize = getInternalsSize(self)
    h[internalsOffset+7:internalsOffset+zMatrixSize] = getHessianGuessLindh(self.zmatrix, symbols, cartesians)
    zMatrixSize
end

function addToHessianGuessSchlegelGetSize!(self::TRZmatrixFragment, h::Array{Float64}, symbols::Array{String}, cartesians::Array{Float64}, internalsOffset::Int64)
    h[internalsOffset+1:internalsOffset+3] = getHessianGuessSchlegelValues(self.translation, symbols, cartesians)
    h[internalsOffset+4:internalsOffset+6] = getHessianGuessSchlegelValues(self.rotation, symbols, cartesians)
    zMatrixSize = getInternalsSize(self)
    h[internalsOffset+7:internalsOffset+zMatrixSize] = getHessianGuessSchlegel(self.zmatrix, symbols, cartesians)
    zMatrixSize
end

function removeOverwindStartingAtOffsetGetNewOffset!(self::TRZmatrixFragment, values::Array{Float64}, internalsOffset::Int64)
    values[internalsOffset+4] = keepPiInRange(values[internalsOffset+4])
    values[internalsOffset+5] = keepPiInRange(values[internalsOffset+5])
    values[internalsOffset+6] = keepPiInRange(values[internalsOffset+6])

    removeOverwindStartingAtOffset!(self.zmatrix, values, internalsOffset+6)
    internalsOffset+getInternalsSize(self)
end