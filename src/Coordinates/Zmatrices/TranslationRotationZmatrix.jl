mutable struct TRZmatrix <: AbstractZmatrix
    zmatrices::Array{TRZmatrixFragment}
end

getInternalsSize(zMatrix::TRZmatrix) = sum(getInternalsSize.(zMatrix.zmatrices))

function getInternalValueArray(zMatrices::TRZmatrix, cartesians::Array{Float64})
    size = 0
    for zMatrix ∈ zMatrices.zmatrices
        size += length(zMatrix.zmatrix.lines)*3
    end
    values = Array{Float64}(undef, size)
    position = 1
    for zMatrix ∈ zMatrices.zmatrices
        endPosition = position + length(zMatrix.zmatrix.lines)*3 - 1
        values[position:endPosition] = getInternalValueArray(zMatrix, cartesians)
        position = endPosition+1
    end
    values
end

function quaternionToReference(zmatrix::TRZmatrixFragment, subsystem::AbstractArray{Float64,2})
    _, q = getBestFittingQuaternion2D(zmatrix.reference, subsystem)
    q
end

function quaternionForRotation(rotations::Tuple{Float64, Float64, Float64})
    rotVec = [rotations...]
    θ = norm(rotVec)
    u = abs(θ) < 1e-4 ? [0.0; 0.0; 0.0] : rotVec/θ
    
    θ_halfe = θ/2
    [cos(θ_halfe); -sin(θ_halfe)*u]
end

function rotateSubsystem(zmatrix::TRZmatrixFragment, rotations::Tuple{Float64, Float64, Float64}, subsystem::Array{Float64})
    subsystem2D = toBaryCenter(cartesianFlattenTo2d(subsystem))
    q_max = quaternionToReference(zmatrix, subsystem2D)
    U = getRotationMartixFromQuaternion(q_max)
    rotatedSystem = subsystem2D*U'
    q_r = quaternionForRotation(rotations)
    U = getRotationMartixFromQuaternion(q_r)
    rotatedSystem = rotatedSystem*U'
    unflattenCartesians(rotatedSystem)
end

function translateSubsystem(subsystem::Array{Float64}, translation::Tuple{Float64, Float64, Float64})
    N = length(subsystem)÷3
    for i ∈ 1:N
        for j ∈ 1:3
            subsystem[(i-1)*3+j] += translation[j]
        end
    end
    subsystem
end

function toCartesians(zMatrices::TRZmatrix, values::Array{Float64})
    size = 0
    for zMatrix ∈ zMatrices.zmatrices
        size += length(zMatrix.zmatrix.lines)*3
    end
    coordinates = Array{Float64}(undef, size)
    
    position = 1
    for zMatrix ∈ zMatrices.zmatrices
        endPosition = position + length(zMatrix.zmatrix.lines)*3 - 1
        translation = (values[position], values[position+1], values[position+2])
        rotation = (values[position+3], values[position+4], values[position+5])./zMatrix.radiusOfGyration
        internals = values[position+6:endPosition]
        
        subsystem = toCartesians(zMatrix.zmatrix, internals, (position-1)÷3)
        
        subsystem = rotateSubsystem(zMatrix, rotation, subsystem)
        subsystem = translateSubsystem(subsystem, translation)

        coordinates[position:endPosition] = subsystem
        position = endPosition + 1
    end
    coordinates
end

function printZmatrix(zMatrices::TRZmatrix, symbols::Array{String}, values::Array{Float64})
    lines = ""
    offset = 0
    for zMatrix ∈ zMatrices.zmatrices
        fragmentSize = getInternalsSize(zMatrix)
        lines *= printLine(zMatrix.translation, (values[offset+1],values[offset+2],values[offset+3])) * "\n"
        lines *= printLine(zMatrix.rotation, (values[offset+4],values[offset+5],values[offset+6])./zMatrix.radiusOfGyration) * "\n"
    
        lines *= printZmatrix(zMatrix.zmatrix, symbols, values[offset+7:offset+fragmentSize])
        for cartesian ∈ eachrow(zMatrix.reference)
            lines *= @sprintf("%12.7f %12.7f %12.7f\n", cartesian...)
        end
        offset += fragmentSize
    end
    lines
end

function printZmatrixFromCartesians(zMatrices::TRZmatrix, symbols::Array{String}, cartesians::Array{Float64})
    lines = ""
    for zMatrix ∈ zMatrices.zmatrices
        lines *= printLine(zMatrix.translation, getInternalValues(zMatrix.translation, cartesians)) * "\n"
        lines *= printLine(zMatrix.rotation, getInternalValues(zMatrix.rotation, cartesians)./zMatrix.radiusOfGyration) * "\n"
    
        lines *= printZmatrixFromCartesians(zMatrix.zmatrix, symbols, cartesians)
        for cartesian ∈ eachrow(zMatrix.reference)
            lines *= @sprintf("%12.7f %12.7f %12.7f\n", cartesian...)
        end
    end
    lines
end

function getBmatrix(self::TRZmatrix, cartesians::Array{Float64}) 
    B = Array{Float64, 2}(undef, getInternalsSize(self), length(cartesians))
    offset = 0
    for fragment ∈ self.zmatrices
        offset += addToBmatrixGetSize!(fragment, B, cartesians, offset)
    end
    B
end

function getDerivativeOfBmatrix(self::TRZmatrix, cartesians::Array{Float64}) 
    BB = Array{Float64, 3}(undef, getInternalsSize(self), length(cartesians), length(cartesians))
    offset = 0
    for fragment ∈ self.zmatrices
        offset += addToDerivativeOfBmatrixGetSize!(fragment, BB, cartesians, offset)
    end
    BB
end

function getHessianGuessLindh(self::TRZmatrix, symbols::Array{String}, cartesians::Array{Float64})
    h = Array{Float64}(undef, getInternalsSize(self))
    offset = 0
    for fragment ∈ self.zmatrices
        offset += addToHessianGuessGetSize!(fragment, h, symbols, cartesians, offset)
    end
    h
end

function getHessianGuessSchlegel(self::TRZmatrix, symbols::Array{String}, cartesians::Array{Float64})
    h = Array{Float64}(undef, getInternalsSize(self))
    offset = 0
    for fragment ∈ self.zmatrices
        offset += addToHessianGuessSchlegelGetSize!(fragment, h, symbols, cartesians, offset)
    end
    h
end

function removeOverwindStartingAtOffset!(self::TRZmatrix, values::Array{Float64}, internalsOffset::Int64)
    offset = internalsOffset
    for fragment ∈ self.zmatrices
        offset = removeOverwindStartingAtOffsetGetNewOffset!(fragment, values, offset)
    end
end

hasTranslationAndRotation(self::TRZmatrix) = true