struct ThirdLine <: ZmatrixLine
    lineNumber::Int
    bond::Bond
    angle::Angle
end
ThirdLine(bond::Bond, angle::Angle) = ThirdLine(3, bond, angle)

function getInternalValues(line::ThirdLine, cartesians::Array{Float64})
    [value(line.bond, cartesians); value(line.angle, cartesians)]
end

function getBmatrixLines(line::ThirdLine, cartesians::Array{Float64})
    B = Array{Float64, 2}(undef, 2, length(cartesians))
    B[1, :] = derivativeVector(line.bond, cartesians)
    B[2, :] = derivativeVector(line.angle, cartesians)
    B
end

function getDerivativeOfBmatrixMatrices(line::ThirdLine, cartesians::Array{Float64})
    BB = Array{Float64, 3}(undef, 2, length(cartesians), length(cartesians))
    BB[1, :, :] = secondDerivativeMatrix(line.bond, cartesians)
    BB[2, :, :] = secondDerivativeMatrix(line.angle, cartesians)
    BB
end

function getHessianGuessValues(line::ThirdLine, symbols::Array{String}, cartesians::Array{Float64})
    [getHessianGuessLindh(line.bond, symbols, cartesians); getHessianGuessLindh(line.angle, symbols, cartesians)]
end

function getHessianGuessSchlegelValues(line::ThirdLine, symbols::Array{String}, cartesians::Array{Float64})
    [getHessianGuessSchlegel(line.bond, symbols, cartesians); getHessianGuessSchlegel(line.angle, symbols, cartesians)]
end

function printLine(line::ThirdLine, symbols::Array{String}, values::Array{Float64})
    b = line.bond.a
    a = line.angle.a
    @sprintf("%3s %2d %12.7f %2d %12.7f", symbols[line.lineNumber], 
                                          b, values[2]*bohr2Ang,
                                          a, values[3]*180.0/π)
end

function addToCartesian!(::ThirdLine, values::Array{Float64}, coordinates::Array{Float64})
    c = [-values[2]*cos(values[3]);
        values[2]*sin(values[3]);
        0.0]
    coordinates[7:9] = c + coordinates[4:6]
end

addToCartesian!(line::ThirdLine, values::Array{Float64}, coordinates::Array{Float64}, ::Int) = addToCartesian!(line, values, coordinates)