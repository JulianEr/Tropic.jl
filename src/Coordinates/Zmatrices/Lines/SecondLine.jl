struct SecondLine <: ZmatrixLine
    lineNumber::Int
    bond::Bond
end
SecondLine(bond::Bond) = SecondLine(2, bond)

function getInternalValues(line::SecondLine, cartesians::Array{Float64})
    value(line.bond, cartesians)
end

function getBmatrixLines(line::SecondLine, cartesians::Array{Float64})
    derivativeVector(line.bond, cartesians)
end

function getDerivativeOfBmatrixMatrices(line::SecondLine, cartesians::Array{Float64})
    secondDerivativeMatrix(line.bond, cartesians)
end

function getHessianGuessValues(line::SecondLine, symbols::Array{String}, cartesians::Array{Float64})
    getHessianGuessLindh(line.bond, symbols, cartesians)
end

function getHessianGuessSchlegelValues(line::SecondLine, symbols::Array{String}, cartesians::Array{Float64})
    getHessianGuessSchlegel(line.bond, symbols, cartesians)
end

function printLine(line::SecondLine, symbols::Array{String}, values::Array{Float64})
    b = line.bond.a
    @sprintf("%3s %2d %12.7f", symbols[line.lineNumber], b, values[1]*bohr2Ang)
end

function addToCartesian!(::SecondLine, values::Array{Float64}, coordinates::Array{Float64})
    coordinates[4:6] = [values[1]; 0.0; 0.0]
end

addToCartesian!(line::SecondLine, values::Array{Float64}, coordinates::Array{Float64}, ::Int) = addToCartesian!(line, values, coordinates)