struct FirstLine <: ZmatrixLine
    lineNumber::Int
end
FirstLine() = FirstLine(1)

function getInternalValues(::FirstLine, ::Array{Float64})
    Array{Float64}(undef, 0)
end

function printLine(line::FirstLine, symbols::Array{String}, ::Array{Float64}) 
    @sprintf("%3s", symbols[line.lineNumber])
end

function addToCartesian!(::FirstLine, ::Array{Float64}, coordinates::Array{Float64})
    coordinates[1:3] = [0.0; 0.0; 0.0]
end

addToCartesian!(line::FirstLine, values::Array{Float64}, coordinates::Array{Float64}, ::Int) = addToCartesian!(line, values, coordinates)