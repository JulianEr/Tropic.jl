struct RotationLine <: ZmatrixLine
    rotationA::RotationA
    rotationB::RotationB
    rotationC::RotationC
end

function getInternalValues(line::RotationLine, cartesians::Array{Float64})
    (value(line.rotationA, cartesians), value(line.rotationB, cartesians), value(line.rotationC, cartesians))
end

function getBmatrixLines(line::RotationLine, cartesians::Array{Float64})
    B = Array{Float64, 2}(undef, 3, length(cartesians))
    B[1, :] = derivativeVector(line.rotationA, cartesians)
    B[2, :] = derivativeVector(line.rotationB, cartesians)
    B[3, :] = derivativeVector(line.rotationC, cartesians)
    B
end

function getDerivativeOfBmatrixMatrices(line::RotationLine, cartesians::Array{Float64})
    BB = Array{Float64, 3}(undef, 3, length(cartesians), length(cartesians))
    BB[1, :, :] = secondDerivativeMatrix(line.rotationA, cartesians)
    BB[2, :, :] = secondDerivativeMatrix(line.rotationB, cartesians)
    BB[3, :, :] = secondDerivativeMatrix(line.rotationC, cartesians)
    BB
end

function getHessianGuessValues(line::RotationLine, symbols::Array{String}, cartesians::Array{Float64})
    [getHessianGuessLindh(line.rotationA, symbols, cartesians); getHessianGuessLindh(line.rotationB, symbols, cartesians); getHessianGuessLindh(line.rotationC, symbols, cartesians)]
end

function getHessianGuessSchlegelValues(line::RotationLine, symbols::Array{String}, cartesians::Array{Float64})
    [getHessianGuessSchlegel(line.rotationA, symbols, cartesians); getHessianGuessSchlegel(line.rotationB, symbols, cartesians); getHessianGuessSchlegel(line.rotationC, symbols, cartesians)]
end

function printLine(::RotationLine, rotation::Tuple{Float64, Float64, Float64})
    @sprintf("Rotation    %12.7f %12.7f %12.7f", (rotation .* (180/π))...)
end