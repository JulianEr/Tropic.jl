struct TranslationLine <: ZmatrixLine
    translationX::TranslationX
    translationY::TranslationY
    translationZ::TranslationZ
end

function getInternalValues(line::TranslationLine, cartesians::Array{Float64})
    (value(line.translationX, cartesians), value(line.translationY, cartesians), value(line.translationZ, cartesians))
end

function getBmatrixLines(line::TranslationLine, cartesians::Array{Float64})
    B = Array{Float64, 2}(undef, 3, length(cartesians))
    B[1, :] = derivativeVector(line.translationX, cartesians)
    B[2, :] = derivativeVector(line.translationY, cartesians)
    B[3, :] = derivativeVector(line.translationZ, cartesians)
    B
end

function getDerivativeOfBmatrixMatrices(line::TranslationLine, cartesians::Array{Float64})
    BB = Array{Float64, 3}(undef, 3, length(cartesians), length(cartesians))
    BB[1, :, :] = secondDerivativeMatrix(line.translationX, cartesians)
    BB[2, :, :] = secondDerivativeMatrix(line.translationY, cartesians)
    BB[3, :, :] = secondDerivativeMatrix(line.translationZ, cartesians)
    BB
end

function getHessianGuessValues(line::TranslationLine, symbols::Array{String}, cartesians::Array{Float64})
    [getHessianGuessLindh(line.translationX, symbols, cartesians); getHessianGuessLindh(line.translationY, symbols, cartesians); getHessianGuessLindh(line.translationZ, symbols, cartesians)]
end

function getHessianGuessSchlegelValues(line::TranslationLine, symbols::Array{String}, cartesians::Array{Float64})
    [getHessianGuessSchlegel(line.translationX, symbols, cartesians); getHessianGuessSchlegel(line.translationY, symbols, cartesians); getHessianGuessSchlegel(line.translationZ, symbols, cartesians)]
end

function printLine(::TranslationLine, translation::Tuple{Float64, Float64, Float64}) 
    @sprintf("Translation %12.7f %12.7f %12.7f", (translation .* bohr2Ang)...)
end