function findTranslationX(translations::Array{TranslationX}, molecule::Array{Int})
    for t ∈ 1:length(translations)
        if translations[t].translation.indices == molecule
            return translations[t], t
        end
    end
    error("Could not find a translation in x-direction for $(molecule)")
end

function findTranslationY(translations::Array{TranslationY}, molecule::Array{Int})
    for t ∈ 1:length(translations)
        if translations[t].translation.indices == molecule
            return translations[t], t
        end
    end
    error("Could not find a translation in y-direction for $(molecule)")
end

function findTranslationZ(translations::Array{TranslationZ}, molecule::Array{Int})
    for t ∈ 1:length(translations)
        if translations[t].translation.indices == molecule
            return translations[t], t
        end
    end
    error("Could not find a translation in z-direction for $(molecule)")
end

function findRotationA(rotations::Array{RotationA}, molecule::Array{Int})
    for r ∈ 1:length(rotations)
        if rotations[r].rotation.indices == molecule
            return rotations[r], r
        end
    end
    error("Could not find a rotation around a for $(molecule)")
end

function findRotationB(rotations::Array{RotationB}, molecule::Array{Int})
    for r ∈ 1:length(rotations)
        if rotations[r].rotation.indices == molecule
            return rotations[r], r
        end
    end
    error("Could not find a rotation around b for $(molecule)")
end

function findRotationC(rotations::Array{RotationC}, molecule::Array{Int})
    for r ∈ 1:length(rotations)
        if rotations[r].rotation.indices == molecule
            return rotations[r], r
        end
    end
    error("Could not find a rotation around c for $(molecule)")
end

function getTranslationsForMolecule(trprims::TranslationRotationAndPrimitives, molecule::Array{Int})
    tx, tx_i = findTranslationX(trprims.translationsX, molecule)
    ntx = length(trprims.translationsX)
    ty, ty_i = findTranslationY(trprims.translationsY, molecule) 
    nty = length(trprims.translationsY)
    tz, tz_i = findTranslationZ(trprims.translationsZ, molecule)
    (tx, ty, tz), (tx_i, ty_i+ntx, tz_i+ntx+nty)
end

function getRotationsForMolecules(trprims::TranslationRotationAndPrimitives, molecule::Array{Int})
    nt = length(trprims.translationsX) + length(trprims.translationsY) + length(trprims.translationsZ)
    ra, ra_i = findRotationA(trprims.rotationsA, molecule)
    nra = length(trprims.rotationsA)
    rb, rb_i = findRotationB(trprims.rotationsB, molecule)
    nrb = length(trprims.rotationsB)
    rc, rc_i = findRotationC(trprims.rotationsC, molecule)
    (ra, rb, rc), (ra_i+nt, rb_i+nt+nra, rc_i+nt+nra+nrb)
end

## TODO unify with other OG builder method
function zMatrixForRedundantInternalCoordinates(primitives::Primitives, troffset::Int, from::Int, to::Int)
    nb, na = length(primitives.bonds), length(primitives.angles)

    lines = Array{ZmatrixLine}(undef, to-from+1)
    linesToPrimitives = Array{Tuple{Int64, Int64, Int64}}(undef, length(lines)-1)
    lines[1] = FirstLine()
    lines[2], b_i = findSecondLine(primitives.bonds, from)
    linesToPrimitives[1] = (b_i+troffset, 0, 0)
    lines[3], (b_i, a_i) = findThirdLine(primitives.bonds, primitives.angles, from)
    linesToPrimitives[2] = (b_i+troffset, a_i+nb+troffset, 0)
    
    for i ∈ from+3:to
        lines[i-from+1], (b_i, a_i, d_i) = findDefaultLine(i-from+1, i, primitives.bonds, primitives.angles, primitives.dihedrals)
        linesToPrimitives[i-from] = (b_i+troffset, a_i+nb+troffset, d_i+nb+na+troffset)
    end

    Zmatrix(lines), linesToPrimitives
end

zMatrixForRedundantInternalCoordinates(trprims::TranslationRotationAndPrimitives) = zMatrixForRedundantInternalCoordinates(trprims, trprims.molecules)

function zMatrixForRedundantInternalCoordinates(trprims::TranslationRotationAndPrimitives, molecules::Array{Array{Int, 1}, 1})
    ntr = length(trprims.translationsX) + length(trprims.translationsY) + length(trprims.translationsZ) + length(trprims.rotationsA) + length(trprims.rotationsB) + length(trprims.rotationsC) 
    result = Array{TRZmatrixFragment}(undef, 0)
    linesToInternals = Array{Tuple{Int, Int, Int}, 1}(undef, 0)
    for molecule ∈ molecules
        translations, translation_indices = getTranslationsForMolecule(trprims, molecule)
        rotations, rotation_indices = getRotationsForMolecules(trprims, molecule)

        tline = TranslationLine(translations...)
        rline = RotationLine(rotations...)

        lines, linesToPrimitives  = zMatrixForRedundantInternalCoordinates(trprims.primitives, ntr, molecule[1], molecule[end])

        linesToInternals = [linesToInternals; [translation_indices; rotation_indices; linesToPrimitives]]
        push!(result, TRZmatrixFragment(tline, rline, lines, rline.rotationA.rotation.radiusOfGyration, rline.rotationA.rotation.reference))
    end
    TRZmatrix(result), linesToInternals
end