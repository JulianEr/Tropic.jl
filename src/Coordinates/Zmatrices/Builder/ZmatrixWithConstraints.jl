function getBondLines!(bondDict::Dict{Int, Bond}, listOfConstraints::Array{Int})
    foundLines = Array{Tuple{Int, Bond}}(undef, 0)
    for (atom, _) ∈ bondDict
        if atom == 2
            push!(listOfConstraints, 1)
        elseif atom == 3
            push!(listOfConstraints, 2)
        else
            push!(listOfConstraints, (atom-4)*3 + 4)
        end

        push!(foundLines, (atom, pop!(bondDict, atom)))
    end
    foundLines
end

function getAngleLines!(angleDict::Dict{Int, Angle}, bondDict::Dict{Int, Bond}, listOfConstraints::Array{Int})
    foundLines = Array{Tuple{Int, Tuple{Bond, Angle}}}(undef, 0)
    for (atom, _) ∈ angleDict
        a = pop!(angleDict, atom)
        b = pop!(bondDict, atom, nothing)
        if !isnothing(b)
            if !(b.b == a.c && b.a == a.b)
                throw(ErrorException("You are trying to constrain a bend angle and a bond length which describe the same atom but the bend does not contain the bond. This is in a non redundant system not possible. $(a) $(b)"))
            end

            if atom == 3
                push!(listOfConstraints, 2)
            else
                push!(listOfConstraints, (atom-4)*3 + 4)
            end
        else
            b = Bond(a.b, a.c)
        end
        if atom == 3
            push!(listOfConstraints, 3)
        else
            push!(listOfConstraints, (atom-4)*3 + 5)
        end
        push!(foundLines, (atom, (b, a)))
    end

    foundLines
end

function getDihedralLines!(dihedralDict::Dict{Int, Dihedral}, angleDict::Dict{Int, Angle}, bondDict::Dict{Int, Bond}, listOfConstraints::Array{Int})
    foundLines = Array{Tuple{Int, Tuple{Bond, Angle, Dihedral}}}(undef, 0)
    for (atom, _) ∈ dihedralDict
        d = pop!(dihedralDict, atom)
        a = pop!(angleDict, atom, nothing)
        b = pop!(bondDict, atom, nothing)
        if !isnothing(a)
            if !(a.c == d.d && a.b == d.c && a.a == d.b)
                 throw(ErrorException("You are trying to constrain a dihedral angle and a bend angle which describe the same atom but the dihedral does not contain the bend. This is in a non redundant system not possible. $(d) $(a)"))
            end
            push!(listOfConstraints, (atom-4)*3 + 5)
        else
            a = Angle(d.b, d.c, d.d)
        end
        if !isnothing(b)
            if !(b.b == d.d && b.a == d.c)
                throw(ErrorException("You are trying to constrain a dihedral angle and a bond length which describe the same atom but the dihedral does not contain the bond. This is in a non redundant system not possible. $(d) $(b)"))
            end
            push!(listOfConstraints, (atom-4)*3 + 4)
        else
            b = Bond(d.c, d.d)
        end

        push!(listOfConstraints, (atom-4)*3 + 6)
        push!(foundLines, (atom, (b, a, d)))
    end
    foundLines
end

function getLinesFromDicts!(dihedralDict::Dict{Int, Dihedral}, angleDict::Dict{Int, Angle}, bondDict::Dict{Int, Bond})
    listOfConstraints = Array{Int}(undef, 0)
    listOfConstraints, getDihedralLines!(dihedralDict, angleDict, bondDict, listOfConstraints), getAngleLines!(angleDict, bondDict, listOfConstraints), getBondLines!(bondDict, listOfConstraints)
end

internalMax(b::Bond) = max(b.a, b.b)
internalMax(a::Angle) = max(a.a, a.c)
internalMax(d::Dihedral) = max(d.a, d.d)

reorder(b::Bond) = Bond(b.b, b.a)
reorder(a::Angle) = Angle(a.c, a.b, a.a)
reorder(d::Dihedral) = Dihedral(d.d, d.c, d.b, d.a)

reorderMaxLast(b::Bond) = b.a > b.b ? reorder(b) : b
reorderMaxLast(a::Angle) = a.a > a.c ? reorder(a) : a
reorderMaxLast(d::Dihedral) = d.a > d.d ? reorder(d) : d

function getDictForLargestAtoms(internals::Array{T}) where T
    dict = Dict{Int, T}()

    for t ∈ internals
        atom = internalMax(t)
        if haskey(dict, atom)
            throw(ErrorException("In a non redundant Z-Matrix it is not possible to constrain two different internal coordinates describing the same atom, in your case: $(t) and $(dict[atom])"))
        else
            dict[atom] = reorderMaxLast(t)
        end
    end
    dict
end

function getLinesWithHoles(mustContain::Primitives)
    bondDict = getDictForLargestAtoms(mustContain.bonds)
    angleDict = getDictForLargestAtoms(mustContain.angles)
    dihedralDict = getDictForLargestAtoms(mustContain.dihedrals)

    getLinesFromDicts!(dihedralDict, angleDict, bondDict)
end

function findAnglesMatchingBond(bond::Bond, angles::Array{Angle})
    matchingAngles = Array{Angle}(undef, 0)
    for a ∈ angles
        if a.a == bond.b && a.b == bond.a
            push!(matchingAngles, reorder(a))
        elseif a.c == bond.b && a.b == bond.a
            push!(matchingAngles, a)
        end
    end
    matchingAngles
end

function findDihedralMatchingAngle(angle::Angle, dihedrals::Array{Dihedral})
    for d ∈ dihedrals
        if d.a == angle.c && d.b == angle.b && d.c == angle.a
            return reorder(d)
        elseif d.d == angle.c && d.c == angle.b && d.b == angle.a
            return d
        end
    end
    return nothing
end

function fillAngleLines(lines::Array{Tuple{Int, Tuple{Bond, Angle}}}, dihedrals::Array{Dihedral})
    filledLines = Array{Tuple{Int, Tuple{AbstractInternalCoordinate, AbstractInternalCoordinate, AbstractInternalCoordinate}}}(undef, 0)

    for line ∈ lines
        (n, (b,a)) = line 
        if n < 4
            push!(filledLines, (n, (b, a, EmptyInternalCoordinate())))
        else
            d = findDihedralMatchingAngle(a, dihedrals)
            if isnothing(d)
                throw(ErrorException("I could not find a dihedral angle in the given set of internal coordinates which includes angle $(line[2][2])"))
            end
            push!(filledLines, (n, (b, a, d)))
        end
    end

    filledLines
end

function findExactlyOneAngleForBond(bond::Bond, angles::Array{Angle})
    listOfOneAngle = findAnglesMatchingBond(bond, angles)
    if length(listOfOneAngle) != 1
        throw(ErrorException("When searching for only one angle for bond $(bond) I found $(length(listOfOneAngle)) angles."))
    end
    listOfOneAngle[1]
end

function fillBondLines(lines::Array{Tuple{Int, Bond}}, dihedrals::Array{Dihedral}, angles::Array{Angle})
    filledLines = Array{Tuple{Int, Tuple{AbstractInternalCoordinate, AbstractInternalCoordinate, AbstractInternalCoordinate}}}(undef, 0)

    for line ∈ lines
        n, b = line
        if n == 2
            push!(filledLines, (n, (b, EmptyInternalCoordinate(), EmptyInternalCoordinate())))
        elseif n == 3
            a = findExactlyOneAngleForBond(b, angles)
            push!(filledLines, (n, (b, a, EmptyInternalCoordinate())))
        else
            as = findAnglesMatchingBond(b, angles)
            if isempty(as)
                throw(ErrorException("I could not find aan angle in the given set of internal coordinates which includes bond $(b)"))
            else
                a, d = (function () 
                    for a ∈ as
                        d = findDihedralMatchingAngle(a, dihedrals)
                        if !isnothing(d)
                           return a, d 
                        end
                    end
                    throw(ErrorException("I could not find a dihedral angle matching any angle which includes bond $(b)"))
                end)()
                push!(filledLines, (n, (b, a, d)))
            end
        end
    end
    filledLines
end

function linesWithPredefinedPrimitives(primitives::Primitives, mustContain::Primitives)
    listOfConstraints, dihedralLines, angleLines, bondLines = getLinesWithHoles(mustContain)
    sort(listOfConstraints), sort([dihedralLines; fillAngleLines(angleLines, primitives.dihedrals); fillBondLines(bondLines, primitives.dihedrals, primitives.angles)], by=line -> line[1])
end

function zMatrixFromInternals(primitives::Primitives, mustContain::Primitives=Primitives([], [], []))
    lines = Array{ZmatrixLine}(undef, getNumberOfLines(primitives))
    constraints, constraintLines = linesWithPredefinedPrimitives(primitives, mustContain)
    constraintLineCount = 1
    constraintLinesMax = length(constraintLines)
    lines[1] = FirstLine()
    
    if constraintLineCount <= constraintLinesMax && constraintLines[constraintLineCount][1] == 2
        lines[2] = SecondLine(constraintLines[constraintLineCount][2][1])
        constraintLineCount += 1
    else
        lines[2], _ = findSecondLine(primitives.bonds)
    end
    if constraintLineCount <= constraintLinesMax && constraintLines[constraintLineCount][1] == 3
        lines[3] = ThirdLine(constraintLines[constraintLineCount][2][1], constraintLines[constraintLineCount][2][2])
        constraintLineCount += 1
    else
        lines[3], _ = findThirdLine(primitives.bonds, primitives.angles)
    end

    for i ∈ 4:length(lines)
        if constraintLineCount <= constraintLinesMax && constraintLines[constraintLineCount][1] == i
            lines[i] = DefaultLine(i, constraintLines[constraintLineCount][2][1], constraintLines[constraintLineCount][2][2], constraintLines[constraintLineCount][2][3])
            constraintLineCount += 1
        else
            lines[i], _ = findDefaultLine(i, i, primitives.bonds, primitives.angles, primitives.dihedrals)
        end
    end
    Zmatrix(lines), constraints
end