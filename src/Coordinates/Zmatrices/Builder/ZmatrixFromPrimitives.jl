function findBond(bonds::Array{Bond}, a::Int, b::Int)
    for i ∈ 1:length(bonds)
        if bonds[i].a == a && bonds[i].b == b
            return bonds[i], i
        elseif bonds[i].a == b && bonds[i].b == a
            bonds[i] = Bond(bonds[i].b, bonds[i].a)
            return bonds[i], i
        end
    end
    throw(ErrorException("Could not find enough bonds for Z-Matrix."))
end

function findAngle(angles::Array{Angle}, a::Int, b::Int, c::Int)
    #findfirst(a -> a.a == a && a.b == b && a.c == c, angles)
    for i ∈ 1:length(angles)
        if angles[i].a == a && 
           angles[i].b == b &&
           angles[i].c == c
            return angles[i], i
        elseif angles[i].c == a && 
               angles[i].b == b && 
               angles[i].a == c
            angles[i] = Angle(angles[i].c, angles[i].b, angles[i].a)
            return angles[i], i
        end
    end
    throw(ErrorException("Could not find enough angles for Z-Matrix."))
end

function findDihedral(dihedrals::Array{Dihedral}, d::Int)
    for i ∈ 1:length(dihedrals)
        if dihedrals[i].a < d &&
           dihedrals[i].b < d &&
           dihedrals[i].c < d &&
           dihedrals[i].d == d
            return dihedrals[i], i
        elseif dihedrals[i].a == d &&
               dihedrals[i].b < d &&
               dihedrals[i].c < d &&
               dihedrals[i].d < d
            dihedrals[i] =  Dihedral(dihedrals[i].d, dihedrals[i].c, dihedrals[i].b, dihedrals[i].a)
            return dihedrals[i], i
        end
    end
    throw(ErrorException("Could not find enough dihedral angles for Z-Matrix."))
end

function findSecondLine(bonds::Array{Bond}, startAt::Int=1)
    b, b_i = findBond(bonds, startAt, startAt+1)
    SecondLine(b), b_i
end

function findThirdLine(bonds::Array{Bond}, angles::Array{Angle}, startAt::Int=1)
    b, b_i = findBond(bonds, startAt+1, startAt+2)
    a, a_i = findAngle(angles, startAt, startAt+1, startAt+2)
    ThirdLine(b, a), (b_i, a_i)
end

function findDefaultLine(lineNumber::Int, atomNumber::Int, bonds::Array{Bond}, angles::Array{Angle}, dihedrals::Array{Dihedral})
    d, d_i = findDihedral(dihedrals, atomNumber)
    a, a_i = findAngle(angles, d.b, d.c, d.d)
    b, b_i = findBond(bonds, a.b, a.c)

    DefaultLine(lineNumber, b, a, d), (b_i, a_i, d_i)
end

function getNumberOfLines(primitives::Primitives) 
    if isempty(primitives.bonds)
        1
    else
        maximum([max(b.a, b.b) for b in primitives.bonds])
    end
end

function zMatrixForRedundantInternalCoordinates(primitives::Primitives)
    nb, na = length(primitives.bonds), length(primitives.angles)

    numberOfLines = getNumberOfLines(primitives)
    lines = Array{ZmatrixLine}(undef, numberOfLines)
    linesToPrimitives = Array{Tuple{Int64, Int64, Int64}}(undef, length(lines)-1)
    lines[1] = FirstLine()
    if numberOfLines == 1
        return Zmatrix(lines), linesToPrimitives
    end
    lines[2], b_i = findSecondLine(primitives.bonds)
    linesToPrimitives[1] = (b_i, 0, 0)
    if numberOfLines == 2
        return Zmatrix(lines), linesToPrimitives
    end

    lines[3], (b_i, a_i) = findThirdLine(primitives.bonds, primitives.angles)
    linesToPrimitives[2] = (b_i, a_i+nb, 0)
    if numberOfLines == 3
        return Zmatrix(lines), linesToPrimitives
    end
    
    for i ∈ 4:numberOfLines
        lines[i], (b_i, a_i, d_i) = findDefaultLine(i, i, primitives.bonds, primitives.angles, primitives.dihedrals)
        linesToPrimitives[i-1] = (b_i, a_i+nb, d_i+nb+na)
    end

    Zmatrix(lines), linesToPrimitives
end