## TODO remove primitives from lines and have them as special field
mutable struct Zmatrix <: AbstractZmatrix
    lines::Array{ZmatrixLine}
end

function getInternalValueArray(lines::Array{ZmatrixLine}, cartesians::Array{Float64})
    internalValues = Array{Float64}(undef, getInternalsSize(lines))
    for (index, line) ∈ enumerate(lines)
        if index > 3
            startDefaultLine = (index-4)*3
            internalValues[4+startDefaultLine:6+startDefaultLine] = getInternalValues(line, cartesians)
        elseif index == 3
            internalValues[2:3] = getInternalValues(line, cartesians)
        elseif index == 2
            internalValues[1] = getInternalValues(line, cartesians)
        end
    end
    internalValues
end

getInternalValueArray(zmatrix::Zmatrix, cartesians::Array{Float64}) = getInternalValueArray(zmatrix.lines, cartesians)

function getInternalsSize(zMatrix::Array{ZmatrixLine})
    n = length(zMatrix)
    if n == 1
        return 0
    elseif n == 2
        return 1
    elseif n == 3
        return 3
    else
        return n*3 - 6
    end
end

getInternalsSize(zMatrix::Zmatrix) = getInternalsSize(zMatrix.lines)

function toCartesians(zMatrix::Zmatrix, values::Array{Float64}, startAt::Int)
    coordinates = Array{Float64}(undef, length(zMatrix.lines)*3)
    for line ∈ zMatrix.lines
        addToCartesian!(line, values, coordinates, startAt)
    end
    coordinates
end

toCartesians(zMatrix::Zmatrix, values::Array{Float64}) = toCartesians(zMatrix, values, 0)

function printZmatrixLines(zMatrix::Zmatrix, symbols::Array{String}, values::Array{Float64})
    lines = ""
    for line ∈ zMatrix.lines
        lines *= printLine(line, symbols, values) * "\n"
    end
    lines
end

function printZmatrix(zMatrix::Zmatrix, symbols::Array{String}, values::Array{Float64})
    printZmatrixLines(zMatrix, symbols, values)
end

function printZmatrixFromCartesians(zMatrix::Zmatrix, symbols::Array{String}, cartesians::Array{Float64})
    values = getInternalValueArray(zMatrix.lines, cartesians)
    printZmatrixLines(zMatrix, symbols, values)
end

function getBmatrix(self::Zmatrix, cartesians::Array{Float64}) 
    B = Array{Float64, 2}(undef, getInternalsSize(self), length(cartesians))
    for (index, line) ∈ enumerate(self.lines)
        if index > 3
            startDefaultLine = (index-4)*3
            B[4+startDefaultLine:6+startDefaultLine, :] = getBmatrixLines(line, cartesians)
        elseif index == 3
            B[2:3, :] = getBmatrixLines(line, cartesians)
        elseif index == 2
            B[1, :] = getBmatrixLines(line, cartesians)
        end
    end
    B
end

function getDerivativeOfBmatrix(self::Zmatrix, cartesians::Array{Float64}) 
    BB = Array{Float64, 3}(undef, getInternalsSize(self), length(cartesians), length(cartesians))
    for (index, line) ∈ enumerate(self.lines)
        if index > 3
            startDefaultLine = (index-4)*3
            BB[4+startDefaultLine:6+startDefaultLine, :, :] = getDerivativeOfBmatrixMatrices(line, cartesians)
        elseif index == 3
            BB[2:3, :, :] = getDerivativeOfBmatrixMatrices(line, cartesians)
        elseif index == 2
            BB[1, :, :] = getDerivativeOfBmatrixMatrices(line, cartesians)
        end
    end
    BB
end

function getHessianGuessLindh(self::Zmatrix, symbols::Array{String}, cartesians::Array{Float64})
    h = Array{Float64}(undef, getInternalsSize(self))
    for (index, line) ∈ enumerate(self.lines)
        if index > 3
            startDefaultLine = (index-4)*3
            h[4+startDefaultLine:6+startDefaultLine] = getHessianGuessValues(line, symbols, cartesians)
        elseif index == 3
            h[2:3] = getHessianGuessValues(line, symbols, cartesians)
        elseif index == 2
            h[1] = getHessianGuessValues(line, symbols, cartesians)
        end
    end
    h
end

function getHessianGuessSchlegel(self::Zmatrix, symbols::Array{String}, cartesians::Array{Float64})
    h = Array{Float64}(undef, getInternalsSize(self))
    for (index, line) ∈ enumerate(self.lines)
        if index > 3
            startDefaultLine = (index-4)*3
            h[4+startDefaultLine:6+startDefaultLine] = getHessianGuessSchlegelValues(line, symbols, cartesians)
        elseif index == 3
            h[2:3] = getHessianGuessSchlegelValues(line, symbols, cartesians)
        elseif index == 2
            h[1] = getHessianGuessSchlegelValues(line, symbols, cartesians)
        end
    end
    h
end

function removeOverwindStartingAtOffset!(self::Zmatrix, values::Array{Float64}, internalsOffset::Int64)
    systemSize = getInternalsSize(self)
    if systemSize > 3
        for i ∈ internalsOffset+6:3:internalsOffset+systemSize
            values[i] = keepPiInRange(values[i])
        end
    end
end

hasTranslationAndRotation(self::Zmatrix) = false