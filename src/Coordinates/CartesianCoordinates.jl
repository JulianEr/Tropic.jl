mutable struct CartesianCoordinates <: AbstractCoordinates
    cartesians::Array{Float64}
    constraints::AbstractSetOfInternalCoordinates
    U::Array{Float64, 2}
end

function orthogonalization(V::AbstractArray{Float64, 2}, degreesOfFreedom::Int64) 
    U = Diagonal(fill(1.0, degreesOfFreedom))
    p = ones(degreesOfFreedom)
    appliedConstraints = 0
    for i ∈ 1:size(V, 2)
        C_p = sum([V[:,i]'*U[:,j]*U[:,j] for j in appliedConstraints+1:degreesOfFreedom])
        c_n = norm(C_p)
        if c_n < 1e-6
            continue
        end
        C_p /= c_n
        W = [C_p U[:, appliedConstraints+1:end]]
        removeCandidate = size(W, 2)
        for j ∈ 2:size(W, 2)
            s = sum([W[:, j]'*W[:, k]*W[:, k] for k ∈ 1:j-1])
            w = W[:, j] - s
            n_w = norm(w)
            if n_w > 1e-7
                W[:, j] = w/n_w
            else 
                W[:, j] = w
                removeCandidate = j
            end
        end
        U = [U[:, 1:appliedConstraints] W[:, 1:removeCandidate-1] W[:, removeCandidate+1:end]]
        appliedConstraints += 1
        p[appliedConstraints] = 0.0
    end
    U[:,appliedConstraints+1:end]#, Diagonal(p)
end

function makeProjector(internals::AbstractSetOfInternalCoordinates, cartesians::Array{Float64})
    V = getBmatrix(internals, cartesians)'
    for i in 1:size(V, 2)
        n =  norm(V[:, i])
        if n > 1e-7
            V[:, i] /= n
        end
    end
    
    orthogonalization(V, length(cartesians))
end

function CartesianCoordinates(cartesians::Array{Float64}; constraints::SetOfConstraints=NoConstraints()) 
    indicesArray = [i for i in 1:length(cartesians)÷3]

    constraints = addTranslation!(constraints, TranslationXConstraint(indicesArray))
    constraints = addTranslation!(constraints, TranslationYConstraint(indicesArray))
    constraints = addTranslation!(constraints, TranslationZConstraint(indicesArray))
    constraints = addRotation!(constraints, RotationAConstraint(indicesArray))
    constraints = addRotation!(constraints, RotationBConstraint(indicesArray))
    constraints = addRotation!(constraints, RotationCConstraint(indicesArray))

    internalCoordinatesAsConstraints, _ = addAndFindConstraints!(Primitives([],[],[]), constraints, cartesians)
    
    U = makeProjector(internalCoordinatesAsConstraints, cartesians)
    CartesianCoordinates(cartesians, internalCoordinatesAsConstraints, U)
end

function setCoordinates!(coords::CartesianCoordinates, newCoords::Array{Float64})
    coords.cartesians = newCoords
    coords.U = makeProjector(coords.constraints, coords.cartesians)
end

function getChangeFromPrimitives(self::CartesianCoordinates, primitiveValues::Array{Float64})
    self.U'*(self.cartesians - primitiveValues)
end

function applyChange!(coords::CartesianCoordinates, Δxs::Array{Float64})
    change = coords.U*Δxs
    @debug "Applying Change:" Δxs=change
    setCoordinates!(coords, coords.cartesians + change)
end

resetCoordinateSystem!(::CartesianCoordinates) = error("Tried to reset Cartesian Coordinates. Cannot Reset Cartesian Coordinates. Aborting.")

getProjectedValues(coords::CartesianCoordinates) = coords.U'*coords.cartesians

getPrimitiveValues(coords::CartesianCoordinates) = coords.cartesians

getCartesians(coords::CartesianCoordinates) = coords.cartesians

transformCartesianGradients(self::CartesianCoordinates, g::Array{Float64}) = self.U'*g

transformCartesianHessian(self::CartesianCoordinates, H::AbstractArray{Float64, 2}, g::Array{Float64}, ::Array{Float64}) = self.U'*g, self.U'*H*self.U

function gradientsToActiveSpace(::CartesianCoordinates, g::Array{Float64})     
    @debug "Active Gradients:" g=g
    g
end

function hessianToActiveSpace(::CartesianCoordinates, H::AbstractArray{Float64, 2}) 
    @debug "Active Hessian:" H=H
    H
end

weightsToHessian(::CartesianCoordinates, H::AbstractArray{Float64, 2}) = H 

function getRedundantSetOfInternalCoordinates(coords::CartesianCoordinates, symbols::Array{String}, withTR::Bool)
    g_mol = buildBondGraph(symbols, coords.cartesians)
    findAllInternalCoordinates(g_mol, symbols, coords.cartesians, withHydrogens=false, forceTranslationRotation=withTR)
end

function getHessianGuess(self::CartesianCoordinates, symbols::Array{String}, optimizer::AbstractOptimizer, hg::Function, withTR::Bool)
    try
        internalSystem = getRedundantSetOfInternalCoordinates(self, symbols, withTR)
        B = getBmatrix(internalSystem, self.cartesians)*self.U
        G = B*B'
        G⁻¹ = pinv(G, 1e-7)
        g_q = G⁻¹*B*getGradients(optimizer)
        H = hg(internalSystem, symbols, self.cartesians)
        @info "Internal Hessian Guess: " H=H

        BB = getDerivativeOfBmatrix(internalSystem, self.cartesians)
        BB_u = Array{Float64}(undef, size(BB, 1), size(self.U, 2), size(self.U, 2))
        for i in axes(BB, 1)
            BB_u[i,:,:] = self.U'*BB[i,:,:]*self.U
        end
        K = Array{Float64}(undef, size(BB_u, 2), size(BB_u, 3))
        for i ∈ 1:size(BB_u, 2)
            for j ∈ 1:size(BB_u, 2)
                K[i, j] = transpose(BB_u[:, i, j])*g_q
            end
        end
        H_x = B'*H*B+K
        if any(isnan, H_x)
            @warn "Hessian to cartesians included NAN" H_x=H_x
            error("The guessed Hessian for Cartesian Coordinates contained NaN")
        else
            H = 0.5*(H_x+H_x')
            @debug "This is the guessed Hessian for cartesians: " H=H
            H
        end
    catch e
        @warn "Something went wrong while building up an internal coordinate system. Maybe try another method for the initial Hessian."
        rethrow(e)
    end
end

function guessHessianLindh(self::CartesianCoordinates, symbols::Array{String}, optimizer::AbstractOptimizer)
    getHessianGuess(self, symbols, optimizer, (zmat, s, c) -> guessHessianLindh(zmat, s, c), true)
end

function guessHessianSchlegel(self::CartesianCoordinates, symbols::Array{String}, optimizer::AbstractOptimizer)
    getHessianGuess(self, symbols, optimizer, (zmat, s, c) -> guessHessianSchlegel(zmat, s, c), false)
end

identityHessian(self::CartesianCoordinates) = self.U'*1.0I(length(self.cartesians))*self.U

toString(::CartesianCoordinates) = error("Not implemented yet")
toString(::CartesianCoordinates, ::Array{Float64}) = error("to call toString with two values for Cartesian coordinares does not make any sense.")