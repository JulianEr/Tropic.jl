struct EmptyInternalCoordinate <: AbstractInternalCoordinate end

function ==(a::EmptyInternalCoordinate, b::EmptyInternalCoordinate)
    true
end

function ==(a::EmptyInternalCoordinate, b::AbstractInternalCoordinate)
    false
end

function ==(a::AbstractInternalCoordinate, b::EmptyInternalCoordinate)
    false
end