simpleForceConstantsAngle(::FirstPeriod, ::FirstPeriod)  = 1.0000
simpleForceConstantsAngle(::SecondPeriod, ::FirstPeriod) = 0.3949
simpleForceConstantsAngle(::ThirdPeriod, ::FirstPeriod)  = 0.3949
simpleForceConstantsAngle(::FirstPeriod, ::SecondPeriod) = 0.3949
simpleForceConstantsAngle(::SecondPeriod, ::SecondPeriod)= 0.2800
simpleForceConstantsAngle(::ThirdPeriod, ::SecondPeriod) = 0.2800
simpleForceConstantsAngle(::FirstPeriod, ::ThirdPeriod)  = 0.3949
simpleForceConstantsAngle(::SecondPeriod, ::ThirdPeriod) = 0.2800
simpleForceConstantsAngle(::ThirdPeriod, ::ThirdPeriod)  = 0.2800
simpleForceConstantsRadius(::FirstPeriod, ::FirstPeriod)  = 1.35
simpleForceConstantsRadius(::SecondPeriod, ::FirstPeriod) = 2.10
simpleForceConstantsRadius(::ThirdPeriod, ::FirstPeriod)  = 2.53
simpleForceConstantsRadius(::FirstPeriod, ::SecondPeriod) = 2.10
simpleForceConstantsRadius(::SecondPeriod, ::SecondPeriod)= 2.87
simpleForceConstantsRadius(::ThirdPeriod, ::SecondPeriod) = 3.40
simpleForceConstantsRadius(::FirstPeriod, ::ThirdPeriod)  = 2.53
simpleForceConstantsRadius(::SecondPeriod, ::ThirdPeriod) = 3.40
simpleForceConstantsRadius(::ThirdPeriod, ::ThirdPeriod)  = 3.40

function simpleForceConstantsρ(left::Period, right::Period, bondLength::Float64)
    ɑ = simpleForceConstantsAngle(left, right)
    r_ref = simpleForceConstantsRadius(left, right)
    exp(ɑ*(r_ref^2 - bondLength^2))
end