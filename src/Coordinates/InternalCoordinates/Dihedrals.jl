const ɛ_dihedrals = 0.01

struct Dihedral <: AbstractInternalCoordinate
    a::Int
    b::Int
    c::Int
    d::Int
end

struct DihedralUndefinedException <:Exception
    msg::String
end

function ==(a::Dihedral, b::Dihedral)
    a.a == b.a && a.b == b.b && a.c == b.c && a.d == b.d || a.a == b.d && a.b == b.c && a.c == b.b && a.d == b.a
end

function isDefined(self::Dihedral, xs::Array{Float64})
    a = getCartesian(self.a, xs)
    b = getCartesian(self.b, xs)
    c = getCartesian(self.c, xs)
    d = getCartesian(self.d, xs)

    u = a-b
    w = c-b
    v = d-c

    λ_u = norm(u)
    λ_w = norm(w)
    λ_v = norm(v)

    u /= λ_u
    w /= λ_w
    v /= λ_v

    cosu = dot(-u, w)
    cosv = dot(-v, w)

    if abs(cosu) > (1.0 - ɛ_dihedrals)  || abs(cosv) > (1.0 - ɛ_dihedrals) 
        @warn("Dihedral $((self.a, self.b, self.c, self.d)) is undefined")
        return false
    else
        return true
    end
end

function value(self::Dihedral, xs::Array{Float64})
    a = getCartesian(self.a, xs)
    b = getCartesian(self.b, xs)
    c = getCartesian(self.c, xs)
    d = getCartesian(self.d, xs)

    u = b-a
    w = c-b
    v = d-c
    u /= norm(u)
    w /= norm(w)
    v /= norm(v)

    n1 = cross(u, w)
    n2 = cross(w, v)

    atan(u'*n2, n1'*n2)
end

function derivative(a::Array{Float64}, b::Array{Float64}, c::Array{Float64}, d::Array{Float64})
    u = a-b
    w = c-b
    v = d-c

    λ_u = norm(u)
    λ_w = norm(w)
    λ_v = norm(v)

    u /= λ_u
    w /= λ_w
    v /= λ_v

    cosu = dot(-u, w)
    cosv = dot(-v, w)

    if abs(cosu) > (1.0 - ɛ_dihedrals)  || abs(cosv) > (1.0 - ɛ_dihedrals) 
        throw(DihedralUndefinedException(@sprintf("Dihedral angle is undefined. cosu=%f; cosv=%f", cosu, cosv)))
    end
    sin2u = 1.0 - cosu^2.0
    sin2v = 1.0 - cosv^2.0

    n1 = cross(u, w)
    n2 = cross(w, v)

    t1 = n1 ./ (λ_u*sin2u)
    t2 = n2 ./ (λ_v*sin2v)
    t3 = (n1*cosu)./(λ_w*sin2u) + (n2*cosv)./(λ_w*sin2v)

    (t1, -t1 - t3, -t2 + t3, t2)
end

function derivativeVector(self::Dihedral, xs::Array{Float64})
    vec = fill(0.0, length(xs))

    try
        a = getCartesian(self.a, xs)
        b = getCartesian(self.b, xs)
        c = getCartesian(self.c, xs)
        d = getCartesian(self.d, xs)

        vec[(self.a-1)*3+1:(self.a-1)*3+3], 
        vec[(self.b-1)*3+1:(self.b-1)*3+3],
        vec[(self.c-1)*3+1:(self.c-1)*3+3],
        vec[(self.d-1)*3+1:(self.d-1)*3+3] = derivative(a, b, c, d)
    catch e
        @warn(@sprintf("Dihedral %d %d %d %d is undefined. Setting derivative to zeroes. Error: %s", 
            self.a, self.b, self.c, self.d, e.msg))
    end

    vec
end

function getTerms(a::Array{Float64}, b::Array{Float64}, c::Array{Float64}, d::Array{Float64})
    u = a-b
    w = c-b
    v = d-c

    λ_u = norm(u)
    λ_w = norm(w)
    λ_v = norm(v)

    u /= λ_u
    w /= λ_w
    v /= λ_v

    cosu = -u'*w
    cosv = -v'*w

    if abs(cosu) > (1.0 - ɛ_dihedrals)  || abs(cosv) > (1.0 - ɛ_dihedrals) 
        throw(DihedralUndefinedException(@sprintf("Dihedral angle is undefined. cosu=%f; cosv=%f", cosu, cosv)))
    end

    sinu = sqrt(1.0 - cosu^2.0)
    sinv = sqrt(1.0 - cosv^2.0)

    n1 = cross(u, w)
    n2 = cross(w, v)


    (term1(w, u, n1, cosu, sinu, λ_u), 
     term2(w, v, n2, cosv, sinv, λ_v), 
     term3(w, u, n1, cosu, sinu, λ_u, λ_w), 
     term4(w, v, n2, cosv, sinv, λ_v, λ_w),
     term5(u, w, n1, cosu, sinu, λ_w),
     term6(v, w, n2, cosv, sinv, λ_w),
     term7(w, u, cosu, sinu, λ_u, λ_w),
     term8(w, v, cosv, sinv, λ_v, λ_w))
end

function term1(w::Array{Float64}, u::Array{Float64}, n1::Array{Float64}, cosu::Float64, sinu::Float64, λ_u::Float64)
    t1 = w*cosu + u
    t1 = n1*t1'
    t1 += t1'
    t1 ./= (λ_u^2.0 * sinu^4.0)

    t1
end

function term2(w::Array{Float64}, v::Array{Float64}, n2::Array{Float64}, cosv::Float64, sinv::Float64, λ_v::Float64)
    t2 = w*cosv + v
    t2 = n2*t2'
    t2 += t2'
    t2 ./= (λ_v^2.0 * sinv^4.0)

    t2
end

function term3(w::Array{Float64}, u::Array{Float64}, n1::Array{Float64}, cosu::Float64, sinu::Float64, λ_u::Float64, λ_w::Float64)
    t3 = w + 2.0*u*cosu + w*cosu^2.0
    t3 = n1*t3'
    t3 += t3'
    t3 ./= (2.0*λ_u*λ_w*sinu^4.0)

    t3
end

function term4(w::Array{Float64}, v::Array{Float64}, n2::Array{Float64}, cosv::Float64, sinv::Float64, λ_v::Float64, λ_w::Float64)
    t4 = w + 2.0*v*cosv + w*cosv^2.0
    t4 = n2*t4'
    t4 += t4'
    t4 ./= (2.0*λ_v*λ_w*sinv^4.0)

    t4
end

function term5(u::Array{Float64}, w::Array{Float64}, n1::Array{Float64}, cosu::Float64, sinu::Float64, λ_w::Float64)
    t5 = u + 3.0*w*cosu + u*cosu^2.0 - w*cosu^3.0
    t5 = n1*t5'
    t5 += t5'
    t5 ./= (2.0*λ_w^2.0*sinu^4.0)

    t5
end

function term6(v::Array{Float64}, w::Array{Float64}, n2::Array{Float64}, cosv::Float64, sinv::Float64, λ_w::Float64)
    t6 = v + 3.0*w*cosv + v*cosv^2.0 - w*cosv^3.0
    t6 = n2*t6'
    t6 += t6'
    t6 ./= (2.0*λ_w^2.0*sinv^4.0)

    t6
end

function term7(w::Array{Float64}, u::Array{Float64}, cosu::Float64, sinu::Float64, λ_u::Float64, λ_w::Float64)
    t7 = (w*cosu+u) ./ (λ_u*λ_w*sinu^2.0)

    [ 0.0       -0.5*t7[3]  0.5*t7[2];
      0.5*t7[3]  0.0       -0.5*t7[1];
     -0.5*t7[2]  0.5*t7[1]  0.0]
end

function term8(w::Array{Float64}, v::Array{Float64}, cosv::Float64, sinv::Float64, λ_v::Float64, λ_w::Float64)
    t8 = (w*cosv+v) ./ (λ_v*λ_w*sinv^2.0)

    [ 0.0       -0.5*t8[3]  0.5*t8[2];
      0.5*t8[3]  0.0       -0.5*t8[1];
     -0.5*t8[2]  0.5*t8[1]  0.0]
end

function secondDerivative(self::Dihedral, xs::Array{Float64})
    a = getCartesian(self.a, xs)
    b = getCartesian(self.b, xs)
    c = getCartesian(self.c, xs)
    d = getCartesian(self.d, xs)

    t1, t2, t3, t4, t5, t6, t7, t8 = getTerms(a, b, c, d)

    # the paper references the second derivatives wrong! This is tested against numeric second derivatives.
    aa = -t1
    ab =  t1          +t3                 -t7
    ac =              -t3                 +t7
    ba =  t1          +t3                 +t7
    bb = -t1      -2.0*t3         -t5 -t6
    bc =              +t3     -t4 +t5 +t6 -t7 +t8
    bd =                      +t4             -t8
    ca =              -t3                 -t7 
    cb =              +t3     -t4 +t5 +t6 +t7 -t8
    cc =     -t2          +2.0*t4 -t5 -t6
    cd =      t2              -t4             +t8
    db =                      +t4             +t8
    dc =      t2              -t4             -t8
    dd =     -t2

    aa, ab, ac, ba, bb, bc, bd, ca, cb, cc, cd, db, dc, dd
end


function secondDerivativeMatrix(self::Dihedral, xs::Array{Float64})
    mat = fill(0.0, (length(xs), length(xs)))
    try 
        mat[(self.a-1)*3+1:(self.a-1)*3+3, (self.a-1)*3+1:(self.a-1)*3+3],
        mat[(self.a-1)*3+1:(self.a-1)*3+3, (self.b-1)*3+1:(self.b-1)*3+3],
        mat[(self.a-1)*3+1:(self.a-1)*3+3, (self.c-1)*3+1:(self.c-1)*3+3],
        mat[(self.b-1)*3+1:(self.b-1)*3+3, (self.a-1)*3+1:(self.a-1)*3+3],
        mat[(self.b-1)*3+1:(self.b-1)*3+3, (self.b-1)*3+1:(self.b-1)*3+3],
        mat[(self.b-1)*3+1:(self.b-1)*3+3, (self.c-1)*3+1:(self.c-1)*3+3],
        mat[(self.b-1)*3+1:(self.b-1)*3+3, (self.d-1)*3+1:(self.d-1)*3+3],
        mat[(self.c-1)*3+1:(self.c-1)*3+3, (self.a-1)*3+1:(self.a-1)*3+3],
        mat[(self.c-1)*3+1:(self.c-1)*3+3, (self.b-1)*3+1:(self.b-1)*3+3],
        mat[(self.c-1)*3+1:(self.c-1)*3+3, (self.c-1)*3+1:(self.c-1)*3+3],
        mat[(self.c-1)*3+1:(self.c-1)*3+3, (self.d-1)*3+1:(self.d-1)*3+3],
        mat[(self.d-1)*3+1:(self.d-1)*3+3, (self.b-1)*3+1:(self.b-1)*3+3],
        mat[(self.d-1)*3+1:(self.d-1)*3+3, (self.c-1)*3+1:(self.c-1)*3+3],
        mat[(self.d-1)*3+1:(self.d-1)*3+3, (self.d-1)*3+1:(self.d-1)*3+3] = secondDerivative(self, xs)
    catch e
        @warn(@sprintf("Dihedral %d %d %d %d is undefined. Setting second derivatives to zeroes. Error: %s", 
            self.a, self.b, self.c, self.d, e.msg))
    end

    mat
end

function dihedralForceConstantSchlegel(leftMiddle::String, rightMiddle::String, middleBondLength::Float64)
    leftVdw = get(covalentRadii, leftMiddle, -1.0)
    rightVdw = get(covalentRadii, rightMiddle, -1.0)
    a = 0.0023
    b = 0.07
    if leftVdw < 0.0 || rightVdw < 0.0
        # No idea where I got this value from. Before I changed the implementation I had a 0.023 there. It served me well
        return a
    else
        vdwMiddleBondLength = leftVdw + rightVdw
        if middleBondLength > (vdwMiddleBondLength + a/b)
            a
        else
            a - b * (middleBondLength - vdwMiddleBondLength)
        end
    end
end

function getHessianGuessSchlegel(d::Dihedral, symbols::Array{String}, cartesians::Array{Float64})
    dihedralForceConstantSchlegel(symbols[d.b], symbols[d.c], getDistanceBetweenCartesianPointAandB(d.b, d.c, cartesians))
end

getHessianGuessLindh(::Dihedral, ::HigherPeriod, ::Period, ::Period, ::Period, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::Period, ::HigherPeriod, ::Period, ::Period, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::Period, ::Period, ::HigherPeriod, ::Period, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::Period, ::Period, ::Period, ::HigherPeriod, ::Array{Float64}) = 0.1

getHessianGuessLindh(::Dihedral, ::HigherPeriod, ::HigherPeriod, ::Period, ::Period, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::HigherPeriod, ::Period, ::HigherPeriod, ::Period, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::HigherPeriod, ::Period, ::Period, ::HigherPeriod, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::Period, ::HigherPeriod, ::HigherPeriod, ::Period, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::Period, ::HigherPeriod, ::Period, ::HigherPeriod, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::Period, ::Period, ::HigherPeriod, ::HigherPeriod, ::Array{Float64}) = 0.1

getHessianGuessLindh(::Dihedral, ::HigherPeriod, ::HigherPeriod, ::HigherPeriod, ::Period, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::HigherPeriod, ::HigherPeriod, ::Period, ::HigherPeriod, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::HigherPeriod, ::Period, ::HigherPeriod, ::HigherPeriod, ::Array{Float64}) = 0.1
getHessianGuessLindh(::Dihedral, ::Period, ::HigherPeriod, ::HigherPeriod, ::HigherPeriod, ::Array{Float64}) = 0.1

getHessianGuessLindh(::Dihedral, ::HigherPeriod, ::HigherPeriod, ::HigherPeriod, ::HigherPeriod, ::Array{Float64}) = 0.1

function getHessianGuessLindh(d::Dihedral, ol::Period, l::Period, r::Period, or::Period, cartesians::Array{Float64})
    leftBondLength = getDistanceBetweenCartesianPointAandB(d.a, d.b, cartesians)
    middleBondLength = getDistanceBetweenCartesianPointAandB(d.b, d.c, cartesians)
    rightBondLength = getDistanceBetweenCartesianPointAandB(d.c, d.d, cartesians)
    0.005*simpleForceConstantsρ(ol, l, leftBondLength)*simpleForceConstantsρ(l, r, middleBondLength)*simpleForceConstantsρ(r, or, rightBondLength)
end

function getHessianGuessLindh(d::Dihedral, symbols::Array{String}, cartesians::Array{Float64})
    outLeft, left, right, outRight = symbols[d.a], symbols[d.b], symbols[d.c], symbols[d.d]
    ol, l, r, or = placeInPeriodicTable[outLeft], placeInPeriodicTable[left], placeInPeriodicTable[right], placeInPeriodicTable[outRight]
    getHessianGuessLindh(d, ol, l, r, or, cartesians)
end

toString(d::Dihedral) = "Dihedral($(d.a), $(d.b), $(d.c), $(d.d))"