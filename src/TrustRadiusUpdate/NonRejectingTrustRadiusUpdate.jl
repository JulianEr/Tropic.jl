using Printf

#Fletchers namely but no rejecting of steps

struct NonRejectingTrustRadiusUpdate <: AbstractTrustRadiusUpdate 
    maxTrustRadius::Float64
    minTrustRadius::Float64
    increaseFactor::Float64
    decreaseFactor::Float64
end

NonRejectingTrustRadiusUpdate(; maxTrustRadius=1.0, minTrustRadius=0.001, increaseFactor=3.0, decreaseFactor=0.25) = NonRejectingTrustRadiusUpdate(maxTrustRadius, minTrustRadius, increaseFactor, decreaseFactor)

function updateTrustRadius(self::NonRejectingTrustRadiusUpdate, stepControl::AbstractStepControl, energyChange::Float64)
    ρ = energyChange/getPredictedEnergyChnage(stepControl)
    if ρ > 0.75
        stepControl.trustRadius = min(self.increaseFactor*stepControl.trustRadius, self.maxTrustRadius)
    elseif ρ < 0.25
        stepControl.trustRadius = max(self.decreaseFactor*stepControl.trustRadius, self.minTrustRadius)
    end
    @info @sprintf("Energy: %15.10e, Expected Energy: %15.10e, trustRadius: %15.10e, quality: %5.2f\n", energyChange, getPredictedEnergyChnage(stepControl), stepControl.trustRadius, ρ)
    if ρ < 0.0
        @warn("This step was bad. I would have rejected it, but I am not rejecting anything! ρ = $(ρ) which means an increase in function value")
    end
    
    return true
end