using Printf

#Fletchers namely

struct StandardTrustRadiusUpdate <: AbstractTrustRadiusUpdate 
    maxTrustRadius::Float64
    minTrustRadius::Float64
    increaseFactor::Float64
    decreaseFactor::Float64
    rejectingWhenStepPredectionBelowThreshold::Float64
end

StandardTrustRadiusUpdate(; maxTrustRadius=1.0, minTrustRadius=0.001, increaseFactor=3.0, decreaseFactor=0.25, rejectingWhenStepPredectionBelowThreshold::Float64=0.0) = StandardTrustRadiusUpdate(maxTrustRadius, minTrustRadius, increaseFactor, decreaseFactor, rejectingWhenStepPredectionBelowThreshold)

function updateTrustRadius(self::StandardTrustRadiusUpdate, stepControl::AbstractStepControl, energyChange::Float64)
    ρ = energyChange/getPredictedEnergyChnage(stepControl)
    if ρ > 0.75# && 1.25*stepControl.lastNorm > stepControl.trustRadius
        stepControl.trustRadius = min(self.increaseFactor*stepControl.trustRadius, self.maxTrustRadius)
    elseif ρ < 0.25
        stepControl.trustRadius = max(self.decreaseFactor*stepControl.trustRadius, self.minTrustRadius)
    end
    @info @sprintf("Energy: %15.10e, Expected Energy: %15.10e, trustRadius: %15.10e, quality: %5.2f\n", energyChange, getPredictedEnergyChnage(stepControl), stepControl.trustRadius, ρ)
    if ρ < self.rejectingWhenStepPredectionBelowThreshold
        if abs(stepControl.trustRadius-self.minTrustRadius) < 1.0e-6
            @warn("Cannot reject step because TR is at its minimum: (TR) $(stepControl.trustRadius) == (Min) $(self.minTrustRadius)")
            return true
        else
            @warn("Rejecting step! ρ = $(ρ) which means an increase in function value")
            return false
        end
    end
    
    return true
end