mutable struct ContainerThatOnlyHoldsN{T}
    N::Int64
    elements::Array{T}
    function ContainerThatOnlyHoldsN{T}(N::Int64) where {T}
        if N <= 1
            throw(BoundsError("ContainerThatOnlyHoldsN container size must be larger than 1"))
        end
        new(N, Array{T}(undef, 0))
    end
end

function Base.pushfirst!(c::ContainerThatOnlyHoldsN{T}, newElement::T) where {T}
    if length(c.elements) >= c.N
        for i in c.N:-1:2
            c.elements[i] = c.elements[i-1]
        end
        c.elements[1] = newElement
    else
        pushfirst!(c.elements, newElement)
    end
end


function Base.push!(c::ContainerThatOnlyHoldsN{T}, newElement::T) where {T}
    if length(c.elements) >= c.N
        for i in 2:c.N
            c.elements[i-1] = c.elements[i]
        end
        c.elements[c.N] = newElement
    else
        push!(c.elements, newElement)
    end
end

function Base.get(c::ContainerThatOnlyHoldsN{T}, i::Int64) where {T}
    c.elements[i]
end

Base.length(c::ContainerThatOnlyHoldsN{T}) where {T} = length(c.elements)