const COVALENT_THRESHOLD::Float64 = 1.25
const VDW_THRESHOLD::Float64 = 0.9

const bohr2Ang = 0.52917721067

const ELECTRO_NEGATIVE_ELEMENTS::Array{String} = ["N", "O", "F", "P", "S", "Cl"]

function borh2Ang(coordinates::Array{Float64})
    coordinates.*bohr2Ang
end

function ang2Bohr(coordinates::Array{Float64})
    coordinates./bohr2Ang
end