function getCartesian(a::Int, cartesians::Array{Float64})
    cartesians[(a-1)*3+1:(a)*3]
end

function getDistanceBetweenCartesianPointAandB(a::Int64, b::Int64, cartesians::Array{Float64})
    a = getCartesian(a, cartesians)
    b = getCartesian(b, cartesians)

    norm(a-b)
end

cartesianFlattenTo2d(cartesians::AbstractArray{Float64}) = transpose(reshape(cartesians, 3, length(cartesians)÷3))

function unflattenCartesians(cartesians::AbstractArray{Float64,2})
    newCartesians = Array{Float64}(undef, size(cartesians, 1)*size(cartesians, 2))
    for i ∈ axes(cartesians, 1)
        for j ∈ axes(cartesians, 2)
            newCartesians[(i-1)*3+j] = cartesians[i, j]
        end
    end
    newCartesians
end

flattenGetBaryCenter(cartesians::AbstractArray{Float64}) = sum(cartesians[i:i+2] for i ∈ 1:3:length(cartesians))/(length(cartesians)÷3)

function flattenToBaryCenter(cartesians::AbstractArray{Float64})
    newCartesians = copy(cartesians)
    baryCenter = flattenGetBaryCenter(newCartesians)
    for i in 1:3:length(newCartesians)
        newCartesians[i:i+2] -= baryCenter
    end
    newCartesians
end

function getBaryCenter(cartesians::AbstractArray{Float64, 2})
    (sum(cartesians, dims=1)/size(cartesians, 1))'
end
    
function shiftCoordinatesToPoint(cartesians::AbstractArray{Float64, 2}, point::AbstractArray{Float64})
    for i ∈ axes(cartesians, 1)
        cartesians[i, :] -= point
    end
    cartesians
end

function toBaryCenter(cartesians::AbstractArray{Float64, 2})
    baryCenter = getBaryCenter(cartesians)    
    shiftCoordinatesToPoint(cartesians, baryCenter)
end