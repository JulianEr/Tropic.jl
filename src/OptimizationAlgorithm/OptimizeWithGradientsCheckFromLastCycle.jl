mutable struct OptimizeWithGradientsCheckFromLastCycle <: AbstractOptimizationAlgorithm
    optimizer::AbstractOptimizer
    maxIter::Int
    writeToLogger::Function
    transformationsFailedInARow::Int64
end

OptimizeWithGradientsCheckFromLastCycle(optimizer::AbstractOptimizer; maxIter::Int=100, writeToLogger::Function = (optimizer::AbstractOptimizer, count::Int)->@info(@sprintf("step %3d: Energy: %.15e", count, getEnergy(optimizer)))) = OptimizeWithGradientsCheckFromLastCycle(optimizer, maxIter, writeToLogger, 0)

function takeOneStep!(self::OptimizeWithGradientsCheckFromLastCycle)

    try
        Δxs = takeStep(self.optimizer)
        applyChange!(self.optimizer, Δxs)
    catch e
        if e isa CoordinateTransformationException
            @warn "Coordinate System could not transform gradients or Coordinates. Resetting the step and increasind the counter."
            self.transformationsFailedInARow += 1
            if self.transformationsFailedInARow == 3
                rethrow(e)
            end
            return false
        else
            rethrow(e)
        end
    end
    
    self.transformationsFailedInARow = 0

    true
end

function prepareNextStep!(self::OptimizeWithGradientsCheckFromLastCycle, snap::ResetData)
    getNewEnergyAndGradients!(self.optimizer)

    if !updateTrustRadius!(self.optimizer)
        @warn "Trust radius update was not happy with the Step. Resetting the step and increasind the counter."
        resetToSnapShot!(self.optimizer, snap)
    else
        getNewHessian!(self.optimizer)
        executeAfterStep!(self.optimizer)
    end
end

function optimizationLoop!(self::OptimizeWithGradientsCheckFromLastCycle)
    count = 0
    while true
        if count > self.maxIter
            @error @sprintf "Could not find saddle point!\nAfter %3d steps still not converged." count
            break
        end
        
        self.writeToLogger(self.optimizer, count)
        snap = takeSnapShot(self.optimizer)
        tookStep = takeOneStep!(self)
        if !tookStep
            resetToSnapShot!(self.optimizer, snap)
            resetCoordinateSystem!(self.optimizer)
            count += 1
            continue
        end
        @info "Cartesians after step" step=count cartesians=getCartesians(self.optimizer)
        if isConvergenceReached(self.optimizer)
            @info @sprintf "\nOptimization converged after %3d steps!" count+1
            break
        end

        prepareNextStep!(self, snap)

        count += 1
    end
end
        
function optimize!(self::OptimizeWithGradientsCheckFromLastCycle)
    @info "Starting Optimization"
    initialize!(self.optimizer)
    optimizationLoop!(self)
    getCartesians(self.optimizer)
end