struct AnalyticGradientsUpdatedHessian <: AbstractGradientsAndHessian
    symbols::Array{String}
    hessianUpdate::AbstractHessianUpdate
    engine::AbstractEngine
end

function getEnergyAndGradients(self::AnalyticGradientsUpdatedHessian, optimizer::AbstractOptimizer)
    E, g = getEnergyAndGradients(self.engine, self.symbols, getCartesians(optimizer))
    E, transformCartesianGradients(optimizer, g)
end

function notify!(self::AnalyticGradientsUpdatedHessian, optimizer::AbstractOptimizer)
    notify!(self.hessianUpdate, optimizer)
end

function getHessian(self::AnalyticGradientsUpdatedHessian, optimizer::AbstractOptimizer)
    updateHessian(self.hessianUpdate, optimizer)
end

function getAnalyticHessian(self::AnalyticGradientsUpdatedHessian, optimizer::AbstractOptimizer)
    g, H = getGradientsAndHessian(self.engine, self.symbols, getCartesians(optimizer))
    _, H_q = transformCartesianHessian(optimizer, H, g)
    Hermitian(H_q)
end