# COV_EXCL_START
value(self::AbstractInternalCoordinate, ::Array{Float64}) = error("\'value\' not Implemented for type $(typeof(self))")
derivativeVector(self::AbstractInternalCoordinate, ::Array{Float64}) = error("\'derivativeVector\' not Implemented for type $(typeof(self))")
secondDerivativeMatrix(self::AbstractInternalCoordinate, ::Array{Float64}) = error("\'secondDerivativeMatrix\' not Implemented for type $(typeof(self))")
getHessianGuessSchlegel(self::AbstractInternalCoordinate, ::Array{String}, ::Array{Float64}) = error("\'getHessianGuessSchlegel\' not Implemented for type $(typeof(self))")
getHessianGuessLindh(self::AbstractInternalCoordinate, ::Array{String}, ::Array{Float64}) = error("\'getHessianGuessLindh\' not Implemented for type $(typeof(self))")
# COV_EXCL_STOP