# COV_EXCL_START
optimize!(self::AbstractOptimizationAlgorithm) = error("\'optimize!\' seems not to be implemented for type $(typeof(self))")
# COV_EXCL_STOP