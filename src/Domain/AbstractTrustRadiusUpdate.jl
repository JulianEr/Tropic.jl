# COV_EXCL_START
updateTrustRadius(self::AbstractTrustRadiusUpdate, ::AbstractStepControl, ::Float64) = error("\'updateTrustRadius\' not Implemented for type $(typeof(self))")
# COV_EXCL_STOP