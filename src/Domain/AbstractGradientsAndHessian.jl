# COV_EXCL_START
getEnergyAndGradients(self::AbstractGradientsAndHessian, ::AbstractOptimizer) = error("\'getEnergyAndGradients\' not Implemented for type $(typeof(self))")
notify!(self::AbstractGradientsAndHessian, ::AbstractOptimizer) = error("\'getHessian\' not Implemented for type $(typeof(self))")
getHessian(self::AbstractGradientsAndHessian, ::AbstractOptimizer) = error("\'getHessian\' not Implemented for type $(typeof(self))")
getAnalyticHessian(self::AbstractGradientsAndHessian, ::AbstractOptimizer) = error("\'getAnalyticHessian\' not Implemented for type $(typeof(self))")
# COV_EXCL_STOP