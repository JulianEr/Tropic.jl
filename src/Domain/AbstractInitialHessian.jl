# COV_EXCL_START
getInitialHessian(self::AbstractInitialHessian, ::AbstractOptimizer) = error("\'getInitialHessian\' seems not to be implemented for type $(typeof(self))")
# COV_EXCL_STOP