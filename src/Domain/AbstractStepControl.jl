# COV_EXCL_START
takeStep!(self::AbstractStepControl, ::AbstractOptimizer) = error("'takeStep!' not Implemented for type $(typeof(self))")
getPredictedEnergyChnage(self::AbstractStepControl) = error("'getPredictedEnergyChnage' not Implemented for type $(typeof(self))")
# COV_EXCL_STOP