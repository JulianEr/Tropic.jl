# COV_EXCL_START
getInternalsSize(self::AbstractZmatrix) = error("\'getInternalsSize\' not Implemented for type $(typeof(self))")
getInternalValueArray(self::AbstractZmatrix, ::Array{Float64}) = error("\'getInternalValueArray\' not Implemented for type $(typeof(self))")
toCartesians(self::AbstractZmatrix, ::Array{Float64}) = error("\'toCartesians\' not Implemented for type $(typeof(self))")
printZmatrix(self::AbstractZmatrix, ::Array{String}, ::Array{Float64}) = error("\'printZmatrix\' not Implemented for type $(typeof(self))")
printZmatrixFromCartesians(::AbstractZmatrix, ::Array{String}, ::Array{Float64}) = error("\'printZmatrixFromCartesians\' not Implemented for type $(typeof(self))")
getBmatrix(self::AbstractZmatrix, ::Array{Float64}) = error("\'getBmatrix\' not Implemented for type $(typeof(self))")
getDerivativeOfBmatrix(self::AbstractZmatrix, ::Array{Float64}) = error("\'getDerivativeOfBmatrix\' not Implemented for type $(typeof(self))")
getHessianGuessLindh(self::AbstractZmatrix, ::Array{String}, ::Array{Float64}) = error("\'getHessianGuessLindh\' not Implemented for type $(typeof(self))")
getHessianGuessSchlegel(self::AbstractZmatrix, ::Array{String}, ::Array{Float64}) = error("\'getHessianGuessSchlegel\' not Implemented for type $(typeof(self))")
removeOverwindStartingAtOffset!(self::AbstractZmatrix, ::Array{Float64}, ::Int64) = error("\'removeOverwindStartingAtOffset!\' not Implemented for type $(typeof(self))")
hasTranslationAndRotation(self::TRZmatrix) = error("\'hasTranslationAndRotation\' not Implemented for type $(typeof(self))")
# COV_EXCL_STOP