# COV_EXCL_START
isConvergenceReached(self::AbstractConvergenceCheck, ::AbstractOptimizer) = error("isConvergenceReached not Implemented for type $(typeof(self))")
# COV_EXCL_STOP