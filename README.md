![](icon.svg)

# Tropic

Tropic is an optimization package for molecular geometries. For energy, gradient, and Hessian information Tropic uses Engines, which serve as interfaces to external applications. External applications are Psi4 and Xtb. 

## Installation

Tropic was developed and tested using Julia Version 1.9.2
To use Tropic as a library simple use 

```
using Pkg
Pkg.add(url="https://gitlab.com/JulianEr/Tropic.jl")
```

Then you will be able to use Tropic in your Julia-Scripts. To ship Julia as a command line tool you need to clone this repository and have Julia installed. Switch into the repositories directory and execute

```
julia .\src\buildApp.jl
```

This will take a few minutes. In the generated folder `build/bin` you should find an executable file called `Tropic.exe`. You are now ready to distribute this folder or add it to your `path` environment variable and use Tropics CLI to your hearts content.

As an example: Let's say you created a folder under the root directory called `calc` and in that folder you have a file with a xyz-structre called `water.xyz`. And you have Psi4 installed, and it is accessible via `psi4.bat`. Then you can start a calculation with 

```
..\build\bin\Tropic.exe --inputFile .\water.xyz --engine Psi4 path psi4.bat
```

use

```
..\build\bin\Tropic.exe --help
```

for a complete list of possible parameters.

## Develop Tropic

Checkout the Repository, change directory to the root folder of Tropic and add Tropic to your packages at its current state with:

```
using Pkg
Pkg.develop(path=".")
```

## Documentation

Will follow soon (hopefully)

## Citations

Will also follow soon (hopefully) at least I will update this place with my PhD Thesis as soon as it gets accepted :)
