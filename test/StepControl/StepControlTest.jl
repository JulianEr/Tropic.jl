using Test, LinearAlgebra

import Tropic

include("data.jl")

include("OptimizerRfoDouble.jl")

@info "Start RFO tests"
include("RfoTest.jl")

rsrfoTests()