struct OptimizerRfoDouble <: Tropic.AbstractOptimizer end

Tropic.getActiveGradients(::OptimizerRfoDouble) = h2o2twiceActiveInternalGradientsForRfo
Tropic.getActiveHessian(::OptimizerRfoDouble) = h2o2twiceActiveInternalHessianForRfo