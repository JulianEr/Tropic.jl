@testset "ContainerThatOnlyHoldsN Tests" begin
    @testset "Test Inserts push!" begin
        container = ContainerThatOnlyHoldsN{Array{Float64}}(3)
        push!(container, [1.0,2.0,3.0])
        @test length(container) == 1
        push!(container, [4.0,5.0])
        @test length(container) == 2
        push!(container, [6.0])
        @test length(container) == 3
        get(container, 2)[2] = 42.0
        push!(container, [7.0,8.0,9.0])
        @test length(container) == 3
        @test get(container, 1) == [4.0,42.0]
    end
end

@testset "ContainerThatOnlyHoldsN Tests" begin
    @testset "Test Inserts pushfirst!" begin
        container = ContainerThatOnlyHoldsN{Array{Float64}}(3)
        pushfirst!(container, [1.0,2.0,3.0])
        @test length(container) == 1
        pushfirst!(container, [4.0,5.0])
        @test length(container) == 2
        pushfirst!(container, [6.0])
        @test length(container) == 3
        get(container, 2)[2] = 42.0
        pushfirst!(container, [7.0,8.0,9.0])
        @test length(container) == 3
        @test get(container, 3) == [4.0,42.0]
    end
end