using Pkg

cd("$(@__DIR__)/../../")

Pkg.activate(".")
Pkg.test(;coverage=true)

using Coverage
using Printf

covered_lines, total_lines = get_summary(process_folder())

coverage = covered_lines/total_lines*100.0

@printf "%6.2f%% of code covered\n" coverage

coverage_threshold = parse(Float64, ENV["TEST_COVERAGE_THRESHOLD"])

if coverage < coverage_threshold
    @printf "Coverage of %6.2f%% does not reach the quality gate of at least %6.2f%% of coverage\n" coverage coverage_threshold
    exit(1)
end