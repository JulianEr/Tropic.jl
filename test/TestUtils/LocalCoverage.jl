using Pkg

cd("$(@__DIR__)/../../")

Pkg.activate(".")
Pkg.test(;coverage=true)

using Coverage
using Printf

outputs = String[]

try 
    coverage = process_folder()

    covered_lines, total_lines = get_summary(coverage)

    LCOV.writefile("lcov.info", coverage)

    push!(outputs, @sprintf "%d of %d lines covered" covered_lines total_lines)
    push!(outputs, @sprintf "%6.2f %% of code covered" covered_lines/total_lines*100.0)
finally
    clean_folder(".")
end

for o in outputs
    println(o)
end