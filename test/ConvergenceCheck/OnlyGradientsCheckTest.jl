@testset "Only Gradients Norm Check Tests" begin
    convergenceCheck = Tropic.OnlyGradientsCheck()

    convergedOptimizer = ConvergenceCheckOptimizerDouble(convergedGradients_ConvergenceCheckTestData)
    @test Tropic.isConvergenceReached(convergenceCheck, convergedOptimizer)

    nonConvergedOptimizer = ConvergenceCheckOptimizerDouble(nonConvergedGradients_ConvergenceCheckTestData)
    @test !Tropic.isConvergenceReached(convergenceCheck, nonConvergedOptimizer)
end