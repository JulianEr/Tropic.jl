@testset "Gradients And Chnage Check Tests" begin
    convergenceCheck = Tropic.GradientsAndChangeCheck()

    convergedOptimizer = ConvergenceCheckOptimizerDouble(convergedChange_ConvergenceCheckTestData, convergedGradients_ConvergenceCheckTestData)
    @test Tropic.isConvergenceReached(convergenceCheck, convergedOptimizer)

    nonConvergedOptimizerBecauseOfChange = ConvergenceCheckOptimizerDouble(nonConvergedChange_ConvergenceCheckTestData, convergedGradients_ConvergenceCheckTestData)
    @test !Tropic.isConvergenceReached(convergenceCheck, nonConvergedOptimizerBecauseOfChange)

    nonConvergedOptimizerBecauseOfGradients = ConvergenceCheckOptimizerDouble(convergedChange_ConvergenceCheckTestData, nonConvergedGradients_ConvergenceCheckTestData)
    @test !Tropic.isConvergenceReached(convergenceCheck, nonConvergedOptimizerBecauseOfGradients)

    nonConvergedOptimizerBecauseOfChangeAndGradients = ConvergenceCheckOptimizerDouble(nonConvergedChange_ConvergenceCheckTestData, nonConvergedGradients_ConvergenceCheckTestData)
    @test !Tropic.isConvergenceReached(convergenceCheck, nonConvergedOptimizerBecauseOfChangeAndGradients)
end