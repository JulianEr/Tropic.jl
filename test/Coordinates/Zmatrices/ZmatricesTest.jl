using Test

import Tropic

include("Builders/ZmatrixBuildersTest.jl")
include("Lines/LinesTest.jl")

include("ZmatricesTestData.jl")

include("ZmatrixTest.jl")
include("TranslationRotationZmatrixFragmentTest.jl")
include("TranslationRotationZmatrixTest.jl")

zmatrixTest()
translationRotationZmatrixFragmentTest()
translationRotationZmatrixTest()