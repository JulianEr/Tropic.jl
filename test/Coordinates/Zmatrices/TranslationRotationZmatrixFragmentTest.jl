function translationRotationZmatrixFragmentTest()
    @testset "Tranlsation Rotation Z-Matrix Fragment Tests" begin
        @testset "Interal Size For TR Z-Matrix Fragment Tests" begin
            @test Tropic.getInternalsSize(h2o2twicZmatrixFragment_ZmatrixTestData) == 12
        end

        @testset "Internal Value Array for TR Z-Matrix Fragment Tests" begin
            @test norm(Tropic.getInternalValueArray(h2o2twicZmatrixFragment_ZmatrixTestData, h2o2twiceCartesians_ZmatrixTestData) - h2o2twiceInternalValueArray_ZmatrixTestData[13:end]) < 1e-7
            @test norm(Tropic.getInternalValueArray(h2o2twicZmatrixFragment_ZmatrixTestData, h2o2twiceRotatedCartesians_ZmatrixTestData) - h2o2twiceRotatedInternalValueArray_ZmatrixTestData[13:end]) < 1e-7
        end

        @testset "Add to B-Matrix for TR Z-Matrix Fragment Tests" begin
            emptyBmatrix = zeros(24, 24)
            sizeOfMolecule = Tropic.addToBmatrixGetSize!(h2o2twicZmatrixFragment_ZmatrixTestData, emptyBmatrix, h2o2twiceCartesians_ZmatrixTestData, 12)

            @test norm(emptyBmatrix[1:12]) < 1e-7
            @test norm(emptyBmatrix[13:end, :] - h2o2twiceBmatrix_ZmatrixTestData[13:end, :]) < 1e-7
            @test sizeOfMolecule == 12

            emptyBmatrix = zeros(24, 24)
            sizeOfMolecule = Tropic.addToBmatrixGetSize!(h2o2twicZmatrixFragment_ZmatrixTestData, emptyBmatrix, h2o2twiceRotatedCartesians_ZmatrixTestData, 12)

            @test norm(emptyBmatrix[1:12]) < 1e-7
            @test norm(emptyBmatrix[13:end, :] - h2o2twiceRotatedBmatrix_ZmatrixTestData[13:end, :]) < 1e-7
            @test sizeOfMolecule == 12
        end

        @testset "Add to B-Matrix Derivatives for TR Z-Matrix Fragment Tests" begin
            emptyBmatrix = zeros(24, 24, 24)
            sizeOfMolecule = Tropic.addToDerivativeOfBmatrixGetSize!(h2o2twicZmatrixFragment_ZmatrixTestData, emptyBmatrix, h2o2twiceCartesians_ZmatrixTestData, 12)

            @test norm(emptyBmatrix[1:12]) < 1e-7
            @test norm(emptyBmatrix[13:end, :, :] - h2o2twiceBmatrixDerivatives_ZmatrixTestData[13:end, :, :]) < 1e-7
            @test sizeOfMolecule == 12

            emptyBmatrix = zeros(24, 24, 24)
            sizeOfMolecule = Tropic.addToDerivativeOfBmatrixGetSize!(h2o2twicZmatrixFragment_ZmatrixTestData, emptyBmatrix, h2o2twiceRotatedCartesians_ZmatrixTestData, 12)

            @test norm(emptyBmatrix[1:12]) < 1e-7
            @test norm(emptyBmatrix[13:end, :, :] - h2o2twiceRotatedBmatricDerivatives_ZmatrixTestData[13:end, :, :]) < 1e-7
            @test sizeOfMolecule == 12
        end

        @testset "Add to Hessian Guess for TR Z-Matrix Fragment" begin
            h = zeros(24)

            sizeOfMolecule = Tropic.addToHessianGuessGetSize!(h2o2twicZmatrixFragment_ZmatrixTestData, h, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceCartesians_ZmatrixTestData, 12)
            
            @test norm(h[1:12]) < 1e-7
            @test norm(h[13:end] - h2o2twiceGuessHessianValues_ZmatrixTestData[13:end]) < 1e-7
            @test sizeOfMolecule == 12

            h = zeros(24)

            sizeOfMolecule = Tropic.addToHessianGuessGetSize!(h2o2twicZmatrixFragment_ZmatrixTestData, h, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceRotatedCartesians_ZmatrixTestData, 12)
            
            @test norm(h[1:12]) < 1e-7
            @test norm(h[13:end] - h2o2twiceRotatedGuessHessianValues_ZmatrixData[13:end]) < 1e-7
            @test sizeOfMolecule == 12
        end
        

        @testset "Add to Hessian Guess Schlegel for TR Z-Matrix Fragment" begin
            h = zeros(24)

            sizeOfMolecule = Tropic.addToHessianGuessSchlegelGetSize!(h2o2twicZmatrixFragment_ZmatrixTestData, h, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceCartesians_ZmatrixTestData, 12)
            
            @test norm(h[1:12]) < 1e-7
            @test norm(h[13:end] - h2o2twiceGuessHessianSchlegelValues_ZmatrixTestData[13:end]) < 1e-7
            @test sizeOfMolecule == 12

            h = zeros(24)

            sizeOfMolecule = Tropic.addToHessianGuessSchlegelGetSize!(h2o2twicZmatrixFragment_ZmatrixTestData, h, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceRotatedCartesians_ZmatrixTestData, 12)
            
            @test norm(h[1:12]) < 1e-7
            @test norm(h[13:end] - h2o2twiceRotatedGuessHessianSchlegelValues_ZmatrixTestData[13:end]) < 1e-7
            @test sizeOfMolecule == 12
        end

        @testset "Remove Overwind for TR Z-Matrix Tests" begin
            testData = deepcopy(h2o2twiceInternalValueArray_ZmatrixTestData)

            testData[16] = -1.5π
            testData[17] = 1.5π
            testData[18] = -0.5π
            testData[24] = -1.5π

            Tropic.removeOverwindStartingAtOffsetGetNewOffset!(h2o2twicZmatrixFragment_ZmatrixTestData, testData, 12)

            @test testData[16] == 0.5π
            @test testData[17] == -0.5π
            @test testData[18] == -0.5π
            @test testData[24] == 0.5π
        end
    end
end

# function removeOverwindStartingAtOffsetGetNewOffset!(self::TRZmatrixFragment, values::Array{Float64}, internalsOffset::Int64)
#     values[internalsOffset+4] = keepPiInRange(values[internalsOffset+4])
#     values[internalsOffset+5] = keepPiInRange(values[internalsOffset+5])
#     values[internalsOffset+6] = keepPiInRange(values[internalsOffset+6])

#     removeOverwindStartingAtOffset!(self.zmatrix, values, internalsOffset+6)
#     internalsOffset+getInternalsSize(self)
# end