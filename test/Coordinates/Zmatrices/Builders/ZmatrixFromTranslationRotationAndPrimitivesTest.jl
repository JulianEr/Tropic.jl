function zmatrixFromTranslationAndRotationAndPrimitivesTest()
    @testset "Zmatrix from Translation and Rotation Test" begin
        @testset "Find Translation Tests" begin
            @test Tropic.findTranslationX(someTranslationX_ZmatrixBuildersTest, exampleMoleculeToSearchFor_ZmatrixBuildersTest) == (expectedTranslationX_ZmatrixBuildersTest, 2)
            @test Tropic.findTranslationY(someTranslationY_ZmatrixBuildersTest, exampleMoleculeToSearchFor_ZmatrixBuildersTest) == (expectedTranslationY_ZmatrixBuildersTest, 2)
            @test Tropic.findTranslationZ(someTranslationZ_ZmatrixBuildersTest, exampleMoleculeToSearchFor_ZmatrixBuildersTest) == (expectedTranslationZ_ZmatrixBuildersTest, 2)

            @test_throws ErrorException Tropic.findTranslationX(someTranslationX_ZmatrixBuildersTest, [42,43,44])
            @test_throws ErrorException Tropic.findTranslationY(someTranslationY_ZmatrixBuildersTest, [42,43,44])
            @test_throws ErrorException Tropic.findTranslationZ(someTranslationZ_ZmatrixBuildersTest, [42,43,44])

            @test Tropic.getTranslationsForMolecule(trAndPrimitives_ZmatrixBuildersTest, exampleMoleculeToSearchFor_ZmatrixBuildersTest) == ((Tropic.TranslationX(Tropic.Translation([5, 6, 7, 8])), Tropic.TranslationY(Tropic.Translation([5, 6, 7, 8])), Tropic.TranslationZ(Tropic.Translation([5, 6, 7, 8]))), (2, 5, 8))
        end
        @testset "Find Rotations Tests" begin
            @test Tropic.findRotationA(someRotationA_ZmatrixBuildersTest, exampleMoleculeToSearchFor_ZmatrixBuildersTest) == (expectedRotationA_ZmatrixBuildersTest, 2)
            @test Tropic.findRotationB(someRotationB_ZmatrixBuildersTest, exampleMoleculeToSearchFor_ZmatrixBuildersTest) == (expectedRotationB_ZmatrixBuildersTest, 2)
            @test Tropic.findRotationC(someRotationC_ZmatrixBuildersTest, exampleMoleculeToSearchFor_ZmatrixBuildersTest) == (expectedRotationC_ZmatrixBuildersTest, 2)
        
            @test_throws ErrorException Tropic.findRotationA(someRotationA_ZmatrixBuildersTest, [42,43,44])
            @test_throws ErrorException Tropic.findRotationB(someRotationB_ZmatrixBuildersTest, [42,43,44])
            @test_throws ErrorException Tropic.findRotationC(someRotationC_ZmatrixBuildersTest, [42,43,44])

            @test Tropic.getRotationsForMolecules(trAndPrimitives_ZmatrixBuildersTest, exampleMoleculeToSearchFor_ZmatrixBuildersTest) == ((Tropic.RotationA(Tropic.Rotation([5, 6, 7, 8], [0.0 0.0 0.0; 0.0 0.0 0.0; 0.0 0.0 0.0; 0.0 0.0 0.0], 0.0)), Tropic.RotationB(Tropic.Rotation([5, 6, 7, 8], [0.0 0.0 0.0; 0.0 0.0 0.0; 0.0 0.0 0.0; 0.0 0.0 0.0], 0.0)), Tropic.RotationC(Tropic.Rotation([5, 6, 7, 8], [0.0 0.0 0.0; 0.0 0.0 0.0; 0.0 0.0 0.0; 0.0 0.0 0.0], 0.0))), (11, 14, 17))
        end

        @testset "Create Z-Matrix from Internal Coordinates test" begin
            _, icPositions = Tropic.zMatrixForRedundantInternalCoordinates(trAndPrimitives_ZmatrixBuildersTest)

            @test icPositions == expectedFoundInternalCoordinatesForZmatrix_ZmatrixBuildersTest
        end
    end
end