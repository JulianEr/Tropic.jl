using Test, LightGraphs

import Tropic

include("ZmatrixBuildersTestData.jl")
include("ZmatrixFromPrimitivesTest.jl")
include("ZmatrixFromTranslationRotationAndPrimitivesTest.jl")
include("ZmatrixWithConstraintsTest.jl")

zmatrixFromPrimitivesTest()
zmatrixFromTranslationAndRotationAndPrimitivesTest()
zmatrixWithConstraintsTest()