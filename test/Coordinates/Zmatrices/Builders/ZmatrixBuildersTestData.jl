const someBonds_ZmatrixBuildersTest = [Tropic.Bond(1, 2), Tropic.Bond(2, 3), Tropic.Bond(3, 4), Tropic.Bond(4, 5), Tropic.Bond(5, 6)]

const expectedBondToBeFound_ZmatrixBuildersTest = Tropic.Bond(2, 3)

const someAngles_ZmatrixBuildersTest = [Tropic.Angle(1, 2, 3), Tropic.Angle(2, 3, 4), Tropic.Angle(3, 4, 5), Tropic.Angle(4, 5, 6)]

const expectedAngleToBeFound_ZmatrixBuildersTest = Tropic.Angle(2, 3, 4)

const someDihedrals_ZmatrixBuildersTest = [Tropic.Dihedral(1, 2, 3, 4), Tropic.Dihedral(2, 3, 4, 5), Tropic.Dihedral(3, 4, 5, 6)]

const expectedDihedralToBeFound_ZmatrixBuildersTest = Tropic.Dihedral(2, 3, 4, 5)

const primitives_ZmatrixBuildersTest = Tropic.Primitives(someBonds_ZmatrixBuildersTest, someAngles_ZmatrixBuildersTest, someDihedrals_ZmatrixBuildersTest)

const primitivesOneAtom_ZmatrixBuildersTest = Tropic.Primitives([],[],[])

const primitivesTwoAtoms_ZmatrixBuildersTest = Tropic.Primitives([Tropic.Bond(1,2)],[],[])

const primitivesThreeAtoms_ZmatrixBuildersTest = Tropic.Primitives([Tropic.Bond(1,2),Tropic.Bond(3,2)],[Tropic.Angle(3,2,1)],[])

const expectedResultOfBuildingZmatrix_ZmatrixBuildersTest = [(1, 0, 0), (2, 6, 0), (3, 7, 10), (4, 8, 11), (5, 9, 12)]

const someTranslationX_ZmatrixBuildersTest = [Tropic.TranslationX([1,2,3,4]), Tropic.TranslationX([5,6,7,8]), Tropic.TranslationX([9,10,11,12])]
const someTranslationY_ZmatrixBuildersTest = [Tropic.TranslationY([1,2,3,4]), Tropic.TranslationY([5,6,7,8]), Tropic.TranslationY([9,10,11,12])]
const someTranslationZ_ZmatrixBuildersTest = [Tropic.TranslationZ([1,2,3,4]), Tropic.TranslationZ([5,6,7,8]), Tropic.TranslationZ([9,10,11,12])]

const expectedTranslationX_ZmatrixBuildersTest = Tropic.TranslationX([5,6,7,8])
const expectedTranslationY_ZmatrixBuildersTest = Tropic.TranslationY([5,6,7,8])
const expectedTranslationZ_ZmatrixBuildersTest = Tropic.TranslationZ([5,6,7,8])

const someRotationA_ZmatrixBuildersTest = [Tropic.RotationA([1,2,3,4], zeros(36)), Tropic.RotationA([5,6,7,8], zeros(36)), Tropic.RotationA([9,10,11,12], zeros(36))]
const someRotationB_ZmatrixBuildersTest = [Tropic.RotationB([1,2,3,4], zeros(36)), Tropic.RotationB([5,6,7,8], zeros(36)), Tropic.RotationB([9,10,11,12], zeros(36))]
const someRotationC_ZmatrixBuildersTest = [Tropic.RotationC([1,2,3,4], zeros(36)), Tropic.RotationC([5,6,7,8], zeros(36)), Tropic.RotationC([9,10,11,12], zeros(36))]

const expectedRotationA_ZmatrixBuildersTest = Tropic.RotationA([5,6,7,8], zeros(36))
const expectedRotationB_ZmatrixBuildersTest = Tropic.RotationB([5,6,7,8], zeros(36))
const expectedRotationC_ZmatrixBuildersTest = Tropic.RotationC([5,6,7,8], zeros(36))

const exampleMoleculeToSearchFor_ZmatrixBuildersTest = [5,6,7,8]

const someTrBonds_ZmatrixBuildersTest = [Tropic.Bond(1,2),Tropic.Bond(2,3), Tropic.Bond(3,4),Tropic.Bond(5,6),Tropic.Bond(6,7), Tropic.Bond(7,8),Tropic.Bond(9,10),Tropic.Bond(10,11), Tropic.Bond(11,12)]
const someTrAngles_ZmatrixBuildersTest = [Tropic.Angle(1,2,3), Tropic.Angle(2,3,4),Tropic.Angle(5,6,7), Tropic.Angle(6,7,8),Tropic.Angle(9,10,11), Tropic.Angle(10,11,12)]
const someTrDihedrals_ZmatrixBuildersTest = [Tropic.Dihedral(1,2,3,4),Tropic.Dihedral(5,6,7,8),Tropic.Dihedral(9,10,11,12)]

const trPrimitives_ZmatrixBuildersTest = Tropic.Primitives(someTrBonds_ZmatrixBuildersTest,someTrAngles_ZmatrixBuildersTest,someTrDihedrals_ZmatrixBuildersTest)
const trAndPrimitives_ZmatrixBuildersTest = Tropic.TranslationRotationAndPrimitives([[1,2,3,4],[5,6,7,8],[9,10,11,12]],someTranslationX_ZmatrixBuildersTest,someTranslationY_ZmatrixBuildersTest,someTranslationZ_ZmatrixBuildersTest,someRotationA_ZmatrixBuildersTest,someRotationB_ZmatrixBuildersTest,someRotationC_ZmatrixBuildersTest,trPrimitives_ZmatrixBuildersTest)

const expectedFoundInternalCoordinatesForZmatrix_ZmatrixBuildersTest = [(1, 4, 7), (10, 13, 16), (19, 0, 0), (20, 28, 0), (21, 29, 34), (2, 5, 8), (11, 14, 17), (22, 0, 0), (23, 30, 0), (24, 31, 35), (3, 6, 9), (12, 15, 18), (25, 0, 0), (26, 32, 0), (27, 33, 36)]

const constrainablePrimitiveSystem_ZmatrixBuildersTest = Tropic.Primitives(Tropic.Bond[Tropic.Bond(1, 2), Tropic.Bond(2, 3), Tropic.Bond(3, 4), Tropic.Bond(3, 6), Tropic.Bond(4, 5), Tropic.Bond(4, 7), Tropic.Bond(5, 8), Tropic.Bond(5, 9), Tropic.Bond(6, 10), Tropic.Bond(6, 11), Tropic.Bond(7, 12), Tropic.Bond(7, 13)], Tropic.Angle[Tropic.Angle(1, 2, 3), Tropic.Angle(2, 3, 4), Tropic.Angle(2, 3, 6), Tropic.Angle(4, 3, 6), Tropic.Angle(3, 4, 5), Tropic.Angle(3, 4, 7), Tropic.Angle(5, 4, 7), Tropic.Angle(4, 5, 8), Tropic.Angle(4, 5, 9), Tropic.Angle(8, 5, 9), Tropic.Angle(3, 6, 10), Tropic.Angle(3, 6, 11), Tropic.Angle(10, 6, 11), Tropic.Angle(4, 7, 12), Tropic.Angle(4, 7, 13), Tropic.Angle(12, 7, 13)], Tropic.Dihedral[Tropic.Dihedral(1, 2, 3, 4), Tropic.Dihedral(1, 2, 3, 6), Tropic.Dihedral(2, 3, 4, 5), Tropic.Dihedral(2, 3, 4, 7), Tropic.Dihedral(6, 3, 4, 5), Tropic.Dihedral(6, 3, 4, 7), Tropic.Dihedral(2, 3, 6, 10), Tropic.Dihedral(2, 3, 6, 11), Tropic.Dihedral(4, 3, 6, 10), Tropic.Dihedral(4, 3, 6, 11), Tropic.Dihedral(3, 4, 5, 8), Tropic.Dihedral(3, 4, 5, 9), Tropic.Dihedral(7, 4, 5, 8), Tropic.Dihedral(7, 4, 5, 9), Tropic.Dihedral(3, 4, 7, 12), Tropic.Dihedral(3, 4, 7, 13), Tropic.Dihedral(5, 4, 7, 12), Tropic.Dihedral(5, 4, 7, 13)])
const constraintsForPrimitiveSystem_ZmatrixBuildersTest = Tropic.Primitives(Tropic.Bond[Tropic.Bond(1,2)], Tropic.Angle[Tropic.Angle(3,6,10),Tropic.Angle(1,2,3)], Tropic.Dihedral[Tropic.Dihedral(7,4,5,8)])
const constraintsForPrimitiveSystemReverted_ZmatrixBuildersTest = Tropic.Primitives(Tropic.Bond[Tropic.Bond(2,1)], Tropic.Angle[Tropic.Angle(10,6,3),Tropic.Angle(3,2,1)], Tropic.Dihedral[Tropic.Dihedral(8,5,4,7)])
const constraintsForPrimitiveSystemSameLine_ZmatrixBuildersTest = Tropic.Primitives(Tropic.Bond[Tropic.Bond(3,4)], Tropic.Angle[Tropic.Angle(2,3,4)], Tropic.Dihedral[Tropic.Dihedral(1,2,3,4)])
const constraintsForPrimitiveSystemSameCoordinateTwiceAngle_ZmatrixBuildersTest = Tropic.Primitives(Tropic.Bond[], Tropic.Angle[Tropic.Angle(3,4,7), Tropic.Angle(5,4,7)], Tropic.Dihedral[])
const constraintsForPrimitiveSystemSameCoordinateTwiceDihedral_ZmatrixBuildersTest = Tropic.Primitives(Tropic.Bond[], Tropic.Angle[], Tropic.Dihedral[Tropic.Dihedral(3,4,5,8), Tropic.Dihedral(7,4,5,8)])
const constraintsForPrimitiveSystemMoreInOneLine_ZmatrixBuildersTest = Tropic.Primitives(Tropic.Bond[Tropic.Bond(3,4), Tropic.Bond(4,5), Tropic.Bond(3,6)], Tropic.Angle[Tropic.Angle(2,3,4), Tropic.Angle(3,4,7), Tropic.Angle(4,3,6)], Tropic.Dihedral[Tropic.Dihedral(1,2,3,4), Tropic.Dihedral(2,3,4,5), Tropic.Dihedral(2,3,4,7)])

const someAngleLines_ZmatrixBuildersTest = Tuple{Int, Tuple{Tropic.Bond, Tropic.Angle}}[(3, (Tropic.Bond(2,3), Tropic.Angle(1,2,3))), (4, (Tropic.Bond(3,4), Tropic.Angle(2,3,4)))]
const someOtherAngleLines_ZmatrixBuildersTest = Tuple{Int, Tuple{Tropic.Bond, Tropic.Angle}}[(4, (Tropic.Bond(3,4), Tropic.Angle(2,3,4)))]
const someBondLines_ZmatrixBuildersTest = Tuple{Int, Tropic.Bond}[(2, Tropic.Bond(1,2)), (3, Tropic.Bond(2,3)), (5, Tropic.Bond(4,5))]
const someOtherBondLines_ZmatrixBuildersTest = Tuple{Int, Tropic.Bond}[(3, Tropic.Bond(2,3))]
const evenMoreBondLines_ZmatrixBuildersTest = Tuple{Int, Tropic.Bond}[(5, Tropic.Bond(4,5))]