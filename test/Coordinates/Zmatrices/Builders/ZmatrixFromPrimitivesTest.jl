function zmatrixFromPrimitivesTest() 
    @testset "Zmatrix from primitives tests" begin
        @testset "Find Bonds Tests" begin
            @test Tropic.findBond(someBonds_ZmatrixBuildersTest, 2, 3) == (expectedBondToBeFound_ZmatrixBuildersTest, 2)
            @test Tropic.findBond(someBonds_ZmatrixBuildersTest, 3, 2) == (expectedBondToBeFound_ZmatrixBuildersTest, 2)
            @test_throws ErrorException Tropic.findBond(someBonds_ZmatrixBuildersTest, 100, 101)
        end

        @testset "Find Angles Tests" begin
            @test Tropic.findAngle(someAngles_ZmatrixBuildersTest, 2, 3, 4) == (expectedAngleToBeFound_ZmatrixBuildersTest, 2)
            @test Tropic.findAngle(someAngles_ZmatrixBuildersTest, 4, 3, 2) == (expectedAngleToBeFound_ZmatrixBuildersTest, 2)
            @test_throws ErrorException Tropic.findAngle(someAngles_ZmatrixBuildersTest, 3, 4, 2)
        end

        @testset "Find Dihedrals Tests" begin
            @test Tropic.findDihedral(someDihedrals_ZmatrixBuildersTest, 5) == (expectedDihedralToBeFound_ZmatrixBuildersTest, 2)
            @test_throws ErrorException Tropic.findDihedral(someDihedrals_ZmatrixBuildersTest, 7)
        end

        @testset "Find Lines Tests" begin
            @test Tropic.findSecondLine(someBonds_ZmatrixBuildersTest, 2) == (Tropic.SecondLine(2, Tropic.Bond(2, 3)), 2)
            @test Tropic.findThirdLine(someBonds_ZmatrixBuildersTest, someAngles_ZmatrixBuildersTest, 2) == (Tropic.ThirdLine(3, Tropic.Bond(3, 4), Tropic.Angle(2, 3, 4)), (3, 2))
            @test Tropic.findDefaultLine(5, 5, someBonds_ZmatrixBuildersTest, someAngles_ZmatrixBuildersTest, someDihedrals_ZmatrixBuildersTest) == (Tropic.DefaultLine(5, Tropic.Bond(4, 5), Tropic.Angle(3, 4, 5), Tropic.Dihedral(2, 3, 4, 5)), (4, 3, 2))
        end

        @testset "Create Z-Matrix from Primitives Test" begin
            @test Tropic.getNumberOfLines(primitivesOneAtom_ZmatrixBuildersTest) == 1
            @test Tropic.getNumberOfLines(primitivesTwoAtoms_ZmatrixBuildersTest) == 2
            @test Tropic.getNumberOfLines(primitivesThreeAtoms_ZmatrixBuildersTest) == 3
            @test Tropic.getNumberOfLines(primitives_ZmatrixBuildersTest) == 6

            _, icPositionsOneAtom = Tropic.zMatrixForRedundantInternalCoordinates(primitivesOneAtom_ZmatrixBuildersTest)
            @test isempty(icPositionsOneAtom)

            _, icPositionsTwoAtoms = Tropic.zMatrixForRedundantInternalCoordinates(primitivesTwoAtoms_ZmatrixBuildersTest)
            @test icPositionsTwoAtoms == [(1,0,0)]

            _, icPositionsThreeAtoms = Tropic.zMatrixForRedundantInternalCoordinates(primitivesThreeAtoms_ZmatrixBuildersTest)
            @test icPositionsThreeAtoms == [(1,0,0), (2,3,0)]

            _, icPositions = Tropic.zMatrixForRedundantInternalCoordinates(primitives_ZmatrixBuildersTest)
            @test icPositions == expectedResultOfBuildingZmatrix_ZmatrixBuildersTest
        end
    end
end