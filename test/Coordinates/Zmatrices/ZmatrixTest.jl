function zmatrixTest()
    @testset "Tests for Zmatrix with primitive internal Coordinates" begin
        @testset "Internal Value Array Tests" begin
            @test norm(Tropic.getInternalValueArray(h2o2Zmatrix_ZmatrixTestData, h2o2Cartesians_ZmatrixTestData) - h2o2InternalValueArray_ZmatrixTestData) < 1e-7
        end

        @testset "Internals Size Tests" begin
            @test Tropic.getInternalsSize(oneLineZmatrix_ZmatrixTestData) == 0
            @test Tropic.getInternalsSize(twoLineZmatrix_ZmatrixTestData) == 1
            @test Tropic.getInternalsSize(threeLineZmatrix_ZmatrixTestData) == 3
            @test Tropic.getInternalsSize(h2o2Zmatrix_ZmatrixTestData) == 6
        end

        @testset "Z-Matrix to Cartesians Tests" begin
            @test norm(Tropic.toCartesians(h2o2Zmatrix_ZmatrixTestData, h2o2InternalValueArray_ZmatrixTestData) - h2o2Cartesians_ZmatrixTestData) < 1e-7
        end

        @testset "Print Z-Matrix Tests" begin
            @test Tropic.printZmatrix(h2o2Zmatrix_ZmatrixTestData, h2o2Symbols_ZmatrixTestData, h2o2InternalValueArray_ZmatrixTestData) == h2o2PrintedZmatrix_ZmatrixTestData
            @test Tropic.printZmatrixFromCartesians(h2o2Zmatrix_ZmatrixTestData, h2o2Symbols_ZmatrixTestData, h2o2Cartesians_ZmatrixTestData) == h2o2PrintedZmatrix_ZmatrixTestData
        end

        @testset "B-Matrix for Z-Matrix Tests" begin
            @test norm(Tropic.getBmatrix(h2o2Zmatrix_ZmatrixTestData, h2o2Cartesians_ZmatrixTestData) - h2o2Bamtrix_ZmatrixTestData) < 1e-7
        end

        @testset "Derivatives of B-Matrix for Z-Matrix Tests" begin
            @test norm(Tropic.getDerivativeOfBmatrix(h2o2Zmatrix_ZmatrixTestData, h2o2Cartesians_ZmatrixTestData) - h2o2DerivativesOfBmarix_ZmatrixTestData) < 1e-7
        end

        @testset "Hessian Guess for Z-Matrix Tests" begin
            @test norm(Tropic.getHessianGuessLindh(h2o2Zmatrix_ZmatrixTestData, h2o2Symbols_ZmatrixTestData, h2o2Cartesians_ZmatrixTestData) - h2o2HessianGuess_ZmatrixTestData) < 1e-7
        end

        @testset "Hessian Guess Schlege for Z-Matrix Tests" begin
            @test norm(Tropic.getHessianGuessSchlegel(h2o2Zmatrix_ZmatrixTestData, h2o2Symbols_ZmatrixTestData, h2o2Cartesians_ZmatrixTestData) - h2o2HessianGuessSchlegel_ZmatrixTestData) < 1e-7
        end

        @testset "Remove Overwind in Z-Matrix Values Test" begin
            testData = deepcopy(h2o2InternalValueArray_ZmatrixTestData)
            
            testData[6] = 1.5π
            Tropic.removeOverwindStartingAtOffset!(h2o2Zmatrix_ZmatrixTestData, testData, 0)
            @test testData[6] == -0.5π
            
            testData[6] = -1.5π
            Tropic.removeOverwindStartingAtOffset!(h2o2Zmatrix_ZmatrixTestData, testData, 0)
            @test testData[6] == 0.5π
            
            testData[6] = π
            Tropic.removeOverwindStartingAtOffset!(h2o2Zmatrix_ZmatrixTestData, testData, 0)
            @test isapprox(testData[6], π)

            testData[6] = -π
            Tropic.removeOverwindStartingAtOffset!(h2o2Zmatrix_ZmatrixTestData, testData, 0)
            @test isapprox(testData[6], -π)
        end
    end
end