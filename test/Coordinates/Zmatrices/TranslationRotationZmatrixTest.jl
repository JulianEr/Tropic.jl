function translationRotationZmatrixTest()
    @testset "Translation Rotation Z-Matrix Tests" begin
        @testset "Internals Size for TR Z-Matrix Tests" begin
            @test Tropic.getInternalsSize(h2o2twiceZmatrix_ZmatrixTestData) == 24
        end
        
        @testset "Internal Value Array for TR Z-Matrix Tests" begin
            @test norm(Tropic.getInternalValueArray(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceCartesians_ZmatrixTestData) - h2o2twiceInternalValueArray_ZmatrixTestData) < 1e-7
            @test norm(Tropic.getInternalValueArray(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceRotatedCartesians_ZmatrixTestData) - h2o2twiceRotatedInternalValueArray_ZmatrixTestData) < 1e-7
        end

        @testset "To Cartesians for TR Z-Matrix Tests" begin
            @test norm(Tropic.toCartesians(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceInternalValueArray_ZmatrixTestData) - h2o2twiceCartesians_ZmatrixTestData) < 1e-7
            @test norm(Tropic.toCartesians(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceRotatedInternalValueArray_ZmatrixTestData) - h2o2twiceRotatedCartesians_ZmatrixTestData) < 1e-7
        end

        @testset "Print Zmatrix for TR Z-Matrix Tests" begin
            @test Tropic.printZmatrix(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceInternalValueArray_ZmatrixTestData) == h2o2twicePrinted_ZmatrixTestData
            @test Tropic.printZmatrixFromCartesians(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceCartesians_ZmatrixTestData) == h2o2twicePrinted_ZmatrixTestData
            @test Tropic.printZmatrix(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceRotatedInternalValueArray_ZmatrixTestData) == h2o2twiceRotatedPrinted_ZmatrixTestData
            @test Tropic.printZmatrixFromCartesians(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceRotatedCartesians_ZmatrixTestData) == h2o2twiceRotatedPrinted_ZmatrixTestData
        end

        @testset "B-Matrix for TR Z-Matrix Tests" begin
            @test norm(Tropic.getBmatrix(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceCartesians_ZmatrixTestData) - h2o2twiceBmatrix_ZmatrixTestData) < 1e-7
            @test norm(Tropic.getBmatrix(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceRotatedCartesians_ZmatrixTestData) - h2o2twiceRotatedBmatrix_ZmatrixTestData) < 1e-7
        end

        @testset "B-Matrix Derivatives for TR Z-Matrix Tests" begin
            @test norm(Tropic.getDerivativeOfBmatrix(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceCartesians_ZmatrixTestData) - h2o2twiceBmatrixDerivatives_ZmatrixTestData) < 1e-7
            @test norm(Tropic.getDerivativeOfBmatrix(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceRotatedCartesians_ZmatrixTestData) - h2o2twiceRotatedBmatricDerivatives_ZmatrixTestData) < 1e-7
        end

        @testset "Hessian Guess for TR Z-Matrix Tests" begin
            @test norm(Tropic.getHessianGuessLindh(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceCartesians_ZmatrixTestData) - h2o2twiceGuessHessianValues_ZmatrixTestData) < 1e-7
            @test norm(Tropic.getHessianGuessLindh(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceRotatedCartesians_ZmatrixTestData) - h2o2twiceRotatedGuessHessianValues_ZmatrixData) < 1e-7
        end

        @testset "Hessian Guess Schlegel for TR Z-Matrix Tests" begin
            @test norm(Tropic.getHessianGuessSchlegel(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceCartesians_ZmatrixTestData) - h2o2twiceGuessHessianSchlegelValues_ZmatrixTestData) < 1e-7
            @test norm(Tropic.getHessianGuessSchlegel(h2o2twiceZmatrix_ZmatrixTestData, h2o2twiceSymbols_ZmatrixTestData, h2o2twiceRotatedCartesians_ZmatrixTestData) - h2o2twiceRotatedGuessHessianSchlegelValues_ZmatrixTestData) < 1e-7
        end

        @testset "Remove Overwind for TR Z-Matrix Tests" begin
            testData = deepcopy(h2o2twiceInternalValueArray_ZmatrixTestData)

            testData[4] = 1.5π
            testData[5] = -1.5π
            testData[6] = π
            testData[16] = -π
            testData[17] = 0.5π
            testData[18] = -0.5π
            testData[12] = 1.5π
            testData[24] = -1.5π

            Tropic.removeOverwindStartingAtOffset!(h2o2twiceZmatrix_ZmatrixTestData, testData, 0)

            @test testData[4] == -0.5π
            @test testData[5] == 0.5π
            @test isapprox(testData[6], π)
            @test isapprox(testData[16], -π)
            @test testData[17] == 0.5π
            @test testData[18] == -0.5π
            @test testData[12] == -0.5π
            @test testData[24] == 0.5π
        end
    end
end