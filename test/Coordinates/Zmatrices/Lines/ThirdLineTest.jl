function thirdLineTest()
    @testset "Third Line Tests" begin
        thridLine = Tropic.ThirdLine(Tropic.Bond(1,2), Tropic.Angle(1,2,3))
        @testset "Internal Values for Third Line Test" begin
            @test norm(Tropic.getInternalValues(thridLine, h2o2Cartesians_LinesTestData) - expectedValuesForThridLine_LinesTestData) < 1e-7
        end

        @testset "B-Matrix Line for Third Line Test" begin
            @test norm(Tropic.getBmatrixLines(thridLine, h2o2Cartesians_LinesTestData) - expectedBmatrixLineForThirdLine_LinesTestData) < 1e-7
        end

        @testset "B-Matrix Derivatives for Third Line Test" begin
            @test norm(Tropic.getDerivativeOfBmatrixMatrices(thridLine, h2o2Cartesians_LinesTestData) - expectedBmatrixDerivativeForThirdLine_LinesTestData) < 1e-7
        end

        @testset "Hessian Guess for Third Line Test" begin
            @test norm(Tropic.getHessianGuessValues(thridLine, h2o2Symbols_LinesTestData, h2o2Cartesians_LinesTestData) - expectedHessianGuessForThirdLine_LinesTestData) < 1e-7
        end

        @testset "Schlegel Hessian Guess for Third Line Test" begin
            @test norm(Tropic.getHessianGuessSchlegelValues(thridLine, h2o2Symbols_LinesTestData, h2o2Cartesians_LinesTestData) - expectedSchlegelHessianGuessForThirdLine_LinesTestData) < 1e-7
        end

        @testset "Print Third Line Test" begin
            @test Tropic.printLine(thridLine, h2o2Symbols_LinesTestData, allInternalValues_LinesTestData) == "  O  1    1.0139820  1  108.8370503"
        end

        @testset "Add to Cartesians for Third Line Test" begin
            cartesians = zeros(12)
            cartesians[1:6] = h2o2Cartesians_LinesTestData[1:6]

            Tropic.addToCartesian!(thridLine, allInternalValues_LinesTestData, cartesians, 42)

            expectedCartesians = zeros(12)
            expectedCartesians[1:9] = h2o2Cartesians_LinesTestData[1:9]

            @test norm(cartesians - expectedCartesians) < 1e-7
        end
    end
end