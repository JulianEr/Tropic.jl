using Test, LinearAlgebra

import Tropic

include("LinesTestData.jl")

include("DefaultLineTest.jl")
include("FirstLineTest.jl")
include("RotationLineTest.jl")
include("SecondLineTest.jl")
include("ThirdLineTest.jl")
include("TranslationLineTest.jl")

defaultLineTest()
firstLineTest()
rotationLineTest()
secondLineTest()
thirdLineTest()
translationLineTest()