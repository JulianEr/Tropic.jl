function translationLineTest()
    @testset "Translation Line Tests" begin
        translationLine = Tropic.TranslationLine(
            Tropic.TranslationX([1, 2, 3, 4]),
            Tropic.TranslationY([1, 2, 3, 4]),
            Tropic.TranslationZ([1, 2, 3, 4])
        )
        @testset "Internal Values for Translation Line Test" begin
            x,y,z = Tropic.getInternalValues(translationLine, rotatedAndShiftedH2o2Cartesians_LinesTestData)
            expectedX, expectedY, expectedZ = expectedValuesForTranslationLine_LinesTestData
            @test x == expectedX
            @test y == expectedY
            @test z == expectedZ
        end

        @testset "B-Matrix Line for Translation Line Test" begin
            @test norm(Tropic.getBmatrixLines(translationLine, rotatedAndShiftedH2o2Cartesians_LinesTestData) - expectedBmatrixLineForTranslationLine_LinesTestData) < 1e-7
        end

        @testset "B-Matrix Derivatives for Translation Line Test" begin
            @test norm(Tropic.getDerivativeOfBmatrixMatrices(translationLine, rotatedAndShiftedH2o2Cartesians_LinesTestData) - expectedBmatrixDerviativeForTranslationLine_LinesTestData) < 1e-7
        end

        @testset "Hessian Guess for Translation Line Test" begin
            @test norm(Tropic.getHessianGuessValues(translationLine, h2o2Symbols_LinesTestData, rotatedAndShiftedH2o2Cartesians_LinesTestData) - expectedHessianGuessForTranlsationLine_LinesTestData) < 1e-7
        end

        @testset "Schlegel Hessian Guess for Translation Line Test" begin
            @test norm(Tropic.getHessianGuessSchlegelValues(translationLine, h2o2Symbols_LinesTestData, rotatedAndShiftedH2o2Cartesians_LinesTestData) - expectedSchlegelHessianGuessForTranlationLine_LinesTestData) < 1e-7
        end

        @testset "Print Translation Line Test" begin
            @test Tropic.printLine(translationLine, expectedValuesForTranslationLine_LinesTestData) == "Translation   -3.2226358   -0.7488606   -0.4690881"
        end
    end
end