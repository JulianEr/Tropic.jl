function firstLineTest()
    @testset "First Line Tests" begin
        firstLine = Tropic.FirstLine()
        @testset "Internal Values for First Line Test" begin
            @test isempty(Tropic.getInternalValues(firstLine, h2o2Cartesians_LinesTestData))
        end

        @testset "Print First Line Test" begin
            @test Tropic.printLine(firstLine, h2o2Symbols_LinesTestData, allInternalValues_LinesTestData) == "  H"
        end

        @testset "Add to Cartesian for First Line Test" begin
            cartesians = Array{Float64}(undef, 3)

            Tropic.addToCartesian!(firstLine, allInternalValues_LinesTestData, cartesians)

            @test norm(cartesians) < 1e-7
        end
    end
end