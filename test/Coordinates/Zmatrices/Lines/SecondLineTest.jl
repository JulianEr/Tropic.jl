function secondLineTest()
    @testset "Second Line Tests" begin
        secondLine = Tropic.SecondLine(Tropic.Bond(1,2))
        @testset "Internal Values for Second Line Test" begin
            @test Tropic.getInternalValues(secondLine, h2o2Cartesians_LinesTestData) == expectedValueForSecondLine_LinesTestData
        end

        @testset "B-Matrix Line for Second Line Test" begin
            @test norm(Tropic.getBmatrixLines(secondLine, h2o2Cartesians_LinesTestData) - expectedBmatrixLineForSecondLine_LinesTestData) < 1e-7
        end

        @testset "B-Matrix Derivatives for Second Line Test" begin
            @test norm(Tropic.getDerivativeOfBmatrixMatrices(secondLine, h2o2Cartesians_LinesTestData) - expectedBmatrixDerivativeForSecondLine_LinesTestData) < 1e-7
        end

        @testset "Hessian Guess for Second Line Test" begin
            @test Tropic.getHessianGuessValues(secondLine, h2o2Symbols_LinesTestData, h2o2Cartesians_LinesTestData) == expectedHessianGuessForSecondLine_LinesTestData
        end

        @testset "Schlegel Hessian Guess for Second Line Test" begin
            @test Tropic.getHessianGuessSchlegelValues(secondLine, h2o2Symbols_LinesTestData, h2o2Cartesians_LinesTestData) == expectedSchlegelHessianGuessForSecondLine_LinesTestData
        end

        @testset "Print Second Line Test" begin
            @test Tropic.printLine(secondLine, h2o2Symbols_LinesTestData, allInternalValues_LinesTestData) == "  O  1    1.0139820"
        end

        @testset "Add to Cartesians for Second Line Test" begin
            cartesians = zeros(12)
            cartesians[1:3] = h2o2Cartesians_LinesTestData[1:3]

            Tropic.addToCartesian!(secondLine, allInternalValues_LinesTestData, cartesians, 42)

            expectedCartesians = zeros(12)
            expectedCartesians[1:6] = h2o2Cartesians_LinesTestData[1:6]

            @test norm(cartesians - expectedCartesians) < 1e-7
        end
    end
end