function rotationConstraintTest() 
    @testset "RotationsConstraint Tests" begin
        @testset "Test equality" begin
            fakeCartesians = ones(Float64, 18)
            rotationsA = [Tropic.RotationA([1, 2, 3, 4, 5], fakeCartesians); Tropic.RotationA([2, 5, 3, 1, 4], fakeCartesians); Tropic.RotationA([1, 2, 3, 4, 5, 6], fakeCartesians)]
            rotationAConstraint = Tropic.RotationAConstraint([3, 5, 2, 1, 4])

            @test rotationsA[1] == rotationAConstraint
            @test rotationsA[2] == rotationAConstraint
            @test rotationsA[3] != rotationAConstraint
            @test rotationAConstraint == rotationsA[1]
            @test rotationAConstraint == rotationsA[2]
            @test rotationAConstraint != rotationsA[3]

            rotationsB = [Tropic.RotationB([1, 2, 3, 4, 5], fakeCartesians); Tropic.RotationB([2, 5, 3, 1, 4], fakeCartesians); Tropic.RotationB([1, 2, 3, 4, 5, 6], fakeCartesians)]
            rotationBConstraint = Tropic.RotationBConstraint([3, 5, 2, 1, 4])

            @test rotationsB[1] == rotationBConstraint
            @test rotationsB[2] == rotationBConstraint
            @test rotationsB[3] != rotationBConstraint
            @test rotationBConstraint == rotationsB[1]
            @test rotationBConstraint == rotationsB[2]
            @test rotationBConstraint != rotationsB[3]

            rotationsC = [Tropic.RotationC([1, 2, 3, 4, 5], fakeCartesians); Tropic.RotationC([2, 5, 3, 1, 4], fakeCartesians); Tropic.RotationC([1, 2, 3, 4, 5, 6], fakeCartesians)]
            rotationCConstraint = Tropic.RotationCConstraint([3, 5, 2, 1, 4])

            @test rotationsC[1] == rotationCConstraint
            @test rotationsC[2] == rotationCConstraint
            @test rotationsC[3] != rotationCConstraint
            @test rotationCConstraint == rotationsC[1]
            @test rotationCConstraint == rotationsC[2]
            @test rotationCConstraint != rotationsC[3]
        end
        
        @testset "Test Reorder" begin
            @test Tropic.reorderRotationAConstraint(Tropic.RotationAConstraint([3, 4, 5, 6, 7]), newOrder_ConstraintTestData).indices == [9, 4, 6, 7, 2]
            @test Tropic.reorderRotationBConstraint(Tropic.RotationBConstraint([3, 4, 5, 6, 7]), newOrder_ConstraintTestData).indices == [9, 4, 6, 7, 2]
            @test Tropic.reorderRotationCConstraint(Tropic.RotationCConstraint([3, 4, 5, 6, 7]), newOrder_ConstraintTestData).indices == [9, 4, 6, 7, 2]
        end
    end
end