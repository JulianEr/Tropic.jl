using Test

import Tropic

include("ConstraintTestData.jl")

include("BondConstraintTest.jl")
include("AngleConstraintTest.jl")
include("DihedralConstraintTest.jl")
include("TranslationConstraintsTest.jl")
include("RotationConstraintTest.jl")
include("SetOfConstraintsTest.jl")

bondConstraintTest()
angleConstraintTest()
dihedralConstraintTest()
rotationConstraintTest()
translationConstraintTest()
setOfConstraintsTest()