function setOfConstraintsTest()
    @testset "Set Of Constraints Tests" begin
        @testset "Add Rotation Tests" begin
            rotationA = Tropic.RotationAConstraint([1,2,3,4])
            rotationB = Tropic.RotationBConstraint([1,2,3,4])
            rotationC = Tropic.RotationCConstraint([1,2,3,4])
            noConstraints = Tropic.NoConstraints()

            isTRConstraint(::Tropic.SetOfConstraints) = false
            isTRConstraint(::Tropic.TrAndPrimitiveConstraints) = true

            constraints = Tropic.addRotation!(noConstraints, rotationA)
            @test isTRConstraint(constraints)
            @test length(constraints.rotationsA) == 1

            constraints = Tropic.addRotation!(noConstraints, rotationB)
            @test isTRConstraint(constraints)
            @test length(constraints.rotationsB) == 1

            constraints = Tropic.addRotation!(noConstraints, rotationC)
            @test isTRConstraint(constraints)
            @test length(constraints.rotationsC) == 1

            primitives = Tropic.PrimitiveConstraints([],[],[])

            constraints = Tropic.addRotation!(primitives, rotationA)
            @test isTRConstraint(constraints)
            @test length(constraints.rotationsA) == 1

            constraints = Tropic.addRotation!(primitives, rotationB)
            @test isTRConstraint(constraints)
            @test length(constraints.rotationsB) == 1

            constraints = Tropic.addRotation!(primitives, rotationC)
            @test isTRConstraint(constraints)
            @test length(constraints.rotationsC) == 1

            trPrimitives = Tropic.TrAndPrimitiveConstraints([],[],[], [rotationA], [rotationB], [rotationC], primitives)

            constraints = Tropic.addRotation!(trPrimitives, rotationA)
            @test isTRConstraint(constraints)
            @test length(constraints.rotationsA) == 2

            constraints = Tropic.addRotation!(trPrimitives, rotationB)
            @test isTRConstraint(constraints)
            @test length(constraints.rotationsB) == 2

            constraints = Tropic.addRotation!(trPrimitives, rotationC)
            @test isTRConstraint(constraints)
            @test length(constraints.rotationsC) == 2
        end

        @testset "Add Translation Tests" begin
            translationX = Tropic.TranslationXConstraint([1,2,3,4])
            translationY = Tropic.TranslationYConstraint([1,2,3,4])
            translationZ = Tropic.TranslationZConstraint([1,2,3,4])
            noConstraints = Tropic.NoConstraints()

            isTRConstraint(::Tropic.SetOfConstraints) = false
            isTRConstraint(::Tropic.TrAndPrimitiveConstraints) = true

            constraints = Tropic.addTranslation!(noConstraints, translationX)
            @test isTRConstraint(constraints)
            @test length(constraints.translationsX) == 1

            constraints = Tropic.addTranslation!(noConstraints, translationY)
            @test isTRConstraint(constraints)
            @test length(constraints.translationsY) == 1

            constraints = Tropic.addTranslation!(noConstraints, translationZ)
            @test isTRConstraint(constraints)
            @test length(constraints.translationsZ) == 1

            primitives = Tropic.PrimitiveConstraints([],[],[])

            constraints = Tropic.addTranslation!(primitives, translationX)
            @test isTRConstraint(constraints)
            @test length(constraints.translationsX) == 1

            constraints = Tropic.addTranslation!(primitives, translationY)
            @test isTRConstraint(constraints)
            @test length(constraints.translationsY) == 1

            constraints = Tropic.addTranslation!(primitives, translationZ)
            @test isTRConstraint(constraints)
            @test length(constraints.translationsZ) == 1

            trPrimitives = Tropic.TrAndPrimitiveConstraints([translationX],[translationY],[translationZ], [], [], [], primitives)

            constraints = Tropic.addTranslation!(trPrimitives, translationX)
            @test isTRConstraint(constraints)
            @test length(constraints.translationsX) == 2

            constraints = Tropic.addTranslation!(trPrimitives, translationY)
            @test isTRConstraint(constraints)
            @test length(constraints.translationsY) == 2

            constraints = Tropic.addTranslation!(trPrimitives, translationZ)
            @test isTRConstraint(constraints)
            @test length(constraints.translationsZ) == 2
        end

        @testset "Reorder Constraints" begin
            primitives = Tropic.PrimitiveConstraints([Tropic.BondConstraint(6, 7)],[Tropic.AngleConstraint(6, 7, 8)],[Tropic.DihedralConstraint(6, 7, 8, 9)])
            newOrderPrimitives = Tropic.reorderConstraints(primitives, newOrder_ConstraintTestData)

            @test newOrderPrimitives.bonds[1] == Tropic.BondConstraint(7, 2)
            @test newOrderPrimitives.angles[1] == Tropic.AngleConstraint(7, 2, 3)
            @test newOrderPrimitives.dihedrals[1] == Tropic.DihedralConstraint(7, 2, 3, 5)

            trPrimitives = Tropic.TrAndPrimitiveConstraints([],[Tropic.TranslationYConstraint([1, 2, 3, 4])],[], [], [], [Tropic.RotationCConstraint([5, 6, 7, 8])], primitives)
            newTrPrimitives = Tropic.reorderConstraints(trPrimitives, newOrder_ConstraintTestData)
            
            @test newTrPrimitives.translationsY[1].indices == [10, 1, 9, 4]
            @test newTrPrimitives.rotationsC[1].indices == [6, 7, 2, 3]

            @test newTrPrimitives.primitives.bonds[1] == Tropic.BondConstraint(7, 2)
            @test newTrPrimitives.primitives.angles[1] == Tropic.AngleConstraint(7, 2, 3)
            @test newTrPrimitives.primitives.dihedrals[1] == Tropic.DihedralConstraint(7, 2, 3, 5)
        end

        @testset "Has Constraints Test" begin
            noConstraints = Tropic.NoConstraints()
            primitiveConstraints = Tropic.PrimitiveConstraints([], [], [])
            trConstraints = Tropic.TrAndPrimitiveConstraints([], [], [], [], [], [], primitiveConstraints)

            @test Tropic.hasConstraints(noConstraints) == false
            @test Tropic.hasConstraints(primitiveConstraints) == true
            @test Tropic.hasConstraints(trConstraints) == true
        end
    end
end