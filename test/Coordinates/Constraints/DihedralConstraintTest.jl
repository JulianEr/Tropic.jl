function dihedralConstraintTest()
    @testset "DihedralConstraint Tests" begin
        @testset "Test equality" begin
            dihedrals = [Tropic.Dihedral(1, 2, 3, 4); Tropic.Dihedral(4, 3, 2, 1); Tropic.Dihedral(2, 3, 4, 5)]
            dihedralConstraint = Tropic.DihedralConstraint(1, 2, 3, 4)

            @test dihedrals[1] == dihedralConstraint
            @test dihedrals[2] == dihedralConstraint
            @test dihedrals[3] != dihedralConstraint
            @test dihedralConstraint == dihedrals[1]
            @test dihedralConstraint == dihedrals[2]
            @test dihedralConstraint != dihedrals[3]
        end
        
        @testset "Test Reorder" begin
            @test Tropic.reorderDihedralConstraint(Tropic.DihedralConstraint(3, 4, 5, 6), newOrder_ConstraintTestData) == Tropic.DihedralConstraint(9, 4, 6, 7)
        end
    end
end