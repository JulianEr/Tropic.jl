function translationRotationZmatrixFormat()

    @testset "Read In Translation Rotation Zmatrix" begin
        newZMatrix, cartesians, symbols = Tropic.readInZmatrix(tRZmatrixReadInTripleh2o2StringMoved_FormatsTestData)
        
        
        @test norm(cartesians - readInTrZmatTripleh2o2CartesiansMoved_FormatsTestData) < 1e-6
        @test symbols == readInTrZmatTripleh2o2Symbols_FormatsTestData
    end
end