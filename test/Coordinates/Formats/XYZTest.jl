function xyzFormatTests()
        @testset "XYZ Format Tests" begin
        symbols, cartesians = Tropic.readFromAngCartesiansContentToBohr(readInTrZmatTripleh2o2XyzFile_FormatsTestData)
        @test norm(cartesians - readInCartesianTripleh2o2XyzFile_FormatsTestData) < 1e-7
        xyzContent = Tropic.writeAngCartesiansFromBohr(symbols, cartesians)
        @test xyzContent == readInTrZmatTripleh2o2XyzFile_FormatsTestData

        tmpFileName = "$(@__DIR__())/cartesians.xyz"
        
        write(tmpFileName, xyzContent)
        try
            otherSymbols, otherCartesians = Tropic.readFromAngCartesiansToBohr(tmpFileName)
            @test norm(cartesians - otherCartesians) < 1e-7
            @test symbols == otherSymbols
        finally
            rm(tmpFileName)
        end
    end
end