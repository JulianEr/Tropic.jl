using Test, LinearAlgebra

import Tropic

include("SetsOfInternalCoordinatesTestData.jl")
include("PrimitivesTest.jl")
include("TranslationRotationAndPrimitivesTest.jl")
include("InternalCoordinatesWithHydrogenBondsTest.jl")
include("ZmatrixSetOfInternalsTest.jl")

primitivesTest()
translationRotationAndPrimitivesTest()
internalCoordinatesWithHydrogenTest()
zmatrixSetOfInternalsTest()