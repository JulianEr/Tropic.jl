function internalCoordinatesWithHydrogenTest()
    @testset "Testing Sets Of Internal Coordinates" begin
        
        @testset "Creating B-Matrices" begin
            @test norm(Tropic.getBmatrix(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedHydrogenBondTRBmatrix_SetsOfInternalCoordinatesTest) <= 1e-6

            emptyHydrogenCoordinates = Tropic.InternalCoordinatesWithHydrogenBonds([], translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest.internalCoordinates)
            @test norm(Tropic.getBmatrix(emptyHydrogenCoordinates, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedTRBmatrix_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Creating second Derivative B-Matrices" begin
            @test norm(Tropic.getDerivativeOfBmatrix(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedHydrogenBondTRBmatrixDerivative_SetsOfInternalCoordinatesTest) <= 1e-6

            emptyHydrogenCoordinates = Tropic.InternalCoordinatesWithHydrogenBonds([], translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest.internalCoordinates)
            @test norm(Tropic.getDerivativeOfBmatrix(emptyHydrogenCoordinates, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedTRBmatrixDerivatives_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Creating Hessian Guess" begin
            @test norm(Tropic.guessHessianLindh(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest, h2o2twiceSymbols_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedHydrogenBondTRhessianGuess_SetsOfInternalCoordinatesTest) <= 1e-6

            emptyHydrogenCoordinates = Tropic.InternalCoordinatesWithHydrogenBonds([], translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest.internalCoordinates)
            @test norm(Tropic.guessHessianLindh(emptyHydrogenCoordinates, h2o2twiceSymbols_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedTRhessianGuess_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Creating Hessian Guess Schlegel" begin
            @test norm(Tropic.guessHessianSchlegel(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest, h2o2twiceSymbols_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedHydrogenBondTRhessianGuessSchlegel_SetsOfInternalCoordinatesTest) <= 1e-6

            emptyHydrogenCoordinates = Tropic.InternalCoordinatesWithHydrogenBonds([], translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest.internalCoordinates)
            @test norm(Tropic.guessHessianSchlegel(emptyHydrogenCoordinates, h2o2twiceSymbols_SetsOfInternalCoordinatesTest, h2o2twiceCartesians_SetsOfInternalCoordinatesTest) - expectedTRhessianGuessSchlegel_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Remove Overwind" begin            
            trHydrogenBondsValues = [0; 0; 0; 0; 0; 0; 0; 0; 3.5π; -3.5π; -π; π; 0.5π; -0.5π; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; -1.5π; 1.5π]
            newTrHydrogenBondsValues = Tropic.removeOverwind(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest, trHydrogenBondsValues)
            @test norm(newTrHydrogenBondsValues - [0; 0; 0; 0; 0; 0; 0; 0; -0.5π; 0.5π; -π; π; 0.5π; -0.5π; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0.5π; -0.5π])  <= 1e-6

            emptyHydrogenCoordinates = Tropic.InternalCoordinatesWithHydrogenBonds([], translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest.internalCoordinates)
            newTrValuesEmptyHydrogenBonds = Tropic.removeOverwind(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest.internalCoordinates, trValuesOverwind_SetsOfInternalCoordinatesTest)
            @test norm(newTrValuesEmptyHydrogenBonds - [0; 0; 0; 0; 0; 0; -0.5π; 0.5π; -π; π; 0.5π; -0.5π; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0.5π; -0.5π])  <= 1e-6
        end

        @testset "Get all Values for internal cooridnate system" begin
            @test norm(Tropic.getValuesFromCartesians(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest, h2o2twiceRotatedCartesians_SetsOfInternalCoordinatesTest) - expectedHydrogenBondTrValues_SetsOfInternalCoordinatesTest) <= 1e-6
            
            emptyHydrogenCoordinates = Tropic.InternalCoordinatesWithHydrogenBonds([], translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest.internalCoordinates)
            @test norm(Tropic.getValuesFromCartesians(emptyHydrogenCoordinates, h2o2twiceRotatedCartesians_SetsOfInternalCoordinatesTest) - expectedTrValues_SetsOfInternalCoordinatesTest) <= 1e-6
        end

        @testset "Get Differences for internal coordinate system" begin
            trHydrogenBondsValuesBefore = [2.3; 3.1; 2.1; 2.1; 2.1; 2.1; 2.1; 2.1; 0.99π; -π; -0.99π; 0.99π; 0.5π;  -0.49π; 1.1; 1.1; 1.1; 1.1; 1.1; 1.1; 1.5; 1.5; 1.5; 1.5; -0.99π; 0.99π]
            trHydrogenBondsValuesAfter  = [2.5; 2.9; 2.2; 2.0; 2.2; 2.0; 2.2; 2.0; π;      π; 0.99π; -0.99π; 0.49π; -0.5π; 1.0; 1.0; 1.0; 1.0; 1.0; 1.0; 1.6; 1.6; 1.6; 1.6; 0.99π; -0.99π]
            @test norm(Tropic.getDifferences(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest, trHydrogenBondsValuesBefore, trHydrogenBondsValuesAfter) - [-0.2000000000;   0.2000000000;  -0.1000000000;   0.1000000000;  -0.1000000000;   0.1000000000;  -0.1000000000;   0.1000000000;  -0.0314159265;   0.0000000000;   0.0628318531;  -0.0628318531;   0.0314159265;   0.0314159265;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;  -0.1000000000;  -0.1000000000;  -0.1000000000;  -0.1000000000;   0.0628318531;  -0.0628318531]) <= 1e-6

            emptyHydrogenCoordinates = Tropic.InternalCoordinatesWithHydrogenBonds([], translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest.internalCoordinates)
            @test norm(Tropic.getDifferences(emptyHydrogenCoordinates, trValuesOverwindBefore_SetsOfInternalCoordinatesTest, trValuesOverwindAfter_SetsOfInternalCoordinatesTest) - [-0.1000000000;   0.1000000000;  -0.1000000000;   0.1000000000;  -0.1000000000;   0.1000000000;  -0.0314159265;   0.0000000000;   0.0628318531;  -0.0628318531;   0.0314159265;   0.0314159265;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;   0.1000000000;  -0.1000000000;  -0.1000000000;  -0.1000000000;  -0.1000000000;   0.0628318531;  -0.0628318531]) <= 1e-6
        end

        @testset "Add Constraints to the set of internal coordinates" begin
            internalCoordinatesCopy = deepcopy(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest)
            primitiveConstraints = Tropic.PrimitiveConstraints([Tropic.BondConstraint(3, 8) Tropic.BondConstraint(2, 3) Tropic.BondConstraint(1, 4)], [Tropic.AngleConstraint(4, 3, 2) Tropic.AngleConstraint(5, 7, 8)], [Tropic.DihedralConstraint(5, 6, 7, 8) Tropic.DihedralConstraint(3, 4, 5, 6)])
            constraints = Tropic.TrAndPrimitiveConstraints(
                [Tropic.TranslationXConstraint([1, 2, 3, 4])], 
                [Tropic.TranslationYConstraint([1, 2, 3, 4, 5, 6, 7, 8])],
                [],
                [Tropic.RotationAConstraint([5, 6, 7, 8])],
                [Tropic.RotationBConstraint([1, 2, 3, 4, 5, 6, 7, 8])],
                [],
                primitiveConstraints
            )
            _, listOfConstraints = Tropic.addAndFindConstraints!(internalCoordinatesCopy, constraints, h2o2twiceRotatedCartesians_SetsOfInternalCoordinatesTest)
            expectedListOfConstraints = [2, 3, 7, 11, 14, 18, 23, 25, 28, 30, 31]
            @test listOfConstraints == expectedListOfConstraints
            @test length(internalCoordinatesCopy.hydrogenBonds) == 2
            @test length(internalCoordinatesCopy.internalCoordinates.translationsX) == 2
            @test length(internalCoordinatesCopy.internalCoordinates.translationsY) == 3
            @test length(internalCoordinatesCopy.internalCoordinates.translationsZ) == 2
            @test length(internalCoordinatesCopy.internalCoordinates.rotationsA) == 2
            @test length(internalCoordinatesCopy.internalCoordinates.rotationsB) == 3
            @test length(internalCoordinatesCopy.internalCoordinates.rotationsC) == 2
            @test length(Tropic.getBonds(internalCoordinatesCopy)) == 7
            @test length(internalCoordinatesCopy.internalCoordinates.primitives.angles) == 5
            @test length(internalCoordinatesCopy.internalCoordinates.primitives.dihedrals) == 3

            expectedListOfConstraintsForPrimitives = [2, 16, 21, 23, 26, 28, 29]
            internalCoordinatesCopy = deepcopy(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest)
            _, listOfCinstraintsForPrimitives = Tropic.addAndFindConstraints!(internalCoordinatesCopy, primitiveConstraints, h2o2twiceRotatedCartesians_SetsOfInternalCoordinatesTest)
            @test expectedListOfConstraintsForPrimitives == listOfCinstraintsForPrimitives
        end

        @testset "Do Hydrogen Bond Internal Coordinates have Translation And Rotation Tests" begin
            @test Tropic.hasTranslationAndRotation(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest, false) == true
            @test Tropic.hasTranslationAndRotation(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest, true) == true

            hydrogenWithPrimitives = Tropic.InternalCoordinatesWithHydrogenBonds([], translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest.internalCoordinates.primitives)
            @test Tropic.hasTranslationAndRotation(hydrogenWithPrimitives, false) == false
            @test Tropic.hasTranslationAndRotation(hydrogenWithPrimitives, true) == true
        end

        @testset "toString" begin
            expectedOutput = """HydrogenBond(2, 8)   2.8643303894
                                HydrogenBond(3, 8)   3.8422314510
                                TranslationX(1, 2, 3, 4) -10.5555377053
                                TranslationX(5, 6, 7, 8)  -6.0898990000
                                TranslationY(1, 2, 3, 4)   0.3560716452
                                TranslationY(5, 6, 7, 8)  -1.4151415000
                                TranslationZ(1, 2, 3, 4)   0.5520362444
                                TranslationZ(5, 6, 7, 8)  -0.8864480000
                                RotationA(1, 2, 3, 4)   0.0000000000
                                RotationA(5, 6, 7, 8)  -0.4526489239
                                RotationB(1, 2, 3, 4)   0.0000000000
                                RotationB(5, 6, 7, 8)  -0.6230004263
                                RotationC(1, 2, 3, 4)   0.0000000000
                                RotationC(5, 6, 7, 8)   0.0356125220
                                Bond(1, 2)   1.9161483355
                                Bond(2, 3)   2.8502673624
                                Bond(3, 4)   1.9179248737
                                Bond(5, 6)   1.8391006826
                                Bond(6, 7)   2.7743353175
                                Bond(7, 8)   1.8719814430
                                Angle(1, 2, 3)   1.8995648767
                                Angle(2, 3, 4)   1.9089816985
                                Angle(5, 6, 7)   1.8524852504
                                Angle(6, 7, 8)   1.7314345129
                                Dihedral(1, 2, 3, 4)  -2.5730428532
                                Dihedral(5, 6, 7, 8)  -1.6225437438
                                """
            @test Tropic.toString(translatioonRotationAndPrimitivesWithHydrogenBonds_SetsOfInternalCoordinatesTest, expectedHydrogenBondTrValues_SetsOfInternalCoordinatesTest) == expectedOutput
        end
    end
end