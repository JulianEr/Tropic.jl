function zmatrixSetOfInternalsTest()
    @testset "ZmatrixSetOfInternals tests" begin

        @testset "Test for not implemented functions" begin
            @test_throws "'removeUndefinedDihedrals' seems not to be implemented for type" Tropic.removeUndefinedDihedrals(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, Float64[])
            @test_throws "'coordsForRedundantInternalCoordinates' seems not to be implemented for type" Tropic.coordsForRedundantInternalCoordinates(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, Float64[])
            @test_throws "'addAndFindConstraints!' seems not to be implemented for type" Tropic.addAndFindConstraints!(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, Tropic.PrimitiveConstraints([],[],[]), Float64[], Int64[], 0)
            @test_throws "'toStringWithOffset' seems not to be implemented for type" Tropic.toStringWithOffset(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, Float64[], 0)
        end

        @testset "values test" begin
            @test norm(Tropic.getValuesFromCartesians(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, zmatrixSetOfInternalsH2o2StartGeometry_SetsOfInternalCoordinatesTest) - h2o2ZmatrixSetOfInternalsInitialValueArray_SetsOfInternalCoordinatesTest) < 1e-7
            @test norm(Tropic.getValuesFromCartesians(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, trZmatTripleh2o2CartesianForSetOfInternals_SetsOfInternalCoordinatesTest) - tRZmatrixTripleh2o2ValuesForSetOfInternals_SetsOfInternalCoordinatesTest) < 1e-7
        end

        @testset "B-Matrix and their derivatives test" begin
            numB, numdB = Tropic.numericGradientsAndHessianMultiValues((xs) -> Tropic.getValuesFromCartesians(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, xs), zmatrixSetOfInternalsH2o2StartGeometry_SetsOfInternalCoordinatesTest)
            B = Tropic.getBmatrix(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, zmatrixSetOfInternalsH2o2StartGeometry_SetsOfInternalCoordinatesTest)

            @test norm(B - numB) < 1e-7
            @test norm(B - h2o2ZmatrixSetOfInternalsBmatrix_SetsOfInternalCoordinatesTest) < 1e-7

            BB = Tropic.getDerivativeOfBmatrix(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, zmatrixSetOfInternalsH2o2StartGeometry_SetsOfInternalCoordinatesTest)
            
            @test norm(BB - numdB) < 1e-5
            @test norm(BB - h2o2ZmatrixSetOfInternalsDerivativesOfBmatrix_SetsOfInternalCoordinatesTest) < 1e-7

            numB, numdB = Tropic.numericGradientsAndHessianMultiValues((xs) -> Tropic.getValuesFromCartesians(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, xs), trZmatTripleh2o2CartesianForSetOfInternals_SetsOfInternalCoordinatesTest, 7.5e-4)
            B = Tropic.getBmatrix(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, trZmatTripleh2o2CartesianForSetOfInternals_SetsOfInternalCoordinatesTest)

            @test norm(B - numB) < 1e-7
            @test norm(B - h2o2TrZmatrixSetOfInternalsBmatrix_SetsOfInternalCoordinatesTest) < 1e-7

            BB = Tropic.getDerivativeOfBmatrix(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, trZmatTripleh2o2CartesianForSetOfInternals_SetsOfInternalCoordinatesTest)

            @test norm(BB - numdB) < 1e-5
            @test norm(BB - h2o2TrZmatrixSetOfInternalsDerivativesOfBmatrix_SetsOfInternalCoordinatesTest) < 1e-7
        end

        @testset "Hessian guess Test" begin
            @test norm(Tropic.guessHessianLindh(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, zmatrixSetOfInternalsH2o2Symbols_SetsOfInternalCoordinatesTest, zmatrixSetOfInternalsH2o2StartGeometry_SetsOfInternalCoordinatesTest) - h2o2ZmatrixSetOfInternalsHessianGuess_SetsOfInternalCoordinatesTest) < 1e-7
            @test norm(Tropic.guessHessianSchlegel(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, zmatrixSetOfInternalsH2o2Symbols_SetsOfInternalCoordinatesTest, zmatrixSetOfInternalsH2o2StartGeometry_SetsOfInternalCoordinatesTest) - h2o2ZmatrixSetOfInternalsHessianGuessSchlegel_SetsOfInternalCoordinatesTest) < 1e-7
            @test norm(Tropic.guessHessianLindh(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, tRZmatrixSetOfInternalsH2o2Symbols_SetsOfInternalCoordinatesTest, trZmatTripleh2o2CartesianForSetOfInternalsOther_SetsOfInternalCoordinatesTest) - h2o2TrZmatrixSetOfInternalsHessianGuess_SetsOfInternalCoordinatesTest) < 1e-7
            @test norm(Tropic.guessHessianSchlegel(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, tRZmatrixSetOfInternalsH2o2Symbols_SetsOfInternalCoordinatesTest, trZmatTripleh2o2CartesianForSetOfInternalsOther_SetsOfInternalCoordinatesTest) - h2o2TrZmatrixSetOfInternalsHessianGuessSchlegel_SetsOfInternalCoordinatesTest) < 1e-7
        end

        @testset "Overwind and Differences Test" begin
            @test norm(Tropic.removeOverwind(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, [0.0, 0.0, 0.0, 0.0, 0.0, 2.75π]) - [0.0, 0.0, 0.0, 0.0, 0.0, 0.75π]) < 1e-7
            @test norm(Tropic.removeOverwind(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, [0.0, 0.0, 0.0, 0.0, 0.0, 0.75π]) - [0.0, 0.0, 0.0, 0.0, 0.0, 0.75π]) < 1e-7
            @test norm(Tropic.removeOverwind(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, [0.0, 0.0, 0.0, 0.0, 0.0, -1.2π]) - [0.0, 0.0, 0.0, 0.0, 0.0, 0.8π]) < 1e-7
            @test norm(Tropic.removeOverwind(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, h2o2TrZmatrixSetOfInternalsOverwindedValues_SetsOfInternalCoordinatesTest) - h2o2TrZmatrixSetOfInternalsNoOverwindedValues_SetsOfInternalCoordinatesTest) < 1e-7
            
            @test norm(Tropic.getDifferences(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, [0.0, 0.0, 0.0, 0.0, 0.0, 2.75π], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]) - [0.0, 0.0, 0.0, 0.0, 0.0, 0.75π]) < 1e-7
            @test norm(Tropic.getDifferences(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, [0.0, 0.0, 0.0, 0.0, 0.0, 0.75π], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]) - [0.0, 0.0, 0.0, 0.0, 0.0, 0.75π]) < 1e-7
            @test norm(Tropic.getDifferences(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, [0.0, 0.0, 0.0, 0.0, 0.0, -1.2π], [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]) - [0.0, 0.0, 0.0, 0.0, 0.0, 0.8π]) < 1e-7
            @test norm(Tropic.getDifferences(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, h2o2TrZmatrixSetOfInternalsOverwindedValues_SetsOfInternalCoordinatesTest, zeros(length(h2o2TrZmatrixSetOfInternalsOverwindedValues_SetsOfInternalCoordinatesTest))) - h2o2TrZmatrixSetOfInternalsNoOverwindedValues_SetsOfInternalCoordinatesTest) < 1e-7
        end

        @testset "Do Translation And Rotation Internal Coordinates have Translation And Rotation Tests" begin
            @test Tropic.hasTranslationAndRotation(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, false) == false
            @test Tropic.hasTranslationAndRotation(h2o2ZmatrixSetOfInternals_SetsOfInternalCoordinatesTest, true) == true
            @test Tropic.hasTranslationAndRotation(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, false) == true
            @test Tropic.hasTranslationAndRotation(tRZmatrixTripleh2o2SetOfInternals_SetsOfInternalCoordinatesTest, true) == true
        end
    end
end