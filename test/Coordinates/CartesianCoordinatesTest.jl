include("CartesianCoordinatesTestData.jl")

function cartesianCoordinatesTest()
    @testset "Cartesian Coordinates Tests" begin
        @testset "Cartesian Coordinates Construction Tests" begin
            coordinates = Tropic.CartesianCoordinates(h2o2Cartesians_CartesianCoordinatesTestData)
            @test size(coordinates.U) == (12,6)
            @test norm(coordinates.U - h2o2ProjectorOnlyTR_CartesianCoordinatesTestData) < 1e-7
            @test norm(coordinates.U'*coordinates.U - I) < 1e-7
            @test norm(coordinates.U*coordinates.U' - I) > 1e-7
            @test Tropic.getFullInternalsSize(coordinates.constraints) == 6
            
            constrainedCoordinates = Tropic.CartesianCoordinates(h2o2Cartesians_CartesianCoordinatesTestData, constraints=Tropic.TrAndPrimitiveConstraints([Tropic.TranslationXConstraint([1,2,3,4])], [], [], [Tropic.RotationAConstraint([1,2,3,4])], [], [], Tropic.PrimitiveConstraints([Tropic.BondConstraint(1,2)], [Tropic.AngleConstraint(1,2,3)], [Tropic.DihedralConstraint(1,2,3,4)])))

            bondDer = Tropic.derivativeVector(Tropic.Bond(1,2), h2o2Cartesians_CartesianCoordinatesTestData)
            angleDer = Tropic.derivativeVector(Tropic.Angle(1,2,3), h2o2Cartesians_CartesianCoordinatesTestData)
            dihedralDer = Tropic.derivativeVector(Tropic.Dihedral(1,2,3,4), h2o2Cartesians_CartesianCoordinatesTestData)
            translationDer = Tropic.derivativeVector(Tropic.TranslationX([1,2,3,4]), h2o2Cartesians_CartesianCoordinatesTestData)
            rotationDer = Tropic.derivativeVector(Tropic.RotationA([1,2,3,4], h2o2Cartesians_CartesianCoordinatesTestData), h2o2Cartesians_CartesianCoordinatesTestData)
            
            @test norm(constrainedCoordinates.U'*bondDer) < 1e-7
            @test norm(constrainedCoordinates.U'*angleDer) < 1e-7
            @test norm(constrainedCoordinates.U'*dihedralDer) < 1e-7
            @test norm(constrainedCoordinates.U'*translationDer) < 1e-7
            @test norm(constrainedCoordinates.U'*rotationDer) < 1e-7

            @test size(constrainedCoordinates.U) == (12,3)
            @test norm(constrainedCoordinates.U - h2o2ProjectorConstraintSystem_CartesianCoordinatesTestData) < 1e-7
            @test norm(constrainedCoordinates.U'*constrainedCoordinates.U - I) < 1e-7
            @test norm(constrainedCoordinates.U*constrainedCoordinates.U' - I) > 1e-7
            @test Tropic.getFullInternalsSize(constrainedCoordinates.constraints) == 9
        end

        @testset "Set and Apply New Cartesians Tests" begin
            coordinates = Tropic.CartesianCoordinates(h2o2Cartesians_CartesianCoordinatesTestData)
            internalsBefore = Tropic.getValuesFromCartesians(h2o2TranslationRotationPrimitives_CartesianCoordinatesTestData, coordinates.cartesians)
            
            Tropic.applyChange!(coordinates, h2o2ActiveChange_CartesianCoordinatesTestData)
            
            internalsAfter = Tropic.getValuesFromCartesians(h2o2TranslationRotationPrimitives_CartesianCoordinatesTestData, coordinates.cartesians)
            internalsChange = internalsBefore - internalsAfter
            @test norm(internalsChange[1:6]) < 1e-7
            @test norm(coordinates.cartesians - h2o2Change_CartesianCoordinatesTestData) < 1e-7

            constrainedCoordinates = Tropic.CartesianCoordinates(h2o2Cartesians_CartesianCoordinatesTestData, constraints=Tropic.PrimitiveConstraints([Tropic.BondConstraint(1,2)], [Tropic.AngleConstraint(1,2,3)], [Tropic.DihedralConstraint(1,2,3,4)]))
            internalsBefore = Tropic.getValuesFromCartesians(h2o2TranslationRotationPrimitives_CartesianCoordinatesTestData, constrainedCoordinates.cartesians)
            
            Tropic.applyChange!(constrainedCoordinates, h2o2ConstraintActiveChange_CartesianCoordinatesTestData)
            
            internalsAfter = Tropic.getValuesFromCartesians(h2o2TranslationRotationPrimitives_CartesianCoordinatesTestData, constrainedCoordinates.cartesians)
            internalsChange = internalsBefore - internalsAfter
            @test norm(internalsChange[1:6]) < 1e-7
            @test abs(internalsChange[7]) < 0.01
            @test abs(internalsChange[10]) < 0.01 
            @test abs(internalsChange[12]) < 0.01 
            @test norm(constrainedCoordinates.cartesians - h2o2ConstraintChange_CartesianCoordinatesTestData) < 1e-7
        end

        @testset "Cartesian Change and Values Tests" begin
            coordinates = Tropic.CartesianCoordinates(h2o2Cartesians_CartesianCoordinatesTestData)
            @test norm(Tropic.getChangeFromPrimitives(coordinates, h2o2Change_CartesianCoordinatesTestData) + h2o2ActiveChange_CartesianCoordinatesTestData) < 1e-7
    
            @test_throws ErrorException Tropic.resetCoordinateSystem!(coordinates)

            @test norm(Tropic.getCartesians(coordinates) - h2o2Cartesians_CartesianCoordinatesTestData) < 1e-7
            @test norm(Tropic.getPrimitiveValues(coordinates) - h2o2Cartesians_CartesianCoordinatesTestData) < 1e-7
            @test norm(Tropic.getProjectedValues(coordinates) - h2o2ProjectedValues_CartesianCoordinatesTestData) < 1e-7
        end

        @testset "Gradients and Hessian for Cartesian Coordinates Tests" begin
            coordinates = Tropic.CartesianCoordinates(h2o2Cartesians_CartesianCoordinatesTestData)
            @test norm(Tropic.transformCartesianGradients(coordinates, h2o2Gradients_CartesianCoordiantesTestData) - h2o2ExpectedGradients_CartesianCoordiantesTestData) < 1e-7
            g, H = Tropic.transformCartesianHessian(coordinates, h2o2Hessian_CartesianCoordinatesTestData, h2o2Gradients_CartesianCoordiantesTestData, Float64[])
            @test norm(g - h2o2ExpectedGradients_CartesianCoordiantesTestData) < 1e-7
            @test norm(H - h2o2ExpectedHessian_CartesianCoordiantesTestData) < 1e-7
            @test norm(Tropic.gradientsToActiveSpace(coordinates, h2o2ExpectedGradients_CartesianCoordiantesTestData) - h2o2ExpectedGradients_CartesianCoordiantesTestData) < 1e-7
            @test norm(Tropic.hessianToActiveSpace(coordinates, h2o2ExpectedHessian_CartesianCoordiantesTestData) - h2o2ExpectedHessian_CartesianCoordiantesTestData) < 1e-7
            @test norm(Tropic.weightsToHessian(coordinates, h2o2ExpectedHessian_CartesianCoordiantesTestData) - h2o2ExpectedHessian_CartesianCoordiantesTestData) < 1e-7
        end

        @testset "Hessian Guesses for Cartesian Coordinates Tests" begin
            coordinates = Tropic.CartesianCoordinates(h2o2Cartesians_CartesianCoordinatesTestData)
            optimizer = CartesianCoordinatesOptimizerDouble_CartesianCoordinatesTestData()
            
            @test norm(Tropic.guessHessianLindh(coordinates, h2o2Symbols_CartesianCoordinatesTestData, optimizer) - h2o2HessianGuess_CartesianCoordinatesTestData) < 1e-7
            @test norm(Tropic.guessHessianSchlegel(coordinates, h2o2Symbols_CartesianCoordinatesTestData, optimizer) - h2o2HessianGuessSchlegel_CartesianCoordinatesTestData) < 1e-7
            @test norm(Tropic.identityHessian(coordinates) - I) < 1e-7
            
            optimizer = CartesianCoordinatesOptimizerDoubleNanGradients_CartesianCoordinatesTestData()
            @test_throws "The guessed Hessian for Cartesian Coordinates contained NaN" Tropic.guessHessianSchlegel(coordinates, h2o2Symbols_CartesianCoordinatesTestData, optimizer)
            
            optimizer = CartesianCoordinatesOptimizerDoubleThrowsError_CartesianCoordinatesTestData()
            @test_throws "Something broke in the optimizers test double!" Tropic.guessHessianSchlegel(coordinates, h2o2Symbols_CartesianCoordinatesTestData, optimizer)
        end

        @testset "Hessian Guesses for Cartesian Coordinates Tests" begin
            coordinates = Tropic.CartesianCoordinates(h2o2Cartesians_CartesianCoordinatesTestData)
            
            @test_throws ErrorException Tropic.toString(coordinates) 
            @test_throws ErrorException Tropic.toString(coordinates, Float64[]) 
        end

        @testset "Acetylene test if 3N - 5 holds" begin
            coordinates = Tropic.CartesianCoordinates(acetyleneCartesians_CartesianCoordinatesTestData)

            @test size(coordinates.U) == (12, 7)
        end
    end
end