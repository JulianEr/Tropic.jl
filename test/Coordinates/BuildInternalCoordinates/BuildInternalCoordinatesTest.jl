using Test, LightGraphs, Logging
import Tropic

include("BuildInternalCoordinatesTestData.jl")

@testset "Build Internal Coordinates Tests" begin

    @testset "Find primitive Values" begin
        bonds = Tropic.findAllBonds(histidinGraph_BuildInternalCoordinatesTestData)

        @test bonds == histidinBonds_BuildInternalCoordinatesTestData

        angles = Tropic.findAllAngles(histidinGraph_BuildInternalCoordinatesTestData)

        @test angles == histidinAngles_BuildInternalCoordinatesTestData

        dihedrals = Tropic.findAllDihedrals(histidinGraph_BuildInternalCoordinatesTestData)

        @test dihedrals == histidinDihedrals_BuildInternalCoordinatesTestData

        translationX, translationY, translationZ, rotationA, rotationB, rotationC = Tropic.findTranslationsAndRotations(histidinGraph_BuildInternalCoordinatesTestData, histidinCartesians_BuildInternalCoordinatesTestData) 

        @test translationX == histidinTranslationX_BuildInternalCoordinatesTestData
        @test translationY == histidinTranslationY_BuildInternalCoordinatesTestData
        @test translationZ == histidinTranslationZ_BuildInternalCoordinatesTestData 

        @test rotationA == histidinRotationA_BuildInternalCoordinatesTestData
        @test rotationB == histidinRotationB_BuildInternalCoordinatesTestData
        @test rotationC == histidinRotationC_BuildInternalCoordinatesTestData
    end

    @testset "Finding all internal Coordinates" begin
        h2o2Primitives = Tropic.findAllInternalCoordinates(h2o2Graph_BuildInternalCoordinatesTestData, h2o2Symbols_BuildInternalCoordinatesTestData, h2o2Cartesians_BuildInternalCoordinatesTestData, withHydrogens=false)
        
        @test length(h2o2Primitives.bonds) == 3
        @test length(h2o2Primitives.angles) == 2
        @test length(h2o2Primitives.dihedrals) == 1

        h2o2PrimitivesWithHydrogenBonds = Tropic.findAllInternalCoordinates(h2o2Graph_BuildInternalCoordinatesTestData, h2o2Symbols_BuildInternalCoordinatesTestData, h2o2Cartesians_BuildInternalCoordinatesTestData)
        
        @test length(h2o2PrimitivesWithHydrogenBonds.hydrogenBonds) == 0
        @test length(h2o2PrimitivesWithHydrogenBonds.internalCoordinates.bonds) == 3
        @test length(h2o2PrimitivesWithHydrogenBonds.internalCoordinates.angles) == 2
        @test length(h2o2PrimitivesWithHydrogenBonds.internalCoordinates.dihedrals) == 1

        h2o2TwiceTrPrimitives = Tropic.findAllInternalCoordinates(h2o2TwiceGraph_BuildInternalCoordinatesTestData, h2o2TwiceSymbols_BuildInternalCoordinatesTestData, h2o2TwiceCartesians_BuildInternalCoordinatesTestData, withHydrogens=false)
        
        @test length(h2o2TwiceTrPrimitives.translationsX) == 2
        @test length(h2o2TwiceTrPrimitives.translationsY) == 2
        @test length(h2o2TwiceTrPrimitives.translationsZ) == 2
        @test length(h2o2TwiceTrPrimitives.rotationsA) == 2
        @test length(h2o2TwiceTrPrimitives.rotationsB) == 2
        @test length(h2o2TwiceTrPrimitives.rotationsC) == 2
        @test length(h2o2TwiceTrPrimitives.primitives.bonds) == 6
        @test length(h2o2TwiceTrPrimitives.primitives.angles) == 4
        @test length(h2o2TwiceTrPrimitives.primitives.dihedrals) == 2

        h2o2TwiceTrPrimitivesWithHydrogenBonds = Tropic.findAllInternalCoordinates(h2o2TwiceGraph_BuildInternalCoordinatesTestData, h2o2TwiceSymbols_BuildInternalCoordinatesTestData, h2o2TwiceCartesians_BuildInternalCoordinatesTestData)
        
        @test length(h2o2TwiceTrPrimitivesWithHydrogenBonds.hydrogenBonds) == 2
        @test length(h2o2TwiceTrPrimitivesWithHydrogenBonds.internalCoordinates.translationsX) == 2
        @test length(h2o2TwiceTrPrimitivesWithHydrogenBonds.internalCoordinates.translationsY) == 2
        @test length(h2o2TwiceTrPrimitivesWithHydrogenBonds.internalCoordinates.translationsZ) == 2
        @test length(h2o2TwiceTrPrimitivesWithHydrogenBonds.internalCoordinates.rotationsA) == 2
        @test length(h2o2TwiceTrPrimitivesWithHydrogenBonds.internalCoordinates.rotationsB) == 2
        @test length(h2o2TwiceTrPrimitivesWithHydrogenBonds.internalCoordinates.rotationsC) == 2
        @test length(h2o2TwiceTrPrimitivesWithHydrogenBonds.internalCoordinates.primitives.bonds) == 6
        @test length(h2o2TwiceTrPrimitivesWithHydrogenBonds.internalCoordinates.primitives.angles) == 4
        @test length(h2o2TwiceTrPrimitivesWithHydrogenBonds.internalCoordinates.primitives.dihedrals) == 2

        primitives = Tropic.addHydrogenBondsToInternalCoordinates(histidinPrimitives_BuildInternalCoordinatesTestData, histidinSymbols_BuildInternalCoordinatesTestData, histidinCartesians_BuildInternalCoordinatesTestData)
        @test length(primitives.hydrogenBonds) == 2
        @test findfirst(x -> Tropic.getA(x) == 2 && Tropic.getB(x) == 18, primitives.hydrogenBonds) !== nothing && 
            findfirst(x -> Tropic.getA(x) == 5 && Tropic.getB(x) == 20, primitives.hydrogenBonds) !== nothing
    end
end