const histidinSymbols_BuildInternalCoordinatesTestData = [
    "O";
    "O";
    "N";
    "N";
    "N";
    "C";
    "C";
    "C";
    "C";
    "C";
    "C";
    "H";
    "H";
    "H";
    "H";
    "H";
    "H";
    "H";
    "H";
    "H"
]

const histidinCartesians_BuildInternalCoordinatesTestData = [
     1.479837;    1.615729;    6.108332;
     1.242444;   -2.674654;    6.239409;
    -1.627140;   -0.170632;   -6.389812;
    -1.555259;    2.756916;   -2.988587;
    -0.065190;   -3.436991;    1.781523;
     0.003131;   -0.963827;   -4.492050;
     0.043941;    0.765270;   -2.478602;
    -2.443061;    2.042558;   -5.316264;
     1.617129;    0.550123;   -0.071005;
     0.259153;   -0.680702;    2.222723;
     1.063973;   -0.402392;    5.033607;
     1.090610;   -2.689667;   -4.552950;
    -1.916535;    4.354073;   -1.953960;
    -3.779176;    3.217255;   -6.328177;
     2.244172;    2.476313;    0.444796;
     3.399350;   -0.468043;   -0.475371;
    -1.637986;    0.185168;    2.401944;
    -1.092970;   -4.242594;    3.216437;
    -1.099194;   -3.740011;    0.168105;
     0.713935;   -3.955372;    5.072669
]

const histidinGraph_BuildInternalCoordinatesTestData = begin
    graph = SimpleGraph(20)
    add_edge!(graph, 1, 11)
    add_edge!(graph, 2, 11)
    add_edge!(graph, 2, 20)
    add_edge!(graph, 3, 6)
    add_edge!(graph, 3, 8)
    add_edge!(graph, 4, 7)
    add_edge!(graph, 4, 8)
    add_edge!(graph, 4, 13)
    add_edge!(graph, 5, 10)
    add_edge!(graph, 5, 18)
    add_edge!(graph, 5, 19)
    add_edge!(graph, 6, 7)
    add_edge!(graph, 6, 12)
    add_edge!(graph, 7, 9)
    add_edge!(graph, 8, 14)
    add_edge!(graph, 9, 10)
    add_edge!(graph, 9, 15)
    add_edge!(graph, 9, 16)
    add_edge!(graph, 10, 11)
    add_edge!(graph, 10, 17)
    graph
end

const histidinPrimitives_BuildInternalCoordinatesTestData = Tropic.findAllPrimitives(histidinGraph_BuildInternalCoordinatesTestData)

const histidinBonds_BuildInternalCoordinatesTestData = Tropic.Bond[Tropic.Bond(1, 11), Tropic.Bond(2, 11), Tropic.Bond(2, 20), Tropic.Bond(3, 6), Tropic.Bond(3, 8), Tropic.Bond(4, 7), Tropic.Bond(4, 8), Tropic.Bond(4, 13), Tropic.Bond(5, 10), Tropic.Bond(5, 18), Tropic.Bond(5, 19), Tropic.Bond(6, 7), Tropic.Bond(6, 12), Tropic.Bond(7, 9), Tropic.Bond(8, 14), Tropic.Bond(9, 10), Tropic.Bond(9, 15), Tropic.Bond(9, 16), Tropic.Bond(10, 11), Tropic.Bond(10, 17)]

const histidinAngles_BuildInternalCoordinatesTestData = Tropic.Angle[Tropic.Angle(11, 2, 20), Tropic.Angle(6, 3, 8), Tropic.Angle(7, 4, 8), Tropic.Angle(7, 4, 13), Tropic.Angle(8, 4, 13), Tropic.Angle(10, 5, 18), Tropic.Angle(10, 5, 19), Tropic.Angle(18, 5, 19), Tropic.Angle(3, 6, 7), Tropic.Angle(3, 6, 12), Tropic.Angle(7, 6, 12), Tropic.Angle(4, 7, 6), Tropic.Angle(4, 7, 9), Tropic.Angle(6, 7, 9), Tropic.Angle(3, 8, 4), Tropic.Angle(3, 8, 14), Tropic.Angle(4, 8, 14), Tropic.Angle(7, 9, 10), Tropic.Angle(7, 9, 15), Tropic.Angle(7, 9, 16), Tropic.Angle(10, 9, 15), Tropic.Angle(10, 9, 16), Tropic.Angle(15, 9, 16), Tropic.Angle(5, 10, 9), Tropic.Angle(5, 10, 11), Tropic.Angle(5, 10, 17), Tropic.Angle(9, 10, 11), Tropic.Angle(9, 10, 17), Tropic.Angle(11, 10, 17), Tropic.Angle(1, 11, 2), Tropic.Angle(1, 11, 10), Tropic.Angle(2, 11, 10)]

const histidinDihedrals_BuildInternalCoordinatesTestData = Tropic.Dihedral[Tropic.Dihedral(20, 2, 11, 1), Tropic.Dihedral(20, 2, 11, 10), Tropic.Dihedral(8, 3, 6, 7), Tropic.Dihedral(8, 3, 6, 12), Tropic.Dihedral(6, 3, 8, 4), Tropic.Dihedral(6, 3, 8, 14), Tropic.Dihedral(8, 4, 7, 6), Tropic.Dihedral(8, 4, 7, 9), Tropic.Dihedral(13, 4, 7, 6), Tropic.Dihedral(13, 4, 7, 9), Tropic.Dihedral(7, 4, 8, 3), Tropic.Dihedral(7, 4, 8, 14), Tropic.Dihedral(13, 4, 8, 3), Tropic.Dihedral(13, 4, 8, 14), Tropic.Dihedral(18, 5, 10, 9), Tropic.Dihedral(18, 5, 10, 11), Tropic.Dihedral(18, 5, 10, 17), Tropic.Dihedral(19, 5, 10, 9), Tropic.Dihedral(19, 5, 10, 11), Tropic.Dihedral(19, 5, 10, 17), Tropic.Dihedral(3, 6, 7, 4), Tropic.Dihedral(3, 6, 7, 9), Tropic.Dihedral(12, 6, 7, 4), Tropic.Dihedral(12, 6, 7, 9), Tropic.Dihedral(4, 7, 9, 10), Tropic.Dihedral(4, 7, 9, 15), Tropic.Dihedral(4, 7, 9, 16), Tropic.Dihedral(6, 7, 9, 10), Tropic.Dihedral(6, 7, 9, 15), Tropic.Dihedral(6, 7, 9, 16), Tropic.Dihedral(7, 9, 10, 5), Tropic.Dihedral(7, 9, 10, 11), Tropic.Dihedral(7, 9, 10, 17), Tropic.Dihedral(15, 9, 10, 5), Tropic.Dihedral(15, 9, 10, 11), Tropic.Dihedral(15, 9, 10, 17), Tropic.Dihedral(16, 9, 10, 5), Tropic.Dihedral(16, 9, 10, 11), Tropic.Dihedral(16, 9, 10, 17), Tropic.Dihedral(5, 10, 11, 1), Tropic.Dihedral(5, 10, 11, 2), Tropic.Dihedral(9, 10, 11, 1), Tropic.Dihedral(9, 10, 11, 2), Tropic.Dihedral(17, 10, 11, 1), Tropic.Dihedral(17, 10, 11, 2)]

const histidinTranslationX_BuildInternalCoordinatesTestData = Tropic.TranslationX[Tropic.TranslationX(Tropic.Translation([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]))]

const histidinTranslationY_BuildInternalCoordinatesTestData = Tropic.TranslationY[Tropic.TranslationY(Tropic.Translation([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]))]

const histidinTranslationZ_BuildInternalCoordinatesTestData = Tropic.TranslationZ[Tropic.TranslationZ(Tropic.Translation([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]))]

const histidinRotationA_BuildInternalCoordinatesTestData = Tropic.RotationA[Tropic.RotationA(Tropic.Rotation([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], Array{Float64, 2}(undef, 0, 0), 0.0))]

const histidinRotationB_BuildInternalCoordinatesTestData = Tropic.RotationB[Tropic.RotationB(Tropic.Rotation([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], Array{Float64, 2}(undef, 0, 0), 0.0))]

const histidinRotationC_BuildInternalCoordinatesTestData = Tropic.RotationC[Tropic.RotationC(Tropic.Rotation([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20], Array{Float64, 2}(undef, 0, 0), 0.0))]

const reorderSequenceForHistidin_BuildInternalCoordinatesTestData = [14, 8, 4, 7, 9, 10, 11, 2, 20, 3, 6, 5, 18, 13, 15, 16, 17, 1, 12, 19]

const reorderedPrimitives_BuildInternalCoordinatesTestData = Tropic.Primitives(Tropic.Bond[Tropic.Bond(14, 6), Tropic.Bond(8, 6), Tropic.Bond(8, 19), Tropic.Bond(4, 10), Tropic.Bond(4, 2), Tropic.Bond(7, 11), Tropic.Bond(7, 2), Tropic.Bond(7, 18), Tropic.Bond(9, 3), Tropic.Bond(9, 1), Tropic.Bond(9, 12), Tropic.Bond(10, 11), Tropic.Bond(10, 5), Tropic.Bond(11, 20), Tropic.Bond(2, 13), Tropic.Bond(20, 3), Tropic.Bond(20, 15), Tropic.Bond(20, 16), Tropic.Bond(3, 6), Tropic.Bond(3, 17)], Tropic.Angle[Tropic.Angle(6, 8, 19), Tropic.Angle(10, 4, 2), Tropic.Angle(11, 7, 2), Tropic.Angle(11, 7, 18), Tropic.Angle(2, 7, 18), Tropic.Angle(3, 9, 1), Tropic.Angle(3, 9, 12), Tropic.Angle(1, 9, 12), Tropic.Angle(4, 10, 11), Tropic.Angle(4, 10, 5), Tropic.Angle(11, 10, 5), Tropic.Angle(7, 11, 10), Tropic.Angle(7, 11, 20), Tropic.Angle(10, 11, 20), Tropic.Angle(4, 2, 7), Tropic.Angle(4, 2, 13), Tropic.Angle(7, 2, 13), Tropic.Angle(11, 20, 3), Tropic.Angle(11, 20, 15), Tropic.Angle(11, 20, 16), Tropic.Angle(3, 20, 15), Tropic.Angle(3, 20, 16), Tropic.Angle(15, 20, 16), Tropic.Angle(9, 3, 20), Tropic.Angle(9, 3, 6), Tropic.Angle(9, 3, 17), Tropic.Angle(20, 3, 6), Tropic.Angle(20, 3, 17), Tropic.Angle(6, 3, 17), Tropic.Angle(14, 6, 8), Tropic.Angle(14, 6, 3), Tropic.Angle(8, 6, 3)], Tropic.Dihedral[Tropic.Dihedral(19, 8, 6, 14), Tropic.Dihedral(19, 8, 6, 3), Tropic.Dihedral(2, 4, 10, 11), Tropic.Dihedral(2, 4, 10, 5), Tropic.Dihedral(10, 4, 2, 7), Tropic.Dihedral(10, 4, 2, 13), Tropic.Dihedral(2, 7, 11, 10), Tropic.Dihedral(2, 7, 11, 20), Tropic.Dihedral(18, 7, 11, 10), Tropic.Dihedral(18, 7, 11, 20), Tropic.Dihedral(11, 7, 2, 4), Tropic.Dihedral(11, 7, 2, 13), Tropic.Dihedral(18, 7, 2, 4), Tropic.Dihedral(18, 7, 2, 13), Tropic.Dihedral(1, 9, 3, 20), Tropic.Dihedral(1, 9, 3, 6), Tropic.Dihedral(1, 9, 3, 17), Tropic.Dihedral(12, 9, 3, 20), Tropic.Dihedral(12, 9, 3, 6), Tropic.Dihedral(12, 9, 3, 17), Tropic.Dihedral(4, 10, 11, 7), Tropic.Dihedral(4, 10, 11, 20), Tropic.Dihedral(5, 10, 11, 7), Tropic.Dihedral(5, 10, 11, 20), Tropic.Dihedral(7, 11, 20, 3), Tropic.Dihedral(7, 11, 20, 15), Tropic.Dihedral(7, 11, 20, 16), Tropic.Dihedral(10, 11, 20, 3), Tropic.Dihedral(10, 11, 20, 15), Tropic.Dihedral(10, 11, 20, 16), Tropic.Dihedral(11, 20, 3, 9), Tropic.Dihedral(11, 20, 3, 6), Tropic.Dihedral(11, 20, 3, 17), Tropic.Dihedral(15, 20, 3, 9), Tropic.Dihedral(15, 20, 3, 6), Tropic.Dihedral(15, 20, 3, 17), Tropic.Dihedral(16, 20, 3, 9), Tropic.Dihedral(16, 20, 3, 6), Tropic.Dihedral(16, 20, 3, 17), Tropic.Dihedral(9, 3, 6, 14), Tropic.Dihedral(9, 3, 6, 8), Tropic.Dihedral(20, 3, 6, 14), Tropic.Dihedral(20, 3, 6, 8), Tropic.Dihedral(17, 3, 6, 14), Tropic.Dihedral(17, 3, 6, 8)])

function h2o2BuildGraph_BuildInternalCoordinatesTestData()
    graph = SimpleGraph(4)
    add_edge!(graph, 1, 2)
    add_edge!(graph, 2, 3)
    add_edge!(graph, 3, 4)
    graph
end

const h2o2Graph_BuildInternalCoordinatesTestData = h2o2BuildGraph_BuildInternalCoordinatesTestData()

const h2o2Cartesians_BuildInternalCoordinatesTestData = [
    -3.8242387600890067; -1.2022437610162702;  -1.3957517162631519;
    -5.080717660905917;  -2.4821552657888573;  -0.9890826540646273;
    -7.169998865212092;  -1.4894821320858602;  -2.520894651360743;
    -8.108625831726995;  -0.5703193446631726;  -1.1873149246251533
]

const h2o2Symbols_BuildInternalCoordinatesTestData = [
    "H";
    "O";
    "O";
    "H"
]

function h2o2TwiceBuildGraph_BuildInternalCoordinatesTestData()
    graph = SimpleGraph(8)
    add_edge!(graph, 1, 2)
    add_edge!(graph, 2, 3)
    add_edge!(graph, 3, 4)
    add_edge!(graph, 5, 6)
    add_edge!(graph, 6, 7)
    add_edge!(graph, 7, 8)
    graph
end

const h2o2TwiceGraph_BuildInternalCoordinatesTestData = h2o2TwiceBuildGraph_BuildInternalCoordinatesTestData()

const h2o2TwiceSymbols_BuildInternalCoordinatesTestData = [
    "H";
    "O";
    "O";
    "H";
    "H";
    "O";
    "O";
    "H"
]  

const h2o2TwiceCartesians_BuildInternalCoordinatesTestData = [
    -8.58691551408037;    0.39211817103249935;  2.3986293710436213;
    -9.715082010978694;   1.3558784950159917;   1.1861810889498787;  
    -11.734254376030384; -0.4040234456228836;   0.21164932605127676;  
    -12.185898920014806;  0.0803133603319577;  -1.5883148084473047;
    -3.8242387600890067; -1.2022437610162702;  -1.3957517162631519;
    -5.080717660905917;  -2.4821552657888573;  -0.9890826540646273;
    -7.169998865212092;  -1.4894821320858602;  -2.520894651360743;
    -8.108625831726995;  -0.5703193446631726;  -1.1873149246251533
]