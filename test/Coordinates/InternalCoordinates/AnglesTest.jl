function anglesTest()
    @testset "Angle Tests" begin
        @testset "Angle equality" begin
            @test Tropic.Angle(1, 2, 3) == Tropic.Angle(1, 2, 3)
            @test Tropic.Angle(1, 2, 3) == Tropic.Angle(3, 2, 1)
        end

        @testset "Angle value" begin
            expectedValue = 1.8995648767
            @test isapprox(Tropic.value(Tropic.Angle(1, 2, 3), h2o2Cartesians_InternalCoordinatesData), expectedValue)
        end

        @testset "Angle derivative" begin
            @test norm(Tropic.derivativeVector(Tropic.Angle(1, 2, 3), h2o2Cartesians_InternalCoordinatesData) - expectedDerivativeVectorForAngle_InternalCoordinatesData) <= 1e-6
            @test norm(Tropic.derivativeVector(Tropic.Angle(1, 2, 3), linearAngleMolecule_InternalCoordinatesData) - expetcedDerivativeVectorForAngleForLinearMolecule_InternalCoordinatesData) <= 1e-6
            @test norm(Tropic.derivativeVector(Tropic.Angle(1, 2, 3), anotherLinearAngleMolecule_InternalCoordinatesData) - expectedDerivativeVectorForAngleForOtherLinearMolecule_InternalCoordinatesData) <= 1e-6
        end

        @testset "Angle second derivatives" begin
            @test norm(Tropic.secondDerivativeMatrix(Tropic.Angle(1, 2, 3), h2o2Cartesians_InternalCoordinatesData) - expectedSecondDerivativeMatrixForAngle_InternalCoordinatesData) <= 1e-6
            @test norm(Tropic.secondDerivativeMatrix(Tropic.Angle(1, 2, 3), linearAngleMolecule_InternalCoordinatesData)) == 0.0
        end

        @testset "toString" begin
            @test Tropic.toString(Tropic.Angle(1, 2, 3)) == "Angle(1, 2, 3)"
        end

        @testset "Numeric derivatives" begin
            g, H = Tropic.numericGradientsAndHessian(Tropic.Angle(1, 2, 3), h2o2Cartesians_InternalCoordinatesData)
            @test norm(g - expectedDerivativeVectorForAngle_InternalCoordinatesData) <= 1e-6
            @test norm(H - expectedSecondDerivativeMatrixForAngle_InternalCoordinatesData) <= 1e-5
        end

        @testset "Schlegel Hessian Guess Tests" begin
            a = Tropic.Angle(1, 2, 3)
            @test Tropic.getHessianGuessSchlegel(a, ["H", "H", "H"], zeros(9)) == 0.16
            @test Tropic.getHessianGuessSchlegel(a, ["H", "H", "C"], zeros(9)) == 0.16
            @test Tropic.getHessianGuessSchlegel(a, ["H", "C", "H"], zeros(9)) == 0.16
            @test Tropic.getHessianGuessSchlegel(a, ["C", "H", "H"], zeros(9)) == 0.16
            @test Tropic.getHessianGuessSchlegel(a, ["H", "C", "C"], zeros(9)) == 0.16
            @test Tropic.getHessianGuessSchlegel(a, ["C", "H", "C"], zeros(9)) == 0.16
            @test Tropic.getHessianGuessSchlegel(a, ["C", "C", "H"], zeros(9)) == 0.16
            @test Tropic.getHessianGuessSchlegel(a, ["C", "C", "C"], zeros(9)) == 0.25
        end

        @testset "Hessian Guess Tests" begin
            a = Tropic.Angle(1, 2, 3)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["H", "H", "C"], h2o2Cartesians_InternalCoordinatesData), 0.005445789592049273)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["H", "C", "C"], h2o2Cartesians_InternalCoordinatesData), 0.20722912618057385)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["C", "Si", "H"], h2o2Cartesians_InternalCoordinatesData), 0.6915384598408386)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["H", "Si", "C"], h2o2Cartesians_InternalCoordinatesData), 1.153434717510117)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["Si", "Si", "Si"], h2o2Cartesians_InternalCoordinatesData), 3.5740465470839475)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["Bi", "Si", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.2)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["Si", "Bi", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.2)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["Si", "Si", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.2)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["Bi", "Bi", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.2)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["Bi", "Si", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.2)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["Si", "Bi", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.2)
            @test isapprox(Tropic.getHessianGuessLindh(a, ["Bi", "Bi", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.2)
        end
    end
end