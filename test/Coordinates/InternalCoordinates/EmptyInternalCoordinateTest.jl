function emptyInternalCoodinateTest()
    @testset "Empty Internal Coordinate Tests" begin
        @testset "Empty Internal Coordinate equality" begin
            b = Tropic.Bond(1,2)
            e1 = Tropic.EmptyInternalCoordinate()
            e2 = Tropic.EmptyInternalCoordinate()

            @test e1 == e2
            @test !(e1 == b)
            @test !(b == e1)
        end
    end
end