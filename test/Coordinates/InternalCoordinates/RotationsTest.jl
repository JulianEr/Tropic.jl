function rotationsTest()
    @testset "Rotation A Tests" begin
        rotationA = Tropic.RotationA([1, 2, 3, 4], h2o2Cartesians_InternalCoordinatesData)
        @testset "Rotation equality" begin
            otherRotationA = Tropic.RotationA([1, 2, 3], h2o2Cartesians_InternalCoordinatesData)
            sameRotationA = Tropic.RotationA([1, 2, 3, 4], h2o2Cartesians_InternalCoordinatesData)
            @test rotationA != otherRotationA
            @test rotationA == sameRotationA
        end

        @testset "Rotation value" begin
            expectedValue = -5.768924382657089
            @test isapprox(Tropic.value(rotationA, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData), expectedValue)
        end

        @testset "Rotation derivative" begin
            @test norm(Tropic.derivativeVector(rotationA, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData) - expectedDerivativeVectorForRotationA_InternalCoordinatesData) <= 1e-6
        end

        @testset "Rotation second derivatives" begin
        @test norm(Tropic.secondDerivativeMatrix(rotationA, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData) - expectedSecondDerivativeMatrixForRotationA_InternalCoordinatesData) <= 1e-6
        end

        @testset "toString" begin
            @test Tropic.toString(rotationA) == "RotationA(1, 2, 3, 4)"
        end

        @testset "Numeric derivatives" begin
            g, H = Tropic.numericGradientsAndHessian(rotationA, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData)
            @test norm(g - expectedDerivativeVectorForRotationA_InternalCoordinatesData) <= 1e-6
            @test norm(H - expectedSecondDerivativeMatrixForRotationA_InternalCoordinatesData) <= 1e-4
        end
    end

    @testset "Rotation B Tests" begin
        rotationB = Tropic.RotationB([1, 2, 3, 4], h2o2Cartesians_InternalCoordinatesData)
        @testset "Rotation equality" begin
            otherRotationB = Tropic.RotationB([1, 2, 3], h2o2Cartesians_InternalCoordinatesData)
            sameRotationB = Tropic.RotationB([1, 2, 3, 4], h2o2Cartesians_InternalCoordinatesData)
            @test rotationB != otherRotationB
            @test rotationB == sameRotationB
        end

        @testset "Rotation value" begin
            expectedValue = -0.6940507971411547
            @test isapprox(Tropic.value(rotationB, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData), expectedValue)
        end

        @testset "Rotation derivative" begin
            @test norm(Tropic.derivativeVector(rotationB, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData) - expectedDerivativeVectorForRotationB_InternalCoordinatesData) <= 1e-6
        end

        @testset "Rotation second derivatives" begin
        @test norm(Tropic.secondDerivativeMatrix(rotationB, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData) - expectedSecondDerivativeMatrixForRotationB_InternalCoordinatesData) <= 1e-6
        end

        @testset "toString" begin
            @test Tropic.toString(rotationB) == "RotationB(1, 2, 3, 4)"
        end

        @testset "Numeric derivatives" begin
            g, H = Tropic.numericGradientsAndHessian(rotationB, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData)
            @test norm(g - expectedDerivativeVectorForRotationB_InternalCoordinatesData) <= 1e-6
            @test norm(H - expectedSecondDerivativeMatrixForRotationB_InternalCoordinatesData) <= 1e-4
        end
    end

    @testset "Rotation C Tests" begin
        rotationC = Tropic.RotationC([1, 2, 3, 4], h2o2Cartesians_InternalCoordinatesData)
        @testset "Rotation equality" begin
            otherRotationC = Tropic.RotationC([1, 2, 3], h2o2Cartesians_InternalCoordinatesData)
            sameRotationC = Tropic.RotationC([1, 2, 3, 4], h2o2Cartesians_InternalCoordinatesData)
            @test rotationC != otherRotationC
            @test rotationC == sameRotationC
        end

        @testset "Rotation value" begin
            expectedValue = -1.1474533593675582
            @test isapprox(Tropic.value(rotationC, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData), expectedValue)
        end

        @testset "Rotation derivative" begin
            @test norm(Tropic.derivativeVector(rotationC, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData) - expectedDerivativeVectorForRotationC_InternalCoordinatesData) <= 1e-6
        end

        @testset "Rotation second derivatives" begin
        @test norm(Tropic.secondDerivativeMatrix(rotationC, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData) - expectedSecondDerivativeMatrixForRotationC_InternalCoordinatesData) <= 1e-6
        end

        @testset "toString" begin
            @test Tropic.toString(rotationC) == "RotationC(1, 2, 3, 4)"
        end

        @testset "Numeric derivatives" begin
            g, H = Tropic.numericGradientsAndHessian(rotationC, rotatetdAndShiftedh2o2Cartesians_InternalCoordinatesData)
            @test norm(g - expectedDerivativeVectorForRotationC_InternalCoordinatesData) <= 1e-6
            @test norm(H - expectedSecondDerivativeMatrixForRotationC_InternalCoordinatesData) <= 1e-4
        end
    end
end