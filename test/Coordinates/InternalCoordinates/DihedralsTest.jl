function dihredralsTest() 
    @testset "Dihedral Tests" begin
        @testset "Dihedral equality" begin
            @test Tropic.Dihedral(1, 2, 3, 4) == Tropic.Dihedral(1, 2, 3, 4)
            @test Tropic.Dihedral(1, 2, 3, 4) == Tropic.Dihedral(4, 3, 2, 1)
        end

        @testset "Dihedral value" begin
            expectedValue = -2.5730428532
            @test isapprox(Tropic.value(Tropic.Dihedral(1, 2, 3, 4), h2o2Cartesians_InternalCoordinatesData), expectedValue)
        end

        @testset "Dihedral derivative" begin
            @test norm(Tropic.derivativeVector(Tropic.Dihedral(1, 2, 3, 4), h2o2Cartesians_InternalCoordinatesData) - expectedDerivatoveVectorForDihedral_InternalCoordinatesData) <= 1e-6
            @test norm(Tropic.derivativeVector(Tropic.Dihedral(1, 2, 3, 4), leftLinearDihedralMolecule_InternalCoordinatesData)) == 0.0
            @test norm(Tropic.derivativeVector(Tropic.Dihedral(1, 2, 3, 4), rightLinearDihedralMolecule_InternalCoordinatesData)) == 0.0
        end

        @testset "Dihedral second derivatives" begin
            @test norm(Tropic.secondDerivativeMatrix(Tropic.Dihedral(1, 2, 3, 4), h2o2Cartesians_InternalCoordinatesData) - expectedSecondDerivativeMatrixForDihedral_InternalCoordinatesData) <= 1e-6
        end

        @testset "Dihedrals are undefined" begin
            @test !Tropic.isDefined(Tropic.Dihedral(1, 2, 3, 4), acetyleneCartesians_InternalCoordinatesData)
            @test Tropic.isDefined(Tropic.Dihedral(1, 2, 3, 4), h2o2Cartesians_InternalCoordinatesData)

            @test isapprox(Tropic.value(Tropic.Dihedral(1, 2, 3, 4), acetyleneCartesians_InternalCoordinatesData), 0.0)
            @test isapprox(norm(Tropic.derivativeVector(Tropic.Dihedral(1, 2, 3, 4), acetyleneCartesians_InternalCoordinatesData)), 0.0)
            @test isapprox(norm(Tropic.secondDerivativeMatrix(Tropic.Dihedral(1, 2, 3, 4), acetyleneCartesians_InternalCoordinatesData)), 0.0)
        end

        @testset "toString" begin
            @test Tropic.toString(Tropic.Dihedral(1, 2, 3, 4)) == "Dihedral(1, 2, 3, 4)"
        end

        @testset "Numeric derivatives" begin
            g, H = Tropic.numericGradientsAndHessian(Tropic.Dihedral(1, 2, 3, 4), h2o2Cartesians_InternalCoordinatesData)
            @test norm(g - expectedDerivatoveVectorForDihedral_InternalCoordinatesData) <= 1e-6
            @test norm(H - expectedSecondDerivativeMatrixForDihedral_InternalCoordinatesData) <= 1e-5
        end

        @testset "Schlegel Hessian Guess Tests" begin
            d = Tropic.Dihedral(1, 2, 3, 4)
            @test isapprox(Tropic.getHessianGuessSchlegel(d, ["H", "O", "O", "H"], h2o2Cartesians_InternalCoordinatesData), 0.0023)
            @test isapprox(Tropic.getHessianGuessSchlegel(d, ["H", "O", "O", "H"], h2o2CartesiansCloseOxygens_InternalCoordinatesData), 0.07715133630714593)
            @test isapprox(Tropic.getHessianGuessSchlegel(d, ["A", "B", "D", "E"], h2o2Cartesians_InternalCoordinatesData), 0.0023)
        end

        @testset "Hessian Guess Tests" begin
            d = Tropic.Dihedral(1, 2, 3, 4)

            @test isapprox(Tropic.getHessianGuessLindh(d, ["H", "H", "C", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.0016495183671237637)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["C", "H", "Si", "H"], h2o2Cartesians_InternalCoordinatesData), 0.009931006590323036)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["C", "C", "Si", "C"], h2o2Cartesians_InternalCoordinatesData), 0.42693266511055566)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Si", "Si", "Si", "Si"], h2o2Cartesians_InternalCoordinatesData), 1.0825712827718361)

            @test isapprox(Tropic.getHessianGuessLindh(d, ["Bi", "Si", "Si", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Si", "Bi", "Si", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Si", "Si", "Bi", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Si", "Si", "Si", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.1)

            @test isapprox(Tropic.getHessianGuessLindh(d, ["Bi", "Bi", "Si", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Bi", "Si", "Bi", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Bi", "Si", "Si", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Si", "Bi", "Bi", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Si", "Bi", "Si", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Si", "Si", "Bi", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.1)

            @test isapprox(Tropic.getHessianGuessLindh(d, ["Si", "Bi", "Bi", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Bi", "Si", "Bi", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Bi", "Bi", "Si", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.1)
            @test isapprox(Tropic.getHessianGuessLindh(d, ["Bi", "Bi", "Bi", "Si"], h2o2Cartesians_InternalCoordinatesData), 0.1)

            @test isapprox(Tropic.getHessianGuessLindh(d, ["Bi", "Bi", "Bi", "Bi"], h2o2Cartesians_InternalCoordinatesData), 0.1)
        end
    end
end