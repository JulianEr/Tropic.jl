function translationsTest()
    @testset "Tranlsation X Tests" begin
        translationX = Tropic.TranslationX([1, 2, 3, 4])
        @testset "Translation equality" begin
            otherTranslationX = Tropic.TranslationX([2, 3, 4])
            sameTranslationX = Tropic.TranslationX([1, 2, 3, 4])
            @test translationX != otherTranslationX
            @test translationX == sameTranslationX
        end

        @testset "Translation value" begin
            expectedValue = -10.555537705276064
            @test isapprox(Tropic.value(translationX, h2o2Cartesians_InternalCoordinatesData), expectedValue)
        end

        @testset "Translation derivative" begin
            @test norm(Tropic.derivativeVector(translationX, h2o2Cartesians_InternalCoordinatesData) - expectedDerivativeVectorForTranslationX_InternalCoordinatesData) <= 1e-6
        end

        @testset "Translation second derivatives" begin
            @test norm(Tropic.secondDerivativeMatrix(translationX, h2o2Cartesians_InternalCoordinatesData) - expectedSecondDerivativeMatrixForAnyTranslation) <= 1e-6
        end

        @testset "toString" begin
            @test Tropic.toString(translationX) == "TranslationX(1, 2, 3, 4)"
        end

        @testset "Numeric derivatives" begin
            # Translations are not twice differentiable so numeric values will not work for second derivatives
            g, _ = Tropic.numericGradientsAndHessian(translationX, h2o2Cartesians_InternalCoordinatesData)
            @test norm(g - expectedDerivativeVectorForTranslationX_InternalCoordinatesData) <= 1e-6
        end
    end

    @testset "Tranlsation Y Tests" begin
        translationY = Tropic.TranslationY([1, 2, 3, 4])
        @testset "Translation equality" begin
            otherTranslationY = Tropic.TranslationY([2, 3, 4])
            sameTranslationY = Tropic.TranslationY([1, 2, 3, 4])
            @test translationY != otherTranslationY
            @test translationY == sameTranslationY
        end

        @testset "Translation value" begin
            expectedValue = 0.35607164518939133
            @test isapprox(Tropic.value(translationY, h2o2Cartesians_InternalCoordinatesData), expectedValue)
        end

        @testset "Translation derivative" begin
            @test norm(Tropic.derivativeVector(translationY, h2o2Cartesians_InternalCoordinatesData) - expectedDerivativeVectorForTranslationY_InternalCoordinatesData) <= 1e-6
        end

        @testset "Translation second derivatives" begin
            @test norm(Tropic.secondDerivativeMatrix(translationY, h2o2Cartesians_InternalCoordinatesData) - expectedSecondDerivativeMatrixForAnyTranslation) <= 1e-6
        end

        @testset "toString" begin
            @test Tropic.toString(translationY) == "TranslationY(1, 2, 3, 4)"
        end

        @testset "Numeric derivatives" begin
            # Translations are not twice differentiable so numeric values will not work for second derivatives
            g, _ = Tropic.numericGradientsAndHessian(translationY, h2o2Cartesians_InternalCoordinatesData)
            @test norm(g - expectedDerivativeVectorForTranslationY_InternalCoordinatesData) <= 1e-6
        end
    end

    @testset "Tranlsation Z Tests" begin
        translationZ = Tropic.TranslationZ([1, 2, 3, 4])
        @testset "Translation equality" begin
            otherTranslationZ = Tropic.TranslationZ([2, 3, 4])
            sameTranslationZ = Tropic.TranslationZ([1, 2, 3, 4])
            @test translationZ != otherTranslationZ
            @test translationZ == sameTranslationZ
        end

        @testset "Translation value" begin
            expectedValue = 0.552036244399368
            @test isapprox(Tropic.value(translationZ, h2o2Cartesians_InternalCoordinatesData), expectedValue)
        end

        @testset "Translation derivative" begin
            @test norm(Tropic.derivativeVector(translationZ, h2o2Cartesians_InternalCoordinatesData) - expectedDerivativeVectorForTranslationZ_InternalCoordinatesData) <= 1e-6
        end

        @testset "Translation second derivatives" begin
            @test norm(Tropic.secondDerivativeMatrix(translationZ, h2o2Cartesians_InternalCoordinatesData) - expectedSecondDerivativeMatrixForAnyTranslation) <= 1e-6
        end

        @testset "toString" begin
            @test Tropic.toString(translationZ) == "TranslationZ(1, 2, 3, 4)"
        end
        
        @testset "Numeric derivatives" begin
            # Translations are not twice differentiable so numeric values will not work for second derivatives
            g, _ = Tropic.numericGradientsAndHessian(translationZ, h2o2Cartesians_InternalCoordinatesData)
            @test norm(g - expectedDerivativeVectorForTranslationZ_InternalCoordinatesData) <= 1e-6
        end
    end
end