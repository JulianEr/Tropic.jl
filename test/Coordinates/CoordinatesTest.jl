using Test, LinearAlgebra

import Tropic

include("BuildInternalCoordinates/BuildInternalCoordinatesTest.jl")
include("Constraints/ConstraintsTest.jl")
include("Formats/FormatsTest.jl")
include("InternalCoordinates/InternalCoordinatesTests.jl")
include("SetsOfInternalCoordinates/SetsOfInternalCoordinatesTest.jl")
include("Zmatrices/ZmatricesTest.jl")

include("CartesianCoordinatesTest.jl")
include("DelocalizedInternalCoordinatesTest.jl")
include("RedundantInternalCoordinatesTest.jl")

cartesianCoordinatesTest()
delocalizedInternalCoordinatesTest()
redundantInternalCoodinatesTest()
