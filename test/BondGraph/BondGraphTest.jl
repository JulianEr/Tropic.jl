using Logging, LightGraphs, LinearAlgebra

import Tropic

@info "Start Bond Graph tests"

include("BondGraphTestData.jl")

@testset "BondGraph tests" begin
    graph = Tropic.buildBondGraph(azadirachtinSymbols_BondGraphTestData, azadirachtinCartesians_BondGraphTestData)

    @testset "Finding connections and bonds, angles, and dihedrals" begin
        @test nv(graph) == 95
        @test ne(graph) == 103
        @test length(Tropic.getBonds(graph)) == 103
        @test length(Tropic.getAngles(graph)) == 203
        @test length(Tropic.getDihedrals(graph)) == 327
    end

    @testset "Longest Shortest Path" begin
        path, edges = Tropic.getLongestShortestPathAlongPath(graph, vertices(graph))
        @test length(path) == 17
        @test path == longestShortestPath_BondGraphTestData
    end 

    @testset "get new order" begin
        newOrder = Tropic.getReorderedSequence(graph)
        @test newOrder == expectedNewOrder_BondGraphTestData

        reorderedSymbols = Tropic.reorderSymbols(newOrder, azadirachtinSymbols_BondGraphTestData)
        reorderedCartesians = reorderCoordinates(newOrder, azadirachtinCartesians_BondGraphTestData)

        @test reorderedSymbols == expectedReorderedazadirachtinSymbols_BondGraphTestData
        @test norm(reorderedCartesians - expectedReorderedazadirachtinCartesians_BondGraphTestData) < 1e-6
    end 

    @testset "get new order for multiple molecules" begin

        newOrder, benzeneGraph = Tropic.getReorderedSequenceAndGraph(benzeneTwiceSymbols_BondGraphTestData, benzeneTwiceCartesians_BondGraphTestData)

        @test nv(benzeneGraph) == 24
        @test ne(benzeneGraph) == 24
        @test length(Tropic.getBonds(benzeneGraph)) == 24
        @test length(Tropic.getAngles(benzeneGraph)) == 36
        @test length(Tropic.getDihedrals(benzeneGraph)) == 48

        molecules = Tropic.getMolecules(benzeneGraph)
        for i in eachindex(molecules)
            @test molecules[i] == expectedReorderdMoleculeIndicesForBenzene_BondGraphTestData[i]
        end

        @test newOrder == expectedNewOrderForBenzene_BondGraphTestData

        reorderedSymbols = Tropic.reorderSymbols(newOrder, benzeneTwiceSymbols_BondGraphTestData)
        reorderedCartesians = Tropic.reorderCoordinates(newOrder, benzeneTwiceCartesians_BondGraphTestData)

        @test reorderedSymbols == expectedReorderedSymbolsForBenzene_BondGraphTestData
        @test norm(reorderedCartesians - expectedReorderedCartesiansForBenzene_BondGraphTestData) < 1e-6
    end 

    @testset "Dihedrals for small cycles" begin
        dihedrals = Tropic.getDihedrals(veryCyclicGraph_BondGraphTestData())
        @test dihedrals == expectedDihedralsForCyclicGraphs_BondGraphTestData
    end

    @testset "Dihedrals and Angles" begin
        smallGraph = triangleGraph_BondGraphTestData()
        angles = Tropic.getAngles(smallGraph)
        dihedrals = Tropic.getDihedrals(smallGraph)
        @test angles == expectedTriangleAngles_BondGraphTestData
        @test dihedrals == expectedTriangleDihedrals_BondGraphTestData
    end
end