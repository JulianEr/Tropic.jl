function mainTest()
    @testset "Command Line Interface Tests" begin
        @testset "Test With (almost) no additional arguments" begin
            tmpFileName = "input.xyz"
            write(tmpFileName, h2o2CartesiansFile_CommandLineInterfaceTestData)
            try
                empty!(ARGS)
                append!(ARGS, noArguments_CommandLineInterfaceTestData)
                
                Tropic.julia_main()

                logs = open("log.dat", "r") do iFile
                    read(iFile, String)
                end

                @test occursin("coords = Tropic.DelocalizedInternalCoordinates", logs)
                @test occursin("UnderlyingZmatrix", logs)
                @test occursin("gradientsAndHessian = Tropic.AnalyticGradientsUpdatedHessian([\"H\", \"O\", \"O\", \"H\", \"H\", \"O\", \"O\", \"H\"], Tropic.HessianUpdate(Tropic.bfgs, Float64[], Float64[]), Tropic.Psi4Engine(\"psi4\", 0, \"sto-3g\", \"scf\", 0, 1, 2))", logs)
                @test occursin("stepControl = Tropic.RSRFO(0.5, 0.1, 1.0e-6, 0, Inf, Inf, false)", logs)
                @test occursin("convergenceCheck = Tropic.GradientsAndChangeCheck(0.00045, 0.0003, 0.0018, 0.0012)", logs)
                @test occursin("initialHessian = Tropic.GuessHessian([\"H\", \"O\", \"O\", \"H\", \"H\", \"O\", \"O\", \"H\"], \"Schlegel\")", logs)
                @test occursin("trustRadiusUpdate = Tropic.NonRejectingTrustRadiusUpdate(1.0, 0.001, 3.0, 0.25)", logs)
                
                reorderedCart = open("output.xyz", "r") do iFile
                    read(iFile, String)
                end

                @test reorderedCart == reorderedH2o2CartesiansFile_CommandLineInterfaceTestData

                @test_nowarn rm("output.xyz")
                @test_nowarn rm("log.dat")
            finally 
                rm(tmpFileName)
            end
        end

        @testset "Test with all arguments except constraints" begin
            write(inputFileName_CommandLineInterfaceTestData, h2o2CartesiansFile_CommandLineInterfaceTestData)
            try     
                empty!(ARGS)
                append!(ARGS, variableArguments_CommandLineInterfaceTestData)
                
                Tropic.julia_main()
                
                logs = open(logFileName_CommandLineInterfaceTestData, "r") do iFile
                    read(iFile, String)
                end

                @test occursin("coords = Tropic.CartesianCoordinates", logs)
                @test occursin("gradientsAndHessian = Tropic.AnalyticGradientsAndHessian([\"H\", \"H\", \"H\", \"H\", \"O\", \"O\", \"O\", \"O\"], Tropic.Psi4Engine(\"somewhere/to/psi/psi4.sh\", 3, \"aug-cc-pvtz\", \"b3lyp\", 2, 4, 4))", logs)
                @test occursin("stepControl = Tropic.TrustRegionMethod(0.25, 0.1, 1.0e-6, 0, Inf, Inf)", logs)
                @test occursin("convergenceCheck = Tropic.GradientsAndValueOrChangeCheck(0.42, 0.5, 0.123, 0.432, 0.99)", logs)
                @test occursin("initialHessian = Tropic.CalculatedHessian()", logs)
                @test occursin("trustRadiusUpdate = Tropic.BofillTrustRadiusUpdate(0.0, 2.0, 0.75, 0.8, 1.2, 1.25, 2.0, 0.0001)", logs)
                
                noReorderedCart = open(outputFileName_CommandLineInterfaceTestData, "r") do iFile
                    read(iFile, String)
                end

                @test noReorderedCart == h2o2CartesiansFile_CommandLineInterfaceTestData

                @test_nowarn rm(outputFileName_CommandLineInterfaceTestData)
                @test_nowarn rm(logFileName_CommandLineInterfaceTestData)
            finally 
                rm(inputFileName_CommandLineInterfaceTestData)
            end
        end

        @testset "Test all constraints" begin
            tmpFileName = "input.xyz"
            write(tmpFileName, h2o2CartesiansFile_CommandLineInterfaceTestData)
            try  
                empty!(ARGS)
                append!(ARGS, allConstraintsArguments_CommandLineInterfaceTestData)

                Tropic.julia_main()

                logs = open("log.dat", "r") do iFile
                    read(iFile, String)
                end

                @test occursin("coords = Tropic.RedundantInternalCoordinates", logs)
                @test occursin("UnderlyingZmatrix", logs)
                @test occursin("[3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 23, 28, 31]", logs)
                @test occursin("gradientsAndHessian = Tropic.AnalyticGradientsUpdatedHessian([\"H\", \"O\", \"O\", \"H\", \"H\", \"O\", \"O\", \"H\"], Tropic.BfgsUseLastNForUpdate(Array{Float64}[], Array{Float64}[], 1.0, 0.5, 1.0e-7, 0.5, 5), Tropic.XtbEngine(\"path/to/stb/xtb.bat\", 3))", logs)
                @test occursin("convergenceCheck = Tropic.OnlyGradientsCheck(0.01, 0.042)", logs)
                @test occursin("trustRadiusUpdate = Tropic.StandardTrustRadiusUpdate(1.0, 0.1, 1.4142135623730951, 0.5, -1.0)", logs)

                reorderedCart = open("output.xyz", "r") do iFile
                    read(iFile, String)
                end

                @test reorderedCart == reorderedH2o2CartesiansFile_CommandLineInterfaceTestData

                @test_nowarn rm("output.xyz")
                @test_nowarn rm("log.dat")
            finally 
                rm(tmpFileName)
            end
        end

        @testset "Test silent logs" begin
            tmpFileName = "input.xyz"
            write(tmpFileName, h2o2CartesiansFile_CommandLineInterfaceTestData)
            try  
                empty!(ARGS)
                append!(ARGS, ailentArguments_CommandLineInterfaceTestData)
                
                Tropic.julia_main()

                @test filesize("log.dat") == 0

                reorderedCart = open("output.xyz", "r") do iFile
                    read(iFile, String)
                end

                @test reorderedCart == reorderedH2o2CartesiansFile_CommandLineInterfaceTestData

                @test_nowarn rm("output.xyz")
                @test_nowarn rm("log.dat")
            finally 
                rm(tmpFileName)
            end
        end
    end
end