struct InitialHessianInternalCoordinatesDouble <: AbstractCoordinates end

identityHessian(::InitialHessianInternalCoordinatesDouble) = I

guessHessianSchlegel(::InitialHessianInternalCoordinatesDouble, ::Array{String}, ::AbstractOptimizer) = guessedInitialHessian

guessHessianLindh(::InitialHessianInternalCoordinatesDouble, ::Array{String}, ::AbstractOptimizer) = guessedInitialHessianLindh