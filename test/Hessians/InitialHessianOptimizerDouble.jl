using DataStructures

mutable struct InitialHessianOptimizerDouble <: AbstractOptimizer
    gradientsQueue::Queue{Array{Float64}}
    xsQueue::Queue{Array{Float64}}

    function InitialHessianOptimizerDouble(values::Vector{Vector{Float64}}, gradients::Vector{Vector{Float64}})
        gradientsQueue = Queue{Array{Float64}}()
        xsQueue = Queue{Array{Float64}}()
        for (value, gradient) in zip(values, gradients)
            enqueue!(gradientsQueue, gradient)
            enqueue!(xsQueue, value)
        end
        new(gradientsQueue, xsQueue)
    end
    function InitialHessianOptimizerDouble()
        new(Queue{Array{Float64}}(), Queue{Array{Float64}}())
    end
end

InitialHessianOptimizerDouble1() = InitialHessianOptimizerDouble([updateHessianFirstXs, updateHessianSecondsXs], [updateHessianFirstGradients, updateHessianSecondGradients])
InitialHessianOptimizerDouble2() = InitialHessianOptimizerDouble(
    [[-0.030948186430398875, -0.030948186430398872, 0.04141653455996453],
     [-0.0036224644719039857, -0.0036224644719050564, -0.022684842555517486],
     [0.0045541395972512905, 0.004554139597251337, 0.003703203163076146],
     [2.5067392608965035e-5, 2.5067392608953654e-5, -0.00048593980036699475],
     [1.0857633853199299e-5, 1.0857633853200204e-5, 4.137148772241746e-5],
     [1.0857633853199299e-5, 1.0857633853200204e-5, 4.137148772241746e-5],
     [1.0857633853199299e-5, 1.0857633853200204e-5, 4.137148772241746e-5],
     [1.0857633853199299e-5, 1.0857633853200204e-5, 4.137148772241746e-5]],
    [[1.8141376429, 1.8141376429, 1.9111354810],
     [1.8686233590, 1.8686233590, 1.6721094900],
     [1.8764702600, 1.8764702600, 1.7566344554],
     [1.8698804010, 1.8698804010, 1.7441137358],
     [1.8697481557, 1.8697481557, 1.7459218570],
     [1.8697481557, 1.8697481557, 1.7459218570],
     [1.8697481557, 1.8697481557, 1.7459218570],
     [1.8697481557, 1.8697481557, 1.7459218570],]
)

getCoordinateSystem(::InitialHessianOptimizerDouble) = InitialHessianInternalCoordinatesDouble()

getAnalyticHessian(::InitialHessianOptimizerDouble) = caclulatedHessian

getPrimitiveValues(self::InitialHessianOptimizerDouble) = dequeue!(self.xsQueue)
getChange(self::InitialHessianOptimizerDouble, other::Vector{Float64}) = dequeue!(self.xsQueue) - other
getGradients(self::InitialHessianOptimizerDouble) = dequeue!(self.gradientsQueue)
getHessian(::InitialHessianOptimizerDouble) = updateHessianInitialHessian