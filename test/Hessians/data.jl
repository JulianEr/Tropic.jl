const identityInitialHessian = I
const guessedInitialHessian = [1.0 0.0 0.0; 0.0 2.0 0.0; 0.0 0.0 1.0]
const guessedInitialHessianLindh = [3.0 0.0 0.0; 0.0 1.0 0.0; 0.0 0.0 2.0]
const caclulatedHessian = [2.0 0.0 0.0; 0.0 1.0 0.0; 0.0 0.0 2.0]

const updateHessianFirstGradients = [-0.030948186178436875, -0.030948186178436858, 0.04141653455518471]
const updateHessianSecondGradients = [-0.003622270954085328, -0.0036222709540854135, -0.02268482528717323]
const updateHessianFirstXs = [0.0, 0.0, 0.0]
const updateHessianSecondsXs = [0.0544857156234949, 0.054485715623509845, -0.23902599105705477]
const updateHessianInitialHessian = [0.5547334042430275 0.0 0.0; 0.0 0.5547334042430275 0.0; 0.0 0.0 0.16]

const updateHessianExpectedBfgsHessian = [
     0.5220715907  -0.0326618136  -0.0027615126;
    -0.0326618136   0.5220715907  -0.0027615126;
    -0.0027615126  -0.0027615126   0.2669183949
]

const updateHessianExpectedBfgsUseLastNForUpdateHessian = [
      0.9175652150   0.3628318108  -0.3859959375;
      0.3628318108   0.9175652150  -0.3859959375;
     -0.3859959375  -0.3859959375   3.4154184064
]

const updateHessianExpectedSr1PbsHessian = [
     0.5539553753  -0.0007780289   0.0117742357;
    -0.0007780289   0.5539553753   0.0117742357;
     0.0117742357   0.0117742357   0.2735452110
]