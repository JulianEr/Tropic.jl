include("data.jl")

include("InitialHessianInternalCoordinatesDouble.jl")
include("InitialHessianOptimizerDouble.jl")

@testset "Testing Initial Hessians" begin
    @test getInitialHessian(IdentityHessian(), InitialHessianOptimizerDouble()) == identityInitialHessian
    @test getInitialHessian(GuessHessian(["A"; "B"; "C"]), InitialHessianOptimizerDouble()) == guessedInitialHessian
    @test getInitialHessian(GuessHessian(["A"; "B"; "C"], "Lindh"), InitialHessianOptimizerDouble()) == guessedInitialHessianLindh
    @test getInitialHessian(CalculatedHessian(), InitialHessianOptimizerDouble()) == caclulatedHessian
end

@testset "Testing Hessian Updating" begin
    bfgs = BfgsUpdate()
    optimizer = InitialHessianOptimizerDouble1()
    notify!(bfgs, optimizer)
    @test norm(updateHessian(bfgs, optimizer) - updateHessianExpectedBfgsHessian) < 1e-6

    bfgs = BfgsUseLastNForUpdate(4)
    optimizer = InitialHessianOptimizerDouble2()
    notify!(bfgs, optimizer)
    notify!(bfgs, optimizer)
    notify!(bfgs, optimizer)
    notify!(bfgs, optimizer)
    @test norm(updateHessian(bfgs, optimizer) - updateHessianExpectedBfgsUseLastNForUpdateHessian) < 1e-6

    sr1pbs = Sr1PsbUpdate()
    optimizer = InitialHessianOptimizerDouble1()
    notify!(sr1pbs, optimizer)
    @test norm(updateHessian(sr1pbs, optimizer) - updateHessianExpectedSr1PbsHessian) < 1e-6
end